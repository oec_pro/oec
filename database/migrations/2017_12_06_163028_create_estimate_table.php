<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('customer_id');
                $table->string('customer_name', 120)->nullable();
                $table->integer('esti_no');
                $table->string('usr_esti_no', 20)->nullable();
                $table->date('estimate_date');
                $table->string('sales_cond', 1);
                $table->string('title', 120);
                $table->string('delivery_location', 120)->nullable();
                $table->string('expiration_date', 40)->nullable();
                $table->string('delivery_date', 40)->nullable();
                $table->string('pay_conditions', 40)->nullable();
                $table->text('remarks')->nullable();
                $table->decimal('total_amount_taxnon', 12, 2)->default(0);
                $table->decimal('tax_amount', 12, 2)->default(0);
                $table->decimal('total_amount_taxin', 12, 2)->default(0);
                $table->decimal('total_cost_amount', 12, 2)->default(0);
                $table->string('tax_type', 1)->nullable();
                $table->integer('tax_rate');
                $table->string('round_type', 1);
                $table->text('coments')->nullable();
                $table->integer('staff_id')->nullable();
                $table->string('staff_name', 120)->nullable();
                $table->string('created_name', 40);
                $table->string('updated_name', 40);
                $table->timestamps();
                $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}
