<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Customer;
use App\Estimate;
use App\Estimate_detail;
use App\Taxrate;
use App\User;
use App\Http\Requests\FormEstimateRequest;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Excel;
use Input;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet_Drawing;
use Request;
use Response;
use Schema;
use Session;
use Validator;

class EstimatesController extends Controller
{
    //得意先選択画面 - MAX表示件数
    public $per_page = 10;
    //御見積書Sheet - MAX表示件数
    public $est_rows = 20;
    public $est_taxin_rows = 21;
    //請書Sheet - MAX表示件数
    public $uke_rows = 5;
    //納品書Sheet - MAX表示件数
    public $nouhin_rows = 20;
    public $nouhin_taxin_rows = 21;
    //請求書Sheet - MAX表示件数
    public $seikyu_rows = 24;
    public $seikyu_taxin_rows = 25;

    public $file_name = "";

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return Response
     */
    public function index()
    {
        $query = Estimate::query();

        //データ取得 - 表示件数20件
        $data['results'] = $query->orderBy('updated_at', 'desc')->paginate(\Config::get('const.paginate_20'));
        $data['search']  = null;

        return view('estimates.index', compact('data'));

    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {

        $prms = Request::all();

        //データ取得 - 表示件数20件
        $data['results'] = $this->get_estimate_search_list($prms, Config::get('const.paginate_20'));
        $fff = count($data['results'] );

        return view('estimates.index', compact('data'));
    }

    /**
     * 検索：クエリ作成
     *
     * @return Response
     */
    public function get_estimate_search_list($prms, $paginate_num)
    {

        $query = Estimate::query();

        //得意先名
        if (isset($prms['customer_name']) && !empty($prms['customer_name'])){
            $query->where('customer_name','like','%'.$prms['customer_name'].'%');
        }
        //件名
        if (isset($prms['title']) && !empty($prms['title'])){
            $query->where('title','like','%'.$prms['title'].'%');
        }
        //見積日From
        if (isset($prms['estimate_date_from']) && !empty($prms['estimate_date_from'])){
            $query->where('estimate_date','>=', $prms['estimate_date_from']);
        }
        //見積日To
        if (isset($prms['estimate_date_to']) && !empty($prms['estimate_date_to'])){
            $query->where('estimate_date','<=', $prms['estimate_date_to']);
        }
        //コメント
        if (isset($prms['coments']) && !empty($prms['coments'])){
            $query->where('coments','like','%'.$prms['coments'].'%');
        }
        //状態
        if (isset($prms['sales_cond']) && !empty($prms['sales_cond'])){
            $query->where('sales_cond','=', $prms['sales_cond']);
        }
        //担当者
        if (isset($prms['staff_id']) && !empty($prms['staff_id'])){
            $query->where('staff_id','=', $prms['staff_id']);
        }
        //見積番号
        if (isset($prms['esti_no']) && !empty($prms['esti_no'])){
            $query->where('esti_no','like','%'.$prms['esti_no'].'%');
        }
        //税込金額From
        if (isset($prms['total_amount_taxin_from']) && !empty($prms['total_amount_taxin_from'])){
            $query->where('total_amount_taxin','>=', str_replace(',', '', $prms['total_amount_taxin_from']) );
        }
        //税込金額To
        if (isset($prms['total_amount_taxin_to']) && !empty($prms['total_amount_taxin_to'])){
            $query->where('total_amount_taxin','<=', str_replace(',', '', $prms['total_amount_taxin_to']));
        }

        //paginate
        $data = $query->orderBy('updated_at', 'desc')->paginate($paginate_num)->appends($prms);

        return $data;

    }

    /**
     * 登録更新画面へ遷移
     *
     * @return Response
     */
    public function edit($id=null)
    {
        //見積データテーブル情報の取得
		if (is_numeric($id)) {
            $query = Estimate::query();
            $esti  = $query->where('esti_no',$id)->get();

            $data['info'] = Estimate::find( $esti[0]->id );
            $data['info']['seq_id'] = $data['info']['id'];
        }else{
            $data['info'] = new Estimate();
            $data['info']['seq_id']     = "";
            $data['info']['sales_cond'] = 1;    //状態:「見積」

            /* -----------------------------------------------------
             * 税率計算種別
             * 1:四捨五入(round)　2:切り上げ(ceil)　3:切り捨て(floor)
             * -----------------------------------------------------  */
            if ( Config::get('const.round_type') == "round" ) {
                $round_type_key = "1";
            }elseif ( Config::get('const.round_type') == "ceil" ) {
                $round_type_key = "2";
            }elseif ( Config::get('const.round_type') == "floor" ) {
                $round_type_key = "3";
            }
            $data['info']['round_type'] = $round_type_key;
        }

        //ユーザー情報の取得
        $user_list         = User::orderBy('user_name','asc')->pluck('user_name', 'id');
        $data['user_list'] = $user_list -> prepend('', '');

        //見積明細データの取得
        $query             = Estimate_detail::query();
        $data['results']   = $query->where('esti_no',$id)->orderBy('esti_line_no')->get();

        //消費税データの取得
        $cntTaxRate              = Taxrate::orderBy('tax_rate','asc')->pluck('tax_rate', 'tax_rate')->count();
        $data['taxrates']        = Taxrate::orderBy('effective_date','desc')->pluck('tax_rate', 'id');
        $data['taxrates_active'] = Taxrate::orderBy('effective_date','desc')->get();
        if ( $cntTaxRate == 0 ) {
            $data['taxrates'] = array("0" => "0");
        }
        //変更画面の場合、税率初期値をセットする
        if ( empty($data['info']['tax_rate']) ) {
            foreach ($data['taxrates_active'] as $key => $value) {
                //初期値：税率を見積日にあわせて決定する
                $today = date("Y-m-d");
                if ( $value['effective_date'] <= $today ) {
                    $data['info']['tax_rate'] = $value['id'];
                    break;
                }
            }
        }

        //"複写(新規)"モードのセット
        //0:ノーマル 1:複写モード
        $data['mode']  = 0;

        //データが存在しない場合、空白行を作成する。(新規登録)
        if (count($data['results'])==0) {
            $data['results'] = $this->makeRows();
        }

        return view('estimates.edit', compact('data'));

    }

    /**
     * 空白行を作成する
     * @return Response
     */
    private function makeRows()
    {
        $data = array();
        $data[0]['id']               = "";
        $data[0]['item_id']          = "";
        $data[0]['item_name']        = "";
        $data[0]['item_description'] = "";
        $data[0]['quantity']         = "";
        $data[0]['unit']             = "";
        $data[0]['sal_price']        = "";
        $data[0]['sal_amount']       = "";
        $data[0]['cost_price']       = "";
        $data[0]['cost_amount']      = "";
        $data[0]['tax_flg']          = "";
        $data[0]['item_remarks']     = "";

        return $data;
    }

    /**
     * バリデーションルール
     *
     * @return Response
     */
    public $validateRules = [
        'customer_id'           => '',
        'customer_name'         => 'required|string|max:60',
        'title'                 => 'required|string|max:60',
        'delivery_location'     => 'string|nullable|max:60',
        'expiration_date'       => 'string|nullable|max:20',
        'delivery_date'         => 'string|nullable|max:20',
        'pay_conditions'        => 'string|nullable|max:20',
        'remarks'               => 'string|nullable',
        'estimate_date'         => 'required|date',
        'sales_cond'            => 'required',
//        'esti_no'               => 'required|integer',
        'total_amount_taxnon'   => 'required|numeric|min:-9999999999|max:9999999999.99',
        'tax_amount'            => 'required|numeric|min:-9999999999|max:9999999999.99',
        'total_amount_taxin'    => 'required|numeric|min:-9999999999|max:9999999999.99',
        'total_cost_amount'     => 'required|numeric|min:-9999999999|max:9999999999.99',
        'expiration_date'       => 'string|nullable',
        'staff_id'              => 'required',
        'item_id.*'                 => '',
        'item_name.*'               => 'required|string|max:60',
        'item_description.*'        => 'string|nullable|max:80',
        'quantity.*'                => 'required|numeric|max:99999999',
        'unit.*'                    => 'required|string|max:20',
        'sal_price.*'               => 'required|numeric|max:9999999999.99',
//        'sal_amount.*'              => 'required|numeric|max:9999999999.99',
        'cost_price.*'              => 'numeric|min:0|max:9999999999.99',
//        'cost_amount.*'             => 'numeric|max:9999999999.99',
//        'tax_flg.*'                 => 'required|not_in: 0',
    ];

    /**
     * バリデーションメッセージ
     *
     * @return Response
     */
    public $validateMessages = [
            'tax_flg.*' => ':attributeは必須です。',
    ];

    /**
     * バリデーション前成形処理
     * 一行全て未入力の場合はバリデーションしない
     * @return Response
     */
    private function validationData($input)
    {

        $make_input = array();

        /* ------------------
         * POST見積データ
         * ------------------ */
//        $estimate['info']['id']                  = $input['seq_id'];
        $estimate['info']['seq_id']              = $input['seq_id'];
        $estimate['info']['customer_id'] 	     = $input['customer_id'];
        $estimate['info']['customer_name'] 	     = $input['customer_name'];
        $estimate['info']['esti_no'] 			 = $input['esti_no'];
        $estimate['info']['estimate_date'] 	     = $input['estimate_date'];
        $estimate['info']['sales_cond'] 		 = $input['sales_cond'];
        $estimate['info']['title'] 			     = $input['title'];
        $estimate['info']['delivery_location']   = $input['delivery_location'];
        $estimate['info']['expiration_date'] 	 = $input['expiration_date'];
        $estimate['info']['delivery_date'] 	     = $input['delivery_date'];
        $estimate['info']['pay_conditions'] 	 = $input['pay_conditions'];
        $estimate['info']['remarks'] 			 = $input['remarks'];
        $estimate['info']['total_amount_taxnon'] = str_replace(",", "", $input['total_amount_taxnon']);
        $estimate['info']['tax_amount'] 		 = str_replace(",", "", $input['tax_amount']);
        $estimate['info']['total_amount_taxin']  = str_replace(",", "", $input['total_amount_taxin']);
        $estimate['info']['total_cost_amount'] 	 = str_replace(",", "", $input['total_cost_amount']);
//        $estimate['info']['tax_type'] 		     = $input['tax_type'];
        $estimate['info']['tax_rate'] 		     = $input['tax_rate'];
        $estimate['info']['round_type'] 		 = $input['round_type'];
        $estimate['info']['coments'] 			 = $input['coments'];
        $estimate['info']['staff_id'] 			 = $input['staff_id'];
//        $estimate['info']['staff_name'] 		 = $input['staff_name'];
        $estimate['info']['created_at'] 			 = $input['created_at'];
        $estimate['info']['updated_at'] 			 = $input['updated_at'];
        $estimate['info']['created_name'] 			 = $input['created_name'];
        $estimate['info']['updated_name'] 			 = $input['updated_name'];

        $make_input = $estimate['info'];

        /* ------------------
         * POST見積明細データ
         * ------------------ */
        $cnt = count($input['id']);
        $k=0;
        for ( $i=0; $i<$cnt; $i++ ) {
            if ( empty($input['item_id'][$i]) && empty($input['item_name'][$i]) && empty($input['quantity'][$i]) &&
                 empty($input['unit'][$i]) && empty($input['sal_price'][$i]) && empty($input['sal_amount'][$i]) &&
                 /* empty($input['tax_flg'][$i]) && */ empty($input['item_remarks'][$i]) && empty($input['item_description'][$i]) &&
                 empty($input['cost_price'][$i]) && empty($input['cost_amount'][$i])
                ){

            }else{
                $make_input['id'][$k]               = $input['id'][$i];
                $make_input['no'][$k]               = $input['no'][$i];
                $make_input['item_id'][$k]          = $input['item_id'][$i];
                $make_input['item_name'][$k]        = $input['item_name'][$i];
                $make_input['item_description'][$k] = $input['item_description'][$i];
                $make_input['quantity'][$k]         = $input['quantity'][$i];
                $make_input['unit'][$k]             = $input['unit'][$i];
                $make_input['sal_price'][$k]        = str_replace(",", "", $input['sal_price'][$i]);
                $make_input['sal_amount'][$k]       = str_replace(",", "", $input['sal_amount'][$i]);
                $make_input['cost_price'][$k]       = str_replace(",", "", $input['cost_price'][$i]);
                $make_input['cost_amount'][$k]      = str_replace(",", "", $input['cost_amount'][$i]);
                $make_input['tax_flg'][$k]          = $input['tax_flg'][$i];
                $make_input['item_remarks'][$k]     = $input['item_remarks'][$i];
                $k++;
            }
        }

        $results['input_esti'] = $estimate['info'];
        $results['input_all']  = $make_input;

        return $results;
    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
//    public function save(FormHogeRequest $request)
    public function save()
    {
        //入力データ取得
        $input = Request::all();
        $Qrrr = $input['created_at'];

        //明細入力データ(バリデーション前に成形処理)
        $request = $this->validationData($input);

        //明細情報の作成(Viewデータセット用)
        $data['info'] = $request['input_esti'];

        //ユーザー情報の作成(Viewデータセット用)
        $user_list         = User::orderBy('user_name','asc')->pluck('user_name', 'id');
        $data['user_list'] = $user_list -> prepend('', '');

        //消費税データの取得(Viewデータセット用)
        $data['taxrates']  = Taxrate::orderBy('effective_date','desc')->pluck('tax_rate', 'id');
        $cntTaxRate = count($data['taxrates']);
        if ( $cntTaxRate == 0 ) {
            $data['taxrates'] = array("0" => "0");
        }

        //"複写(新規)"モード(Viewデータセット用)
        //0:ノーマル 1:複写モード
        $data['mode']      = Input::get('mode_copy');

        //バリデーション実行
        $validator = Validator::make($request['input_all'], $this->validateRules, $this->validateMessages);

        if( !isset($request['input_all']['id']) ) {
            //明細行が無い場合、空行を作成する。
            $data['results'] = $this->makeRows();
            $validator->errors()->add('no_rows', '明細情報が入力されておりません。');
            return view('estimates.edit')->with(compact('data'))->withErrors($validator);
        }

        if ($validator->fails()) {

            //動的に行を追加削除しているため、新規にPOSTデータ配列を作成しビューへ返す(Viewデータセット用)
            $data['results'] = array();
            $i=0;
            if( isset($request['input_all']['id']) ) {
//                //明細行が無い場合、空行を作成する。
//                $data['results'] = $this->makeRows();
//                $validator->errors()->add('no_rows', '明細情報が入力されておりません。');
//            }else{
                foreach ($request['input_all']['id'] as $key => $value) {
                    $data['results'][$i]['id']                  = $request['input_all']['id'][$key];
                    $data['results'][$i]['item_id']             = $request['input_all']['item_id'][$key];
                    $data['results'][$i]['item_name']           = $request['input_all']['item_name'][$key];
                    $data['results'][$i]['item_description']    = $request['input_all']['item_description'][$key];
                    $data['results'][$i]['quantity']            = $request['input_all']['quantity'][$key];
                    $data['results'][$i]['unit']                = $request['input_all']['unit'][$key];
                    $data['results'][$i]['sal_price']           = $request['input_all']['sal_price'][$key];
                    $data['results'][$i]['sal_amount']          = $request['input_all']['sal_amount'][$key];
                    $data['results'][$i]['cost_price']          = $request['input_all']['cost_price'][$key];
                    $data['results'][$i]['cost_amount']         = $request['input_all']['cost_amount'][$key];
                    $data['results'][$i]['tax_flg']             = $request['input_all']['tax_flg'][$key];
                    $data['results'][$i]['item_remarks']        = $request['input_all']['item_remarks'][$key];
                    $data['results'][$i]['no']                  = $request['input_all']['no'][$key];
                    $i++;
                }
            }
            return view('estimates.edit')->with(compact('data'))->withErrors($validator);
        }

        //入力画面へ遷移
        if (Input::get('back')) {
            return view('estimates.edit', compact('data'));
        }

        //確認画面へ遷移
        if (Input::get('confirm')) {
            $data['confirm'] = true;
            return view('estimates.index', compact('data'));
        }

        /* ---------------------------------------------------
         * 見積データの登録更新
          --------------------------------------------------- */
        //見積データ情報の取得
        $frfrf = $request['input_all']['seq_id'];
        $estimate = Estimate::find($request['input_all']['seq_id']);

        /* 新規インスタンスを生成する。
         * 条件1:見積データが存在しない場合
         * 条件2:"複写(新規)"モードの場合
         */
        if (is_null($estimate) || $data['mode']==1) {
            $estimate = new Estimate;
        }

        //見積番号採番
        if ( $data['mode']==1 ) {
            $mitumori_no = $this->getMakeEstiNo("");
        }else{
            $mitumori_no = $this->getMakeEstiNo($request['input_all']['esti_no']);
        }

        //編集内容格納
        $estimate->customer_id         = $request['input_all']['customer_id'];
        $estimate->customer_name       = $request['input_all']['customer_name'];
        $estimate->esti_no             = $mitumori_no;
        $estimate->estimate_date       = $request['input_all']['estimate_date'];
        $estimate->sales_cond          = $request['input_all']['sales_cond'];
        $estimate->title           	   = $request['input_all']['title'];
        $estimate->delivery_location   = $request['input_all']['delivery_location'];
        $estimate->expiration_date     = $request['input_all']['expiration_date'];
        $estimate->delivery_date       = $request['input_all']['delivery_date'];
        $estimate->pay_conditions      = $request['input_all']['pay_conditions'];
        $estimate->remarks             = $request['input_all']['remarks'];
        $estimate->total_amount_taxnon = $request['input_all']['total_amount_taxnon'];
        $estimate->tax_amount          = $request['input_all']['tax_amount'];
        $estimate->total_amount_taxin  = $request['input_all']['total_amount_taxin'];
        $estimate->total_cost_amount   = $request['input_all']['total_cost_amount'];
//        $estimate->tax_type            = $request['input_all']['tax_type'];
        $estimate->tax_rate            = $request['input_all']['tax_rate'];
        $estimate->round_type          = $request['input_all']['round_type'];
        $estimate->coments             = $request['input_all']['coments'];
        $estimate->staff_id            = $request['input_all']['staff_id'];
        $estimate->staff_name          = $user_list[$request['input_all']['staff_id']];

        if ( empty($request['input_all']['seq_id']) || $data['mode']==1 ) {
            //新規登録時
            $estimate->created_name   = Auth::user()->user_id;
        }
        $estimate->updated_name   = Auth::user()->user_id;

        //登録更新
        $estimate->save();


        /* ---------------------------------------------------
         * 見積明細データの登録更新
          --------------------------------------------------- */
        //論理削除時使用データの取得
        $query = Estimate_detail::query();
        $ff = $request['input_all']['seq_id'];
        $delMakeList = $query->where('esti_no', $request['input_all']['esti_no'])->get();

        $edits = $request['input_all']['item_name'];
        while(current($edits)) {

            //編集行取得
            $i = key($edits);

            //論理削除データ作成
            foreach ($delMakeList as $key => $value) {
                if ($value->id == $request['input_all']['id'][$i]) {
                    unset($delMakeList[$key]);
                }
            }

            /* 見積明細:新規インスタンスを生成する。
             * 条件1:見積明細データが存在しない場合
             * 条件2:"複写(新規)"モードの場合
             */
            $estimate_detail = Estimate_detail::find($request['input_all']['id'][$i]);
            if ( is_null($estimate_detail) || $data['mode']==1 ) {
                $estimate_detail = new Estimate_detail;
            }

            //編集内容格納
            $estimate_detail->item_id           = $request['input_all']['item_id'][$i];
            $estimate_detail->item_name         = $request['input_all']['item_name'][$i];
            $estimate_detail->item_description  = $request['input_all']['item_description'][$i];
            $estimate_detail->quantity          = $request['input_all']['quantity'][$i];
            $estimate_detail->unit              = $request['input_all']['unit'][$i];
            $estimate_detail->sal_price         = $request['input_all']['sal_price'][$i];
            $estimate_detail->sal_amount        = $request['input_all']['sal_amount'][$i];
            $estimate_detail->cost_price        = ( !empty($request['input_all']['cost_price'][$i]) )? $request['input_all']['cost_price'][$i]:0;
            $estimate_detail->cost_amount       = ( !empty($request['input_all']['cost_amount'][$i]) )? $request['input_all']['cost_amount'][$i]:0;
            $estimate_detail->tax_flg           = $request['input_all']['tax_flg'][$i];
            $estimate_detail->item_remarks      = $request['input_all']['item_remarks'][$i];
            $estimate_detail->esti_line_no      = $request['input_all']['no'][$i];
            $estimate_detail->esti_no           = $mitumori_no;

            if ( empty($request['input_all']['id'][$i]) || $data['mode']==1) {
                //新規登録時
                $estimate_detail->created_name   = Auth::user()->user_id;
            }
            $estimate_detail->updated_name   = Auth::user()->user_id;

            //登録更新
            $estimate_detail->save();

            //次編集行
            next($edits);
        }

        //論理削除
        if ( $data['mode'] != 1) {
            foreach ($delMakeList as $key => $value) {
                $this->del($value->id);
            }
        }

        \Session::flash('edit_success', '見積書を登録しました。');

        return redirect('estimates/edit');

    }

    /**
     * 論理削除処理
     *
     * @return Response
     */
    public function del($id)
    {
		if(!empty($id)){
            $sql = Estimate_detail::find($id);
			$sql->delete();
		}
        return redirect('estimates');
    }

    /**
     * 論理削除処理
     *
     * @return Response
     */
    public function close($id)
    {
		if(!empty($id)){
            //見積データ論理削除
            $sql = Estimate::find($id);
			$sql->delete();

            //見積明細データ論理削除
            $query = Estimate_detail::query();
            $query = $query->where('esti_no',$id)->delete();
		}
        return redirect('estimates');
    }

    /**
     * 共通フォーム
     *
     * @return Response
     */
    public function select()
    {
        $prms = Request::all();
        $page = is_null($prms['page'])?1:$prms['page'];

        //query
        $query = \App\Customer::query();

        //得意先名
        if (isset($prms['s_customer_name']) && !empty($prms['s_customer_name'])){
            $query->where('customer_name','like','%'.$prms['s_customer_name'].'%');
        }
        //得意先名カナ
        if (isset($prms['s_customer_kana']) && !empty($prms['s_customer_kana'])){
            $query->where('customer_kana','like','%'.$prms['s_customer_kana'].'%');
        }

        //総数取得
        $total= count($query->get());

        $query->orderBy('id');
        $query->offset($this->per_page * ($page - 1));
        $query->limit($this->per_page);

        $data= $query->get();

        $pagination['startPage'] = $page;
        $pagination['totalPages'] = ceil($total / $this->per_page);

        //例：検索結果： 1 - 20件 / 100件
        $info['showing'] = empty(count($data))? 0: ($this->per_page * ($page - 1) + 1) . ' - ' . ($this->per_page * ($page - 1) + count($data));
        $info['total']   = $total;

        return Response::json(array(
            'data' => $data,
            'info' => $info,
            'pagination' => $pagination,
        ));
    }

    /**
     * 見積書出力
     *
     * @return Response
     */
    public function xlsprint($out_type, $id)
    {
        $estimate = Estimate::find($id);

        $query      = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->count();
        $file = '/storage/app/public/estimate_detail_'.(string)$out_type.'.xlsx';

        Excel::load($file, function($doc) use ($out_type, $id, $est_detail, $estimate) {

            $doc->setTitle = 'test';

            //御見積書
            if ( $out_type == 1 )
            {

                /* ----------------------------------------
                 * 明細書ページを発行するかの条件
                 * "1" => "税抜", "2" => "税込", "3" => "非課税",
                 * 存在する　　 $this->est_taxin_rows 20件以上で発行する
                 * 存在しない　 $this->est_taxin_rows 21件以上で発行する
                 * ----------------------------------------*/
                //明細行に "税込み" 以外が存在するか
                $query = Estimate_detail::query();
                $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
                if ( $est_tax_flg_cnt > 0 ) {
                    //存在する　　 20件以上で明細書ページ発行する
                    $detail_rows = $this->est_rows;
                }else{
                    //存在しない　 21件以上で明細書ページ発行する
                    $detail_rows = $this->est_taxin_rows;
                }

                // Sheet1:御見積書
                $sheet = $this->sheet1($doc->setActiveSheetIndex(0), $id);

                // Sheet2:御見積書_明細書
                if ( $est_detail > $detail_rows) {
                    $sheet = $this->sheet2($doc->setActiveSheetIndex(1), $id);
                }
                $this->file_name = "御見積書";
            }

            //請書
            if ( $out_type == 2 )
            {
                // Sheet3:請書
                $sheet = $this->sheet3($doc->setActiveSheetIndex(0), $id);

                // Sheet4:請書_明細書
                if ( $est_detail > $this->uke_rows) {
                    $sheet = $this->sheet4($doc->setActiveSheetIndex(1), $id);
                }
                $this->file_name = "請書";
            }

            //納品書
            if ( $out_type == 3 )
            {
                // Sheet5:納品書(一般)
                $sheet = $this->sheet5($doc->setActiveSheetIndex(0), $id);

                // Sheet6:納品書(学校)
                $sheet = $this->sheet6($doc->setActiveSheetIndex(1), $id);

                // Sheet7:納品書_明細書
                if ( $est_detail > $this->nouhin_rows) {
                    $sheet = $this->sheet7($doc->setActiveSheetIndex(2), $id);
                }
                $this->file_name = "納品書";
            }

            //請求書
            if ( $out_type == 4 )
            {
                // Sheet8:請求書(一般)
                $sheet = $this->sheet8($doc->setActiveSheetIndex(0), $id);

                // Sheet9:請求書(学校)
                $sheet = $this->sheet9($doc->setActiveSheetIndex(1), $id);

                // Sheet10:請求書_明細書
                if ( $est_detail > $this->seikyu_rows) {
                    $sheet = $this->sheet10($doc->setActiveSheetIndex(2), $id);
                }
                $this->file_name = "請求書";
            }

        })
        ->setFilename( "【".date("Ymd_His")."_見積No.".$estimate->esti_no."】".$this->file_name)
        ->download('xlsx');

        $this->file_name = "";

        return redirect('estimates');
    }

    /**
     * Sheet1:御見積書
     *
     * @return Response
     */
    private function sheet1($sheet, $id)
    {
//        $id = Input::get('id');

        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        //見積NO：
        $sheet->setCellValue('AE3', "見積NO：".$estimate->esti_no);
        //日付：
        $sheet->setCellValue('AF4', date('Y年m月d日'));
        //会社：
        $sheet->setCellValue('B6', $estimate->customer_name);
        //名称：
        $sheet->setCellValue('H13', $estimate->title);
        //納入場所：
        $sheet->setCellValue('H14', $estimate->delivery_location);
        //有効期限：
        $sheet->setCellValue('H15', $estimate->expiration_date);
        //受渡期日：
        $sheet->setCellValue('H16', $estimate->delivery_date);
        //御支払条件：
        $sheet->setCellValue('H17', $estimate->pay_conditions);
        //備考：
        $sheet->setCellValue('H18', $estimate->remarks);
        //合計金額：
        $sheet->setCellValue('F23', "\\".number_format($estimate->total_amount_taxin));
        //消費税：
        $sheet->setCellValue('R23', "\\".number_format($estimate->tax_amount));
        //担当：
        $sheet->setCellValue('AF16', $estimate->staff_name);

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * "1" => "税抜", "2" => "税込", "3" => "非課税",
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
        $tax_flg = false;
        if ( $est_tax_flg_cnt > 0 ) {
            $tax_flg = true;
            $detail_rows = $this->est_rows;
        }else{
            $detail_rows = $this->est_taxin_rows;
        }

        if (count($est_detail) > $detail_rows) {
            $sheet->setCellValue('B26',  "別紙明細書のとおり");
            $sheet->setCellValue('U26',  "1");
            $sheet->setCellValue('W26',  "式");
            $sheet->setCellValue('Y26',  number_format($estimate->total_cost_amount));
            $sheet->setCellValue('AC26', number_format($estimate->total_amount_taxnon));
            $i = 27;

        }else{
            $iStart  = 26;
            $i       = 26;
            $line_no = 1;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $line_no);
                $sheet->setCellValue('B'.$i, $row->item_name);
                $sheet->setCellValue('L'.$i, $row->item_description);
                $sheet->setCellValue('U'.$i, $row->quantity);
                $sheet->setCellValue('W'.$i, $row->unit);
                $sheet->setCellValue('Y'.$i, $row->sal_price);
                $sheet->setCellValue('AC'.$i, $row->sal_amount);
                $sheet->setCellValue('AH'.$i, $row->item_remarks);

                $i++;
                $line_no++;
            }
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        if ( $tax_flg ) {

            //小計：
            $sheet->setCellValue('B'.$i, "小　計");
            $sheet->setCellValue('AC'.$i, number_format($estimate->total_amount_taxnon));
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('B'.($i+1), "消費税");
            $sheet->setCellValue('AC'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('B'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //合計：
            $sheet->setCellValue('B'.($i+2), "合　計");
            $sheet->setCellValue('AC'.($i+2), "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('B'.($i+2))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        }else{

            //税込合計：
            $sheet->setCellValue('B'.$i, "税 込 合 計");
            $sheet->setCellValue('AC'.$i, "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('B'.($i+1), "(内消費税)");
            $sheet->setCellValue('AC'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('B'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        }

        //フォントサイズ
        //現在のシートのデフォルトフォントを’ＭＳ ゴシック’、11ポイントとする
        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );
        return $sheet;
    }

    /**
     * Sheet2:御見積書_明細
     *
     * @return Response
     */
    private function sheet2($sheet, $id)
    {
        //Sheets:共通処理_明細書
        $this->commonEstimateDetails($sheet, $id);
    }

    /**
     * Sheet3:請書
     *
     * @return Response
     */
    private function sheet3($sheet, $id)
    {

        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        //見積情報
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }
        //得意先情報
        $customer = Customer::find($estimate->customer_id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        //契約の目的：
//        $sheet->setCellValue('N6', '??????です1');
        //納入又は引渡の場所：
        $sheet->setCellValue('N7', $estimate->delivery_location);
        //納入または引渡の期限：
//        $sheet->setCellValue( 'N8', $estimate->expiration_date);
//        $sheet->setCellValue('C40', $estimate->expiration_date);
        //納入又は引渡の方法：
//        $sheet->setCellValue('N9', '??????です2');
        //契約金額：
        $sheet->setCellValue('O10', number_format($estimate->total_amount_taxin));
        //消費税：
        $sheet->setCellValue('AE11', "\\".number_format($estimate->tax_amount));
        //会社：
        $sheet->setCellValue('I46', $customer->representative_name."　　　殿");
        //契約保証金額：
//        if ( $estimate->total_amount_taxin > 200000 && $estimate->total_amount_taxin < 500000 ) {
        if ( $estimate->total_amount_taxin < 500000 ) {
            $sheet->setCellValue('N16', '財務規則第101条第2項第6号により免除');
        }elseif ( $estimate->total_amount_taxin >= 500000 ) {
            $sheet->setCellValue('N16', '財務規則第101条第2項第3号により免除');
        }

////↓画像の挿入処理
//$objDrawing = new PHPExcel_Worksheet_Drawing();
//$objDrawing->setPath('assets/imgs/circle.png');///貼り付ける画像のパスを指定
//$objDrawing->setWidth(51);////画像の幅を指定
//$objDrawing->setHeight(54);////画像の高さを指定
/////画像のプロパティを見たときに表示される情報を設定
////$objDrawing->setName('Arrow');////ファイル名
////$objDrawing->setDescription('Arrow');////画像の概要
//$objDrawing->setCoordinates('C46');///位置(セル座標)
////$objDrawing->setOffsetX(50);////横方向へ何ピクセルずらすかを指定
////$objDrawing->setOffsetY(10);////縦方向へ何ピクセルずらすかを指定
////$objDrawing->setRotation(25);//回転の角度
////$objDrawing->getShadow()->setVisible(false);////ドロップシャドウをつけるかどうか。
/////PHPExcelオブジェクトに貼り込み
//$objDrawing->setWorksheet($sheet);

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        if (count($est_detail) > $this->uke_rows) {
            $sheet->setCellValue('A19', $estimate->title);
            $sheet->setCellValue('A20', "別紙明細書のとおり");
            $sheet->setCellValue('Q20', "1");
            $sheet->setCellValue('V20',  number_format($estimate->total_amount_taxnon));
            $sheet->setCellValue('AB20', number_format($estimate->total_amount_taxnon));
        }else{
            $iStart=19;
            $i     =19;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $row->item_name);
                $sheet->setCellValue('L'.$i, $row->item_description);
                $sheet->setCellValue('Q'.$i, $row->quantity);
                $sheet->setCellValue('V'.$i, $row->sal_price);
                $sheet->setCellValue('AB'.$i, $row->sal_amount);
                $sheet->setCellValue('AI'.$i, $row->item_remarks);
                $i++;
            }
        }

        //小計：
        $sheet->setCellValue('A24', "小　計");
        $sheet->setCellValue('AB24', number_format($estimate->total_amount_taxnon));
        //消費税：
        $sheet->setCellValue('A25', "消費税");
        $sheet->setCellValue('AB25', number_format($estimate->tax_amount));
        //合計：
        $sheet->setCellValue('A26', "合　計");
        $sheet->setCellValue('AB26', "\\  ".number_format($estimate->total_amount_taxin));

        //フォントサイズ
//        $sheet->getStyleByColumnAndRow(0, 17)->getFont()->setSize(20);
        //現在のシートのデフォルトフォントを’ＭＳ ゴシック’、11ポイントとする
        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );

        return $sheet;
    }

    /**
     * Sheet4:御見積書_明細
     *
     * @return Response
     */
    private function sheet4($sheet, $id)
    {
        //Sheets:共通処理_明細書
        $this->commonEstimateDetails($sheet, $id);
    }

    /**
     * Sheet5:納品書(一般)
     *
     * @return Response
     */
    private function sheet5($sheet, $id)
    {

        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        //日付：
//        $sheet->setCellValue('A6', $this->convGtJDate( date("Y/m/d") ) );
        //会社：
        $sheet->setCellValue('B8', $estimate->customer_name);
        //件名：
        $sheet->setCellValue('E12', $estimate->title);

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * "1" => "税抜", "2" => "税込", "3" => "非課税",
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
        $tax_flg = false;
        if ( $est_tax_flg_cnt > 0 ) {
            $tax_flg = true;
            $detail_rows = $this->est_rows;
        }else{
            $detail_rows = $this->est_taxin_rows;
        }

        if (count($est_detail) > $this->nouhin_rows) {
            $sheet->setCellValue('A14', $estimate->title);
            $sheet->setCellValue('A15',  "別紙明細書のとおり");
            $sheet->setCellValue('T15',  "1");
            $sheet->setCellValue('V15',  "式");
            $sheet->setCellValue('X15',  number_format($estimate->total_cost_amount));
            $sheet->setCellValue('AB15', number_format($estimate->total_amount_taxnon));
            $i = 16;

        }else{
            $iStart=14;
            $i     =14;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $row->item_name);
                $sheet->setCellValue('K'.$i, $row->item_description);
                $sheet->setCellValue('T'.$i, $row->quantity);
                $sheet->setCellValue('V'.$i, $row->unit);
                $sheet->setCellValue('X'.$i, $row->sal_price);
                $sheet->setCellValue('AB'.$i, $row->sal_amount);
                $sheet->setCellValue('AG'.$i, $row->item_remarks);
                $i++;
            }
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        if ( $tax_flg ) {

            //小計：
            $sheet->setCellValue('A'.$i, "小　計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxnon));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "消費税");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //合計：
            $sheet->setCellValue('A'.($i+2), "合　計");
            $sheet->setCellValue('AB'.($i+2), "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.($i+2))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }else{

            //小計：
            $sheet->setCellValue('A'.$i, "税 込 合 計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "(内消費税)");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );

        return $sheet;
    }

    /**
     * Sheet6:納品書(学校)
     *
     * @return Response
     */
    private function sheet6($sheet, $id)
    {

        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        //日付：
//        $sheet->setCellValue('A6', $this->convGtJDate( date("Y/m/d") ) );
        //会社：
        $sheet->setCellValue('B8', $estimate->customer_name);
        //件名：
        $sheet->setCellValue('E12', $estimate->title);

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * "1" => "税抜", "2" => "税込", "3" => "非課税",
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
        $tax_flg = false;
        if ( $est_tax_flg_cnt > 0 ) {
            $tax_flg = true;
            $detail_rows = $this->est_rows;
        }else{
            $detail_rows = $this->est_taxin_rows;
        }

        if (count($est_detail) > $this->nouhin_rows) {
            $sheet->setCellValue('A14', $estimate->title);
            $sheet->setCellValue('A15',  "別紙明細書のとおり");
            $sheet->setCellValue('T15',  "1");
            $sheet->setCellValue('V15',  "式");
            $sheet->setCellValue('X15',  number_format($estimate->total_cost_amount));
            $sheet->setCellValue('AB15', number_format($estimate->total_amount_taxnon));
            $i = 16;

        }else{
            $iStart=14;
            $i     =14;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $row->item_name);
                $sheet->setCellValue('K'.$i, $row->item_description);
                $sheet->setCellValue('T'.$i, $row->quantity);
                $sheet->setCellValue('V'.$i, $row->unit);
                $sheet->setCellValue('X'.$i, $row->sal_price);
                $sheet->setCellValue('AB'.$i, $row->sal_amount);
                $sheet->setCellValue('AG'.$i, $row->item_remarks);
                $i++;
            }
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        if ( $tax_flg ) {
            //小計：
            $sheet->setCellValue('A'.$i, "小　計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxnon));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "消費税");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //合計：
            $sheet->setCellValue('A'.($i+2), "合　計");
            $sheet->setCellValue('AB'.($i+2), "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.($i+2))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }else{

            //小計：
            $sheet->setCellValue('A'.$i, "税 込 合 計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "(内消費税)");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );

        return $sheet;
    }

    /**
     * Sheet7:納品書_明細
     *
     * @return Response
     */
    private function sheet7($sheet, $id)
    {
        //Sheets:共通処理_明細書
        $this->commonEstimateDetails($sheet, $id);
    }

    /**
     * Sheet8:請求書(一般)
     *
     * @return Response
     */
    private function sheet8($sheet, $id)
    {
        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }
        $customer = Customer::find($estimate->customer_id);
        if (is_null($customer)) {
            $customer = new Customer;
        }

        //郵便番号：
        $sheet->setCellValue('B3', $customer->zip);
        //住所：
        $sheet->setCellValue('B4', $customer->address);
        //見積NO：
        $sheet->setCellValue('AH3', $estimate->esti_no);
        //日付：
        $sheet->setCellValue('AF4', $this->convGtJDate( date("Y/m/d") ) );
        //会社：
        $sheet->setCellValue('B6', $estimate->customer_name);
        //名称：
        $sheet->setCellValue('H13', $estimate->title);
        //備考：
        $sheet->setCellValue('H14', $estimate->remarks);
        //合計金額：
        $sheet->setCellValue('G21', "\\".number_format($estimate->total_amount_taxin));
        //消費税：
        $sheet->setCellValue('Q21', "\\".number_format($estimate->tax_amount));

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * "1" => "税抜", "2" => "税込", "3" => "非課税",
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
        $tax_flg = false;
        if ( $est_tax_flg_cnt > 0 ) {
            $tax_flg = true;
            $detail_rows = $this->est_rows;
        }else{
            $detail_rows = $this->est_taxin_rows;
        }

        if (count($est_detail) > $this->seikyu_rows) {
            $sheet->setCellValue('A24', $estimate->title);
            $sheet->setCellValue('A25',  "別紙明細書のとおり");
            $sheet->setCellValue('T25',  "1");
            $sheet->setCellValue('V25',  "式");
            $sheet->setCellValue('X25',  number_format($estimate->total_cost_amount));
            $sheet->setCellValue('AB25', number_format($estimate->total_amount_taxnon));
            $i = 26;

        }else{
            $iStart=24;
            $i     =24;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $row->item_name);
                $sheet->setCellValue('K'.$i, $row->item_description);
                $sheet->setCellValue('T'.$i, $row->quantity);
                $sheet->setCellValue('V'.$i, $row->unit);
                $sheet->setCellValue('X'.$i, $row->sal_price);
                $sheet->setCellValue('AB'.$i, $row->sal_amount);
                $sheet->setCellValue('AG'.$i, $row->item_remarks);
                $i++;
            }
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        if ( $tax_flg ) {
            //小計：
            $sheet->setCellValue('A'.$i, "小　計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxnon));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "消費税");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //合計：
            $sheet->setCellValue('A'.($i+2), "合　計");
            $sheet->setCellValue('AB'.($i+2), "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.($i+2))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }else{

            //小計：
            $sheet->setCellValue('A'.$i, "税 込 合 計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "(内消費税)");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        //現在のシートのデフォルトフォントを’ＭＳ ゴシック’、11ポイントとする
        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );

        return $sheet;
    }

    /**
     * Sheet9:請求書(学校)
     *
     * @return Response
     */
    private function sheet9($sheet, $id)
    {
        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        //件名：
        $sheet->setCellValue('E5', $estimate->title);
        //合計金額：
        $sheet->setCellValue('Y5', "\\".number_format($estimate->total_amount_taxin));
        //日付：
//        $sheet->setCellValue('C32', $this->convGtJDate( date("Y/m/d") ) );
        //会社：
        $sheet->setCellValue('D39', $estimate->customer_name);

        /* ----------------------------------------
         * 明細データを取得しセット
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * "1" => "税抜", "2" => "税込", "3" => "非課税",
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        $est_tax_flg_cnt = count( $query->whereRaw('esti_no = ? and tax_flg <> ?', array($estimate->esti_no, 2))->get() );
        $tax_flg = false;
        if ( $est_tax_flg_cnt > 0 ) {
            $tax_flg = true;
            $detail_rows = $this->est_rows;
        }else{
            $detail_rows = $this->est_taxin_rows;
        }

        if (count($est_detail) > $this->seikyu_rows) {
            $sheet->setCellValue('A8', $estimate->title);
            $sheet->setCellValue('A9',  "別紙明細書のとおり");
            $sheet->setCellValue('T9',  "1");
            $sheet->setCellValue('V9',  "式");
            $sheet->setCellValue('X9',  number_format($estimate->total_cost_amount));
            $sheet->setCellValue('AB9', number_format($estimate->total_amount_taxnon));
            $i = 10;

        }else{
            $iStart=8;
            $i     =8;
            foreach ($est_detail as $row) {
                $sheet->setCellValue('A'.$i, $row->item_name);
                $sheet->setCellValue('K'.$i, $row->item_description);
                $sheet->setCellValue('T'.$i, $row->quantity);
                $sheet->setCellValue('V'.$i, $row->unit);
                $sheet->setCellValue('X'.$i, $row->sal_price);
                $sheet->setCellValue('AB'.$i, $row->sal_amount);
                $sheet->setCellValue('AG'.$i, $row->item_remarks);
                $i++;
            }
        }

        /* ----------------------------------------
         * 明細行に "税込み" 以外が存在するか
         * $tax_flg = true  ：存在する　　小計：消費税：合計
         * $tax_flg = false ：存在しない　税込み合計：(消費税)
         * ----------------------------------------*/
        if ( $tax_flg ) {

            //小計：
            $sheet->setCellValue('A'.$i, "小　計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxnon));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "消費税");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //合計：
            $sheet->setCellValue('A'.($i+2), "合　計");
            $sheet->setCellValue('AB'.($i+2), "\\  ".number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.($i+2))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }else{

            //小計：
            $sheet->setCellValue('A'.$i, "税 込 合 計");
            $sheet->setCellValue('AB'.$i, number_format($estimate->total_amount_taxin));
            $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //消費税：
            $sheet->setCellValue('A'.($i+1), "(内消費税)");
            $sheet->setCellValue('AB'.($i+1), number_format($estimate->tax_amount));
            $sheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //現在のシートのデフォルトフォントを’ＭＳ ゴシック’、11ポイントとする
        $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ Ｐ明朝' );

        return $sheet;
    }

    /**
     * Sheet10:請求書_明細
     *
     * @return Response
     */
    private function sheet10($sheet, $id)
    {
        //Sheets:共通処理_明細書
        $this->commonEstimateDetails($sheet, $id);
    }

    /**
     * Sheets:共通処理_明細書
     *
     * @return Response
     */
    private function commonEstimateDetails($sheet, $id)
    {
        /* ----------------------------------------
         * 見積データを取得しセット
         * ----------------------------------------*/
        $estimate = Estimate::find($id);
        if (is_null($estimate)) {
            $estimate = new Estimate;
        }

        /* ----------------------------------------
         * 明細データ取得
         * ----------------------------------------*/
        $query = Estimate_detail::query();
        $est_detail = $query->where('esti_no', $estimate->esti_no)->orderby('esti_line_no', 'asc')->get();
        if (is_null($est_detail)) {
            $est_detail = new Estimate_detail;
        }

        $sheet->setBreak('A37', \PHPExcel_Worksheet::BREAK_ROW)
                ->getPageSetup()
                ->setFitToPage(true)
                ->setFitToWidth(1)   /* 幅を1ページに収める */
                ->setFitToHeight(0); /* 高さを1ページに収める */

        $iStart=6;
        $i=6;
        $j=0; //空行追加用
        $line_no=1;
        foreach ($est_detail as $row) {
            $sheet->setCellValue( 'A'.$i, $line_no);
            $sheet->setCellValue( 'B'.$i, $row->item_name);
            $sheet->setCellValue( 'K'.$i, $row->item_description);
            $sheet->setCellValue( 'T'.$i, $row->quantity);
            $sheet->setCellValue( 'W'.$i, $row->unit);
            $sheet->setCellValue( 'Z'.$i, $row->sal_price);
            $sheet->setCellValue('AE'.$i, $row->sal_amount);
            $sheet->setCellValue('AK'.$i, $row->item_remarks);

            //エクセル - フォーマットセルスタイルの追加
            $this->addCellTyle($i, $iStart, $sheet);

            $i++;   //データセットセルのカウント
            $j++;   //空行追加のためのカウント
            $line_no++;

            //空行セルスタイル追加のための条件
            if ( ($j % 32) == 0 ) {
                $j = 0;
            }
        }

        //空行のセルスタイルの追加
        for($j; $j<32; $j++) {
            //エクセル - フォーマットセルスタイルの追加
            $this->addCellTyle($i, $iStart, $sheet);
            $i++;
        }

        $i = $i - 1;
        //小計：
        $sheet->setCellValue('B'.$i, "小　計");
        $sheet->setCellValue('AE'.$i, number_format($estimate->total_amount_taxnon));
        //セル右揃え
        $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('AE'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        //セル下揃え
        $sheet->getStyle('AE'.$i)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
        //セルフォーマット
        $sheet->getStyle('AE'.$i)->getNumberFormat()->setFormatCode( '#,##0' );

        //印刷範囲
        $sheet->getPageSetup()->setPrintArea('A:AO');

        return $sheet;
    }

    /**
     * 明細書エクセル - フォーマットセルスタイルの追加
     *
     * @return Response
     */
    public function addCellTyle($i, $iStart, $sheet)
    {

        //セルを格子状の罫線で囲む
        $sheet->getStyle('A'.$iStart.':'.'AO'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //フォントサイズ
        $sheet->getStyle('A'.$iStart.':'.'AK'.$i)->getFont()->setSize(10);

        //行高さ
        $sheet->getRowDimension( $i )->setRowHeight( 26.25 );

        //セル中央揃え
        $sheet->getStyle( 'W'.$i )->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //単位

        //セル左揃え
        $sheet->getStyle( 'AK'.$i )->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //備考

        //セル下揃え
        $sheet->getStyle('A'.$iStart.':'.'AK'.$i)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);

        //セルフォーマット
        $sheet->getStyle('T'.$i)->getNumberFormat()->setFormatCode( '#,##0' );
        $sheet->getStyle('Z'.$i)->getNumberFormat()->setFormatCode( '#,##0' );
        $sheet->getStyle('AE'.$i)->getNumberFormat()->setFormatCode( '#,##0' );

        //セル折り返し

        //セルの結合
        $sheet->mergeCells('B'.$i.':'.'J'.$i);
        $sheet->mergeCells('K'.$i.':'.'S'.$i);
        $sheet->mergeCells('T'.$i.':'.'V'.$i);
        $sheet->mergeCells('W'.$i.':'.'Y'.$i);
        $sheet->mergeCells('Z'.$i.':'.'AD'.$i);
        $sheet->mergeCells('AE'.$i.':'.'AJ'.$i);
        $sheet->mergeCells('AK'.$i.':'.'AO'.$i);

    }

    /**
     * CSVエクスポート--見積一覧
     *
     * @return Response
     */
    public function downlist()
    {
        $prms = Request::all();
        $datas = $this->get_estimate_search_list($prms, null);

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=【".date("Ymd_His")."】"."見積一覧.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use($datas) {
            $handle = fopen('php://output', 'w');

//            $columns = [
//                'id',
//                'customer_id',
//            ] ;
            $columns = Schema::getColumnListing("estimates");
            mb_convert_variables('SJIS-win', 'UTF-8', $columns);

            fputcsv($handle, $columns);

//            $datas = Estimate::all();
            foreach ($datas as $data) {
                $csv = [
                    $data->id,
                    $data->customer_id,
                    $data->customer_name,
                    $data->esti_no,
                    $data->usr_esti_no,
                    $data->estimate_date,
                    $data->sales_cond,
                    $data->title,
                    $data->delivery_location,
                    $data->expiration_date,
                    $data->delivery_date,
                    $data->pay_conditions,
                    $data->remarks,
                    $data->total_amount_taxnon,
                    $data->tax_amount,
                    $data->total_amount_taxin,
                    $data->total_cost_amount,
                    $data->tax_type,
                    $data->tax_rate,
                    $data->round_type,
                    $data->coments,
                    $data->staff_id,
                    $data->staff_name,
                    $data->created_name,
                    $data->updated_name,
                    $data->created_at,
                    $data->updated_at,
                    $data->deleted_at,
                ];
                mb_convert_variables('SJIS-win', 'UTF-8', $csv);
                fputcsv($handle, $csv);
            }

            fclose($handle);
        };

      return response()->stream($callback, 200, $headers);
    }

    /**
     * CSVエクスポート--見積明細
     *
     * @return Response
     */
    public function downdetail($id)
    {
        $estimate = Estimate::find($id);
        $fff = $estimate->esti_no;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=【".date("Ymd_His")."_見積No.".$estimate->esti_no."】"."見積明細.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use ($id) {
            $handle = fopen('php://output', 'w');

            /* ---------------------------------
             * (ヘッダー行の作成)出力カラムの編集
             * --------------------------------- */
            //estimatesテーブルカラム名の取得
            $columns_estimates = Schema::getColumnListing("estimates");
            //estimate_detailsテーブルカラム名の取得
            $columns_details = Schema::getColumnListing("estimate_details");
            //カラムの連結
            $columns = array_merge($columns_estimates, $columns_details);
            mb_convert_variables('SJIS-win', 'UTF-8', $columns);
            //CSVヘッダーの作成
            fputcsv($handle, $columns);

            /* ---------------------------------
             * データ取得
             * --------------------------------- */
            //見積伝票情報データ
            $querys = Estimate::query();
            $datas_esti = $querys->where('id', $id)->get();
            //明細行データ
            $query = Estimate_detail::query();
            $datas_deta = $query->where('esti_no', $datas_esti[0]->esti_no)->get();

            //値のセット
            foreach ($datas_deta as $data)
            {
                $csv = [
                    $datas_esti[0]->id,
                    $datas_esti[0]->customer_id,
                    $datas_esti[0]->customer_name,
                    $datas_esti[0]->esti_no,
                    $datas_esti[0]->usr_esti_no,
                    $datas_esti[0]->estimate_date,
                    $datas_esti[0]->sales_cond,
                    $datas_esti[0]->title,
                    $datas_esti[0]->delivery_location,
                    $datas_esti[0]->expiration_date,
                    $datas_esti[0]->delivery_date,
                    $datas_esti[0]->pay_conditions,
                    $datas_esti[0]->remarks,
                    $datas_esti[0]->total_amount_taxnon,
                    $datas_esti[0]->tax_amount,
                    $datas_esti[0]->total_amount_taxin,
                    $datas_esti[0]->total_cost_amount,
                    $datas_esti[0]->tax_type,
                    $datas_esti[0]->tax_rate,
                    $datas_esti[0]->round_type,
                    $datas_esti[0]->coments,
                    $datas_esti[0]->staff_id,
                    $datas_esti[0]->staff_name,
                    $datas_esti[0]->created_name,
                    $datas_esti[0]->updated_name,
                    $datas_esti[0]->created_at,
                    $datas_esti[0]->updated_at,
                    $datas_esti[0]->deleted_at,
                    $data->id,
                    $data->esti_no,
                    $data->usr_esti_no,
                    $data->esti_line_no,
                    $data->item_id,
                    $data->item_name,
                    $data->item_description,
                    $data->quantity,
                    $data->unit,
                    $data->sal_price,
                    $data->sal_amount,
                    $data->cost_price,
                    $data->cost_amount,
                    $data->tax_flg,
                    $data->item_remarks,
                    $data->created_name,
                    $data->updated_name,
                    $data->created_at,
                    $data->updated_at,
                    $data->deleted_at,                ];
                mb_convert_variables('SJIS-win', 'UTF-8', $csv);
                fputcsv($handle, $csv);
            }

            fclose($handle);
        };

      return response()->stream($callback, 200, $headers);
    }

    /**
     * 見積No"esti_no"生成
     *
     * @return Response
     */
	private function getMakeEstiNo($id)
    {
		$val = $id;

        //新規登録
		if ($id == "") {
			//最大IDの取得
			$result = DB::table('estimates')->orderBy('esti_no', 'desc')->take(1)->get();
			if (!empty($result[0])) {
				//esti_noの格納
				$getId = $result[0]->esti_no;
				//0埋め
				$val = $getId + 1;
			}else{
				$val = 80;
			}
		}

		return $val;

	}

    /**
     * 西暦⇔和暦変換
     *
     * @return Response
     */
    function convGtJDate($src)
    {
        list($year, $month, $day) = explode('/', $src);
        if (!@checkdate($month, $day, $year) || $year < 1869 || strlen($year) !== 4
                || strlen($month) !== 2 || strlen($day) !== 2) return false;
        $date = str_replace('/', '', $src);
        if ($date >= 19890108) {
            $gengo = '平成';
            $wayear = $year - 1988;
        } elseif ($date >= 19261225) {
            $gengo = '昭和';
            $wayear = $year - 1925;
        } elseif ($date >= 19120730) {
            $gengo = '大正';
            $wayear = $year - 1911;
        } else {
            $gengo = '明治';
            $wayear = $year - 1868;
        }
        switch ($wayear) {
            case 1:
                $wadate = $gengo.'元年'.$month.'月'.$day.'日';
                break;
            default:
                $wadate = $gengo.sprintf("%02d", $wayear).'年'.$month.'月'.$day.'日';
        }
        return $wadate;
    }

    /**
     * 西暦⇔和暦変換
     *
     * @return Response
     */
    function convJtGDate($src)
    {
        $a = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $g = mb_substr($src, 0, 2, 'UTF-8');
        array_unshift($a, $g);
        if (($g !== '明治' && $g !== '大正' && $g !== '昭和' && $g !== '平成')
                || (str_replace($a, '', $src) !== '年月日' && str_replace($a, '', $src) !== '元年月日')) return false;
        $y = strtok(str_replace($g, '', $src), '年月日');
        $m = strtok('年月日');
        $d = strtok('年月日');
        if (mb_strpos($src, '元年') !== false) $y = 1;
        if ($g === '平成') $y += 1988;
        elseif ($g === '昭和') $y += 1925;
        elseif ($g === '大正') $y += 1911;
        elseif ($g === '明治') $y += 1868;
        if (strlen($y) !== 4 || strlen($m) !== 2 || strlen($d) !== 2 || !@checkdate($m, $d, $y)) return false;
        return $y.'/'.$m.'/'.$d;
    }
}
