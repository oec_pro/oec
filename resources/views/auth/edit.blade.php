@extends('layouts.app')

@section('content')

<!-- container -->
<div class="navi_sub">
    
    <!-- navi_sub_left -->
    <div class="navi_sub_left">
        <label class="clear"></label>
    </div><!-- navi_sub_left -->
    
    <div class="clear"></div>
</div><!-- container -->

<!-- container_page -->
<div class="container_page">
    
    <div class="title_box">
        <div class="title_left">
            <h3><b>プロパティ</b></h3>
        </div>
        <div class="title_right">
        </div>
        <div class="clear"></div>
        <hr>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">変更</div>

                <div class="panel-body">
                    {!! Form::open(['url' => 'users/update', 'class' => 'form-horizontal form-label-left']) !!}    
                        {{ csrf_field() }}
                        {!! Form::hidden('id', $user_info['id']) !!}

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="user_name" class="col-md-4 control-label">ユーザー名</label>
                            @if ( !isset($user_info['confirm']) )
                                <div class="col-md-6">
                                    <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $user_info['user_name'] }}">
                                    <span class="help-block">{{$errors->first('user_name')}}</span>
                                </div>
                            @else
                                <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                                    {{ $user_info['user_name'] }}
                                    {!! Form::hidden('user_name', $user_info['user_name']) !!}
                                </div>
                            @endif
                        </div>

{{--
                        <div class="form-group">
                            <label class="col-md-4 control-label">現在のパスワード</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                                <br>
                            </div>
                        </div>
--}}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">新しいパスワード</label>
                            @if ( !isset($user_info['confirm']) )
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                    <span class="help-block">{{$errors->first('password')}}</span>
                                </div>
                            @else
                                <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                                    {{ !empty($user_info['password']) ? '***************' : '' }}
                                    {!! Form::hidden('password', $user_info['password']) !!}
                                </div>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">新しいパスワード(確認)</label>
                            @if ( !isset($user_info['confirm']) )
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                    <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                                    <br>
                                </div>
                            @else
                                <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                                    {{ !empty($user_info['password_confirmation']) ? '***************' : '' }}
                                    {!! Form::hidden('password_confirmation', $user_info['password_confirmation']) !!}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @if ( !isset($user_info['confirm']) )
                                    {!! Form::submit("確認", ['class' => 'btn btn-primary regist_del_btn', 'name' => 'confirm']) !!}
                                @else
                                    {!! Form::submit(is_numeric($user_info['id'])? '変更': '登録', ['class' =>'btn btn-primary regist_del_btn', 'name' => 'insert']) !!}
                                    {!! Form::submit('入力画面に戻る', ['class' => 'btn btn-default', 'name' => 'back']) !!}
                                @endif
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
        
</div>
<!-- /container_page -->


@endsection