@extends('layouts.app')

@section('content')
	<div class="navi_sub">
		<div class="navi_sub_left">
			<a id="new-taxrates" class="btn btn-success btn-sm" href="{{ action('TaxratesController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
				<i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
			</a>
			<a id="list" class="btn btn-success btn-sm" href="{{ action('TaxratesController@index') }}" role="button" data-toggle="tooltip" title="税率一覧を表示">
				<i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
			</a>
		</div>
		<div class="clear"></div>
	</div>

<!-- container_page -->
<div class="container_page">
    
    <div class="title_box">
        <div class="title_left">
            <h3><b>税率一覧</b></h3>
        </div>
        <div class="title_right">
        </div>
        <div class="clear"></div>
        <hr>
    </div>

    <!-- panel-body --> 
    <div class="panel-body">
        
        <!-- panel-group --> 
		<div class="panel-group" id="" role="tablist" aria-multiselectable="true">
                
            <!-- order_detail_info_wrap --> 
            <div id="order_detail_info_wrap">
                @if (count($data['results']) >= 1)
                    <table summary="年号一覧" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame estimate_list">
                        <thead>
                            <tr class="headings">
                                <th class="detail_title">ID</th>
                                <th class="detail_title">施行日</th>
                                <th class="detail_title">税率</th>
                                <th class="detail_title"></th>
                            </tr>
                        </thead>
                        <tbody id="tbodyLine">
                            @foreach($data['results'] as $result)
                                <tr id="trLine0">
                                    <td class="contents_box_center">{{ $result->id }}</td>
                                    <td class="contents_box_center">{{ date('Y年m月d日',  strtotime($result->effective_date)) }}</td>
                                    <td class="contents_box_center">{{ $result->tax_rate }}</td>
                                    <td class="contents_box_center">
                                        {!! link_to(action('TaxratesController@edit', [$result->id]), '変更', ['class' => 'btn btn-info', 'id' => 'test_update_'.$result->id]) !!}
                                        <button id = "test_del_{{ $result->id }}" type="button" class="btn btn-danger" onClick="confirmDel({{ $result->id }});return false;">削除</button>
                                    </td>
                                </tr><!-- trLine0 -->
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data['results']->links() }}
                @elseif(!is_null($data['results']))
                    <div class="alert alert-info">該当する情報がありませんでした。</div>
                @endif
            </div><!-- order_detail_info_wrap -->
        </div><!-- panel-group -->
    </div>　<!-- panel-body  -->
        
</div>
<!-- /container_page -->

<!-- sweetalert -->
<link rel="stylesheet" href="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function confirmDel(id) {
        swal({
            title: "削除確認",
            text: "削除してもよろしいですか？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "削除",
            cancelButtonText: "キャンセル",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/taxrates/" + "del/" + id, '_self');
                return true;
            } else {
                return false;
            }
        }); 
    }
</script>
@endsection