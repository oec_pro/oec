<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\DatePicker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Taxrate;
use App\User;

/**
 * 税率に関するテスト
 */
class TaxrateTest extends DuskTestCase
{
    // Dusk実行前にマイグレーションする
    use DatabaseMigrations;
    
    /**
     * 税率に関する操作(作成、編集、削除)のテスト
     *
     * @return void
     */
    public function testCRUD()
    {
        // ユーザーを作成
        $user = factory(User::class)->create([
            'user_id'   => 'onaga',
            'user_name' => 'ほげほげ',
        ]);
        
        // 税率の登録する内容
        $taxrate = factory(Taxrate::class)->make([
            'id' => 1,
            'effective_date' => '2017/12/25',
            'tax_rate' => '3',
        ]);
        
        // 編集する内容
        $update = factory(Taxrate::class)->make([
            'id' => 1,
            'effective_date' => '2017/12/25',
            'tax_rate' => '33',
        ]);

        $this->browse(function (Browser $browser) use ($user, $taxrate, $update) {
        $browser->visit('/login')                   // ログインページへ移動
                ->type('user_id', $user->user_id)   // ユーザーIDを入力
                ->type('password', 'secret')        // パスワードを入力
                ->press('Login')                    // 送信ボタンをクリック
                ->assertPathIs('/estimates')        // 見積一覧であることを確認
                ->press('#menu')                    // 「MENU」ボタンを押下
                ->pause(1000)
                ->press('#master')                  // 「マスタ管理」ボタンを押下
                ->pause(1000)
                ->press('#taxrates')                // 「税率マスタ」ボタンを押下
                ->assertPathIs('/taxrates')         // 税率マスタ一覧であることを確認
                /* --------------------------------
                 * 税率新規登録 
                 *--------------------------------- */
                ->press('#new-taxrates')                             // 「新規登録」ボタンを押下
                ->assertPathIs('/taxrates/edit')                     // 税率登録ページであることを確認
                ->press('#btnDate')                                  // 「カレンダー」ボタンを押下
                //->press('active day')
                //->type('effective_date', $taxrate->effective_date) // 施行日を入力
//                ->within(new DatePicker, function ($browser) {
//                    $browser->selectDate(1, 2018);
//                })
//                ->assertSee('January')            
                ->type('tax_rate', $taxrate->tax_rate)               // 税率を入力
                ->press('confirm')                                   // 「確認」ボタンを押下
                ->assertPathIs('/taxrates/save')                     // 確認ページであることを確認
                ->press('insert')                                    // 「新規登録」ボタンを押下
                ->assertPathIs('/taxrates')                          // 税率マスタ一覧であることを確認
                /* --------------------------------
                 * 税率編集
                 *--------------------------------- */
                ->press('#test_update_'.$taxrate->id)                     // 「変更」ボタンを押す
                ->assertPathIs('/taxrates/edit/'.$taxrate->id)
                ->press('#btnDate')                                  // 「カレンダー」ボタンを押下
                //->press('active day')
                //->type('effective_date', $taxrate->effective_date) // 施行日を入力
                ->type('tax_rate', $update->tax_rate)                // 税率を入力
                ->press('confirm')                                   // 「確認」ボタンを押下
                ->assertPathIs('/taxrates/save')                     // 確認ページであることを確認
                ->press('insert')                                    // 「新規登録」ボタンを押下
                ->assertPathIs('/taxrates')                          // 税率マスタ一覧であることを確認
//                    ->press('submit')
//                    ->assertPathIs('/posts/'.$post->id)
//                    ->assertSeeIn('#post-title', $update->title)
//                    ->assertSeeIn('#post-body', $update->body)
                // 削除
                ->press('#test_del_'.$taxrate->id)                   // 「削除」ボタンを押す
                ->press('#test_confirm')                             // ポップアップ画面「削除」ボタンを押す
                ->assertPathIs('/taxrates');                         // 税率マスタ一覧であることを確認
        });
    }    
    
    /**
     * バリデーションの動作を確認する (createアクション)
     *
     * @return void
     */
    public function testValidationCreate()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 作成のみを行う。保存はしない
        $taxrate = factory(Taxrate::class)->make([
//            'effective_date' => '',
            'tax_rate'        => '',
            'tax_rate_100'    => 100,
            'tax_rate_string' => '文字',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $taxrate) {
            $browser->loginAs($user)
                    ->visit('/taxrates/edit')
//                    ->type('effective_date', $taxrate->effective_date)
                    ->type('tax_rate', $taxrate->tax_rate)                      //必須チェック
                    ->press('confirm')                                          //「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $taxrate->tax_rate)
                    ->assertSee('税率は必須です。')                              // 確認:メッセージ

                    ->type('tax_rate', $taxrate->tax_rate_100)                  //最大数値チェック
                    ->press('confirm')                                          //「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $taxrate->tax_rate_100)
                    ->assertSee('税率は99以下にしてください。')                   // 確認:メッセージ

                    ->type('tax_rate', $taxrate->tax_rate_string)               //文字列チェック
                    ->press('confirm')                                          //「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $taxrate->tax_rate_string)
                    ->assertSee('税率は整数にしてください。');                    // 確認:メッセージ
        });
    }
    
    /**
     * バリデーションの動作を確認する (editアクション)
     *
     * @return void
     */
    public function testValidationEdit()
    {
        // ユーザーと税率を新規に作成・保存
        $user    = factory(User::class)->create();
        $taxrate = factory(Taxrate::class)->create();
        
        // 税率の作成のみを行う。保存はしない
        $update = factory(Taxrate::class)->make([
//            'effective_date' => '',
            'tax_rate'        => '',
            'tax_rate_100'    => 100,
            'tax_rate_string' => '文字',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $taxrate, $update) {
            $path = '/taxrates/edit/'.$taxrate->id;
            $browser->loginAs($user)
                    ->visit($path)
                    ->type('tax_rate', $update->tax_rate)                       //必須チェック
                    ->press('confirm')                                          // 「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $update->tax_rate)
                    ->assertSee('税率は必須です。')                              // 確認:メッセージ
                
                    ->type('tax_rate', $update->tax_rate_100)                   //最大数値チェック
                    ->press('confirm')                                          // 「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $update->tax_rate_100)
                    ->assertSee('税率は99以下にしてください。')                   // 確認:メッセージ
                
                    ->type('tax_rate', $update->tax_rate_string)                //文字列チェック
                    ->press('confirm')                                          // 「確認」ボタンを押下
                    ->assertPathIs('/taxrates/save')                            // 登録ページであることを確認
                    ->assertInputValue('tax_rate', $update->tax_rate_string)
                    ->assertSee('税率は整数にしてください。');                    // 確認:メッセージ
        });
    }    
    
    /**
     * 画面遷移テスト
     *
     * @return void
     */
    public function testValidationTransition()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 作成のみを行う。保存はしない
        $taxrate = factory(Taxrate::class)->make();

        $taxrate_string = factory(Taxrate::class)->make([
            'tax_rate'       => 'a8',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $taxrate, $taxrate_string) {
            $browser->loginAs($user)
                    ->visit('/taxrates/edit')
                    ->type('tax_rate',            $taxrate->tax_rate)                   // 入力:税率
                    ->press('confirm')                                                  // 押下:「確認」ボタン
                    ->assertPathIs('/taxrates/save')                                    // 確認:確認ページであること
                    ->press('back')                                                     // 押下:「入力画面に戻る」ボタン
                    ->assertPathIs('/taxrates/save')                                    // 確認:税率マスタ入力画面であること
                    ->type('tax_rate',            $taxrate_string->tax_rate)            // 入力:税率
                    ->press('confirm')                                                  // 押下:「確認」ボタン
                    ->assertPathIs('/taxrates/save')                                    // 確認:確認ページであること
                    ->assertSee('税率は整数にしてください。')                             // 確認:メッセージ
                    ->type('tax_rate',            $taxrate->tax_rate)                   // 入力:税率
                    ->press('confirm')                                                  // 押下:「確認」ボタン
                    ->assertPathIs('/taxrates/save')                                    // 確認:確認ページであること
                    ->press('back')                                                     // 押下:「新規登録」ボタン
                    ->assertPathIs('/taxrates/save')                                    // 確認:税率マスタ入力画面であること
                    ->press('#list')                                                    // 押下:登録画面下部「一覧へ」ボタン
                    ->assertPathIs('/taxrates')                                         // 確認:税率マスタ一覧であること
                    ->press('#new-taxrates')                                            // 押下:税率マスタ一覧「新規登録」ボタン
                    ->assertPathIs('/taxrates/edit')                                    // 確認:税率マスタ入力画面であること
                    ->press('#up_list')                                                 // 押下:登録画面上部「一覧」ボタン
                    ->assertPathIs('/taxrates');                                        // 確認:税率マスタ一覧であること
                    
         });
   }    
    
    
    
}
