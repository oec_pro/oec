<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Taxrate;
use App\Http\Requests\FormTaxrateRequest;
use Config;
use Input;
//use Request;
use Validator;

class TaxratesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * 初期表示
     *
     * @return Response
     */
    public function index()
    {
        $query = \App\Taxrate::query();
        
        $data['results'] = $query->orderBy('id', 'desc')->paginate(\Config::get('const.paginate_20'));
        $data['search']  = null;

        return view('taxrates.index', compact('data'));

    }   

//    /**
//     * 検索
//     *
//     * @return Response
//     */
//    public function search()
//    {
//        $prms = Request::all();
//        
//        $query = Taxrate::query();
//
//        
//        if (isset($prms['tax_rate']) && !empty($prms['tax_rate'])){
//            $query->where('tax_rate','like','%'.$prms['tax_rate'].'%');
//        }
//
//        //paginate
//        $data['results'] = $query->orderBy('id', 'desc')->paginate(Config::get('const.paginate_20'))->appends($prms);
//        
//        return view('eras.index', compact('data'));
//    }
    
    /**
     * 登録更新画面へ遷移
     *
     * @return Response
     */
    public function edit($id=null)
    {
        $data = new Taxrate();
		if (is_numeric($id)) {
            $data = Taxrate::find($id);
            if (is_null($data)) {
                //一覧画面へ遷移
                return redirect('taxrates');
            }
        }
        
        return view('taxrates.edit', compact('data'));
    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save(Request $request)
    {
        
        $data = $request;
        
        /* ----------------------------------------------
         * バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make($request->all(), FormTaxrateRequest::rules());

        // バリデーションエラーだった場合
        if ($validator->fails()) {
            $data['confirm'] = false;
            return view('taxrates.edit', compact('data'))->withErrors($validator);
        }        

        /* ----------------------------------------------
         * 画面遷移
         * ----------------------------------------------*/
        //入力画面へ遷移
        if (Input::get('back')) {
            return view('taxrates.edit', compact('data'));
        }
        
        //確認画面へ遷移
        if (Input::get('confirm')) {
            $data['confirm'] = true;
            return view('taxrates.edit', compact('data'));
        }

        /* ----------------------------------------------
         * 登録更新 
         * ----------------------------------------------*/
        //ID情報の取得
        $tbls = Taxrate::find($data->id);
        if (is_null($tbls)) {
            $tbls = new Taxrate;
        }
        
        //データセット
        $tbls->effective_date = $data->effective_date;
        $tbls->tax_rate       = $data->tax_rate;
        
        //登録更新処理
        if ( !$tbls->save() ) {
            return view('taxrates.edit', compact('data'));
        }
        
        return redirect('taxrates');
    }    

    /**
     * 論理削除処理
     *
     * @return Response
     */
    public function del($id)
    {
		if(!empty($id)){
            $sql = Taxrate::find($id);
			$sql->delete();
		}
        return redirect('taxrates');
    }    
}
