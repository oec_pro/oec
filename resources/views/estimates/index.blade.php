@extends('layouts.app')

<style>
    .status1 {
        color: #fff;
        background: #5cb85c;
    }
    .status2 {
        color: #fff;
        background: #337ab7;
    }   
    .status3 {
        color: #fff;
        background: #f0ad4e;
    }
    .status4 {
        color: #fff;
        background: #5bc0de;
    }
</style>

@section('content')
	<div class="navi_sub">
		<div class="navi_sub_left">
			<a id="new" class="btn btn-success btn-sm" href="{{ action('EstimatesController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
				<i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
			</a>
			<a id="list" class="btn btn-success btn-sm" href="{{ action('EstimatesController@index') }}" role="button" data-toggle="tooltip" title="見積書一覧を表示">
				<i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
			</a>
			<button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="見積書一覧のCSVを出力" onclick="confirmCsv();return false;">
				<i class="fa fa-arrow-circle-down" aria-hidden="true"></i><span class="navi_sub_marginleft">CSV出力</span>
			</button>
		</div>
		<div class="clear"></div>
	</div>

<!-- container_page -->
<div class="container_page">

    <div class="title_box">
        <div class="title_left">
            <h3><b>見積一覧</b></h3>
        </div>
        <div class="title_right">
        </div>
        <div class="clear"></div>
        <hr>
    </div>

    <!-- panel-body -->
    <div class="panel-body">

        <!-- panel-group -->
		<div class="panel-group" id="" role="tablist" aria-multiselectable="true">

            <!-- panel panel-default -->
            <div class="panel panel-default">

                <!-- panel-heading headingOne -->
                <div class="panel-heading" role="tab" id="headingOne">
                    <h5 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="" href="#collapseOne_three" aria-expanded="true" aria-controls="collapseOne">
                            <b>検索条件</b>
                            <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                        </a>
                    </h5>
                </div><!-- panel-heading headingOne -->

                <!-- collapseOne_three -->
                <div id="collapseOne_three" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <!-- panel-body -->
                    <div class="panel-body">
                        {!! Form::open(['method' => 'get', 'url' => 'estimates/search', 'class'=>'form-horizontal form-label-left']) !!}
                            <!-- estimates_egistration -->
                            <div class="estimates_egistration">
                                <!-- estimates_list_box1 -->
                                <div class="estimates_list_box1">
                                    <table border="1" id="table jambo_table"  class="table table-bordered">
                                        <tbody>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">得意先名</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('customer_name', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th><div class="col_title_right_req">件名(名称)</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('title', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box form-inline">
                                                <th class="contents_titile_width"><div class="col_title_right_req">見積日</div></th>
                                                <td>
                                                    <div class="col_contents input-group date">
                                                        {!! Form::text('estimate_date_from', "", array('style' => 'width:100%', 'class' => 'form-control input-sm','maxlength' => '8')); !!}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default input-sm" type="button"><span class="fa fa-calendar"></span></button>
                                                        </span>
                                                    </div>
                                                    <div class="col_contents input-group">～</div>
                                                    <div class="col_contents input-group date">
                                                        {!! Form::text('estimate_date_to', "", array('class' => 'form-control input-sm','maxlength' => '8')); !!}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default input-sm" type="button"><span class="fa fa-calendar"></span></button>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th><div class="col_title_right_req">コメント</div></th>
                                                <td>
                                                    <div class="col_contents">
                                                        {{ Form::text('coments', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th><div class="col_title_right_req">状態</div></th>
                                                <td>
                                                    <div class="col_contents estimates_status">
                                                        <?php $cond_list = array_merge([''], Config::get('const.sales_cond')); ?>
                                                        {{ Form::select('sales_cond', $cond_list, "", ['class' => 'form-control input-sm']) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th><div class="col_title_right_req">担当者名</div></th>
                                                <td>
                                                    <div class="col_contents estimates_person">
                                                        <?php $staff_list     = App\User::orderBy('user_name','asc')->pluck('user_name', 'id')->prepend('', ''); ?>
                                                        {{ Form::select('staff_id', $staff_list, "", ['class' => 'form-control input-sm'] ) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th><div class="col_title_right_req">見積No</div></th>
                                                <td>
                                                    <div class="col_contents estimates_person">
                                                        {{ Form::text('esti_no', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">税込金額</div></th>
                                                <td>
                                                    <div class="col_contents estimates_person" style="display: inline-block; _display: inline;">
                                                        {{ Form::text('total_amount_taxin_from', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                    <div style="display: inline-block; _display: inline;">&nbsp～&nbsp</div>
                                                    <div class="col_contents estimates_person" class="col_contents estimates_person" style="display: inline-block; _display: inline;">
                                                        {{ Form::text('total_amount_taxin_to', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- estimates_list_box1 -->
                                <!-- estimates_list_box2 -->
                                <div class="estimates_list_box2">
                                    <table class="border-color-style">
                                        <tbody>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th>
                                                    <div class="">
                                                        {{ Form::submit('検索', ['class' => 'btn btn-primary btn-sm btn_list_mstyle1', 'data-toggle'=>'tooltip', 'title'=>'この条件で検索する']) }}
                                                        {{ Form::button('クリア', ['id' => 'clear', 'class' => 'btn btn-warning btn-sm btn_list_mstyle2', 'data-toggle'=>'tooltip', 'title'=>'条件をクリアする']) }}
                                                    </div>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- estimates_list_box -->
                                <div class="clear"></div>
                            </div><!-- estimates_egistration -->
                        {!! Form::close() !!}
                    </div><!-- panel-body -->
                </div><!-- collapseOne_three headingOne -->

            </div><!-- panel panel-default -->

            <!-- order_detail_info_wrap -->
            <div id="order_detail_info_wrap">
                @if (count($data['results']) >= 1)
                    <table summary="見積一覧" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame estimate_list">
                        <thead>
                            <tr class="headings">
                                <th class="estimate_detail_title1" rowspan="2"><div class="order_detail_height_style">見積No</div></th>
                                <th class="estimate_detail_title2" rowspan="2"><div class="order_detail_height_style">得意先名</div></th>
                                <th class="estimate_detail_title3" rowspan="2"><div class="order_detail_height_style">件名</div></th>
                                <th class="estimate_detail_title4" rowspan="2"><div class="order_detail_height_style">税込金額</div></th>
                                <th class="estimate_detail_title5" rowspan="2"><div class="order_detail_height_style">見積日</div></th>
                                <th class="estimate_detail_title6" rowspan="2"><div class="order_detail_height_style">利益額</div></th>
                                <th class="estimate_detail_title6" rowspan="2"><div class="order_detail_height_style">利益率</div></th>
                                <th class="estimate_detail_title7" rowspan="2"><div class="order_detail_height_style">状態</div></th>
                                <th class="estimate_detail_title8">更新日時</th>
                                <th class="estimate_detail_title9" rowspan="2"><div class="order_detail_height_style">操作</div></th>
                                <th class="estimate_detail_title10" rowspan="2"><div class="order_detail_height_style">見積書</div></th>
                            </tr>
                            <tr>
                                <th class="estimate_detail_title11">更新者</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyLine">
                            @foreach($data['results'] as $result)
                                <tr id="trLine0">
                                    <!-- 見積No -->
                                    <td id="tdNo0">
                                        <div class="estimate_contents_box7">{{ $result->esti_no }}</div>
                                    </td>
                                    <!-- 得意先名 -->
                                    <td>
                                        <div class="estimate_contents_box1">{{ $result->customer_name }}</div>
                                    </td>
                                    <!-- 件名 -->
                                    <td>
                                        <div class="estimate_contents_box1">{{ $result->title }}</div>
                                    </td>
                                    <!-- 税込み金額 -->
                                    <td>
                                        <div class="estimate_contents_box4">{{ number_format($result->total_amount_taxin) }}</div>
                                    </td>
                                    <!-- 見積日 -->
                                    <td>
                                        <div class="estimate_contents_box7">{{ date('Y/m/d', strtotime($result->estimate_date)) }}</div>
                                    </td>
                                    <!-- 利益額 税抜合計-原価合計 -->
                                    <td>
                                        <?php
                                            $gross_pro_gk = $result->total_amount_taxnon - $result->total_cost_amount
                                        ?>
                                        <div class="estimate_contents_box4">{{ number_format($gross_pro_gk) }}</div>
                                    </td>
                                    <!-- 利益率 ( 利益額 / 税抜合計 ) * 100 -->
                                    <td>
                                        <?php
                                            $gross_profit = 0;
                                            if ( $gross_pro_gk != 0 ) {
                                                $gross_profit = ( $gross_pro_gk / $result->total_amount_taxnon ) * 100;
                                            }
                                        ?>
                                        <div class="estimate_contents_box7">{{ number_format($gross_profit) }}%</div>
                                    </td>
                                    <!-- 状態 -->
                                    <td class="status{{$result->sales_cond}}">
                                        <div class="estimate_contents_box7">
                                            <b>{{ Config::get('const.sales_cond')[$result->sales_cond] }}</b>
                                        </div>
                                    </td>
                                    <!-- 更新日時 -->
                                    <td>
                                        <div class="estimate_contents_box7">{{ date('Y/m/d H:i:s', strtotime($result->updated_at)) }}</div>
                                        <div class="estimate_contents_box7">{{ $result->updated_name }}</div>
                                    </td>
                                    <!-- 操作 -->
                                    <td>
                                        <div class="estimate_contents_box7">
                                            {!! link_to(action('EstimatesController@edit', [$result->esti_no]), '詳細', ['id' => 'test_update_'.$result->esti_no, 'class' => 'btn btn-primary btn-sm margin_gap', 'data-toggle' => 'tooltip', 'title' => '詳細を表示']) !!}
                                        </div>
                                    </td>
                                    <!-- 見積書 -->
                                    <td>
                                        <div class="estimate_contents_box7 btn-group">
                                            <button id="xls_out" type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-file-text" aria-hidden="true"></i><span class="navi_sub_marginleft">見積書出力</span>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(1, '御見積書出力', {{ $result->id }});return false;">御見積書出力</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(2, '請書出力', {{ $result->id }});return false;">請書出力</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(3, '納品書出力', {{ $result->id }});return false;">納品書出力</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(4, '請求書出力', {{ $result->id }});return false;">請求書出力</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr><!-- trLine0 -->
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data['results']->links() }}
                @elseif(!is_null($data['results']))
                    <div class="alert alert-info">該当する情報がありませんでした。</div>
                @endif
            </div><!-- order_detail_info_wrap -->
        </div><!-- panel-group -->
    </div>　<!-- panel-body  -->

</div>
<!-- /container_page -->

<!-- sweetalert -->
<link rel="stylesheet" href="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.min.js"></script>
<script>
    /* 検索項目クリアー */
    $(document).on("click", "#clear", function(){
        $('[name=customer_name]').val('');
        $('[name=title]').val('');
        $('[name=estimate_date_from]').val('');
        $('[name=estimate_date_to]').val('');
        $('[name=coments]').val('');
        $('select[name=sales_cond]').val('');
        $('select[name=staff_id]').val('');
        $('[name=esti_no]').val('');
        $('[name=total_amount_taxin_from]').val('');
        $('[name=total_amount_taxin_to]').val('');
    });

    function confirmCsv() {
        var prams = "";
        prams = '?customer_name='              + $('[name=customer_name]').val();
        prams = prams + '&title='              + $('[name=title]').val();
        prams = prams + '&estimate_date_from=' + $('[name=estimate_date_from]').val();
        prams = prams + '&estimate_date_to='   + $('[name=estimate_date_to]').val();
        prams = prams + '&coments='            + $('[name=coments]').val();
        prams = prams + '&sales_cond='         + $('select[name=sales_cond]').val();
        prams = prams + '&staff_id='           + $('select[name=staff_id]').val();
        prams = prams + '&esti_no='            + $('[name=esti_no]').val();
        swal({
            title: "見積書一覧CSV出力確認",
            text: "出力してもよろしいですか？",
            type: "", // default:null "warning","error","success","info"
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"出力", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#31b0d5", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: true // default:true 確認ボタンを押したらアラートが削除される
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/estimates/download/list" + prams, '_self');
                return true;
            } else {
                return false;
            }
        });
    }

    /* 見積書出力エクセル */
    function confirmExl(out_type, file_nm, id) {
        swal({
            title: file_nm,
            text: "出力してもよろしいですか？",
            type: "", // default:null "warning","error","success","info"
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"出力", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#31b0d5", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: true // default:true 確認ボタンを押したらアラートが削除される
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/estimates/xlsprint/" + out_type + "/" + id, '_self');
                return true;
            } else {
                return false;
            }
        });
    }

</script>
@endsection