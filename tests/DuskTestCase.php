<?php

namespace Tests;

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
//            '--headless',
             // Bootstrap4のブレークポイントの基準で「Large devices(992px以上)」となるよう横幅を指定。
             // ただし、ウィンドウ枠などを考慮して余裕をもたせる。
//            '--window-size=1200,1000',
//            '--window-zoom=50%',
        ]);

        return RemoteWebDriver::create(
            //Google
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
            
//            'http://localhost', DesiredCapabilities::firefox()
            
//            'http://norimasa.co.jp', DesiredCapabilities::chrome()->setCapability(
//                ChromeOptions::CAPABILITY, $options
//            )
            
        );
    }
}
