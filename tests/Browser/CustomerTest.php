<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Customer;
use App\User;

/**
 * 得意先に関するテスト
 */
class CustomerTest extends DuskTestCase
{
    // Dusk実行前にマイグレーションする
    use DatabaseMigrations;
    
    /**
     * 得意先に関する操作(作成、編集、削除)のテスト
     *
     * @return void
     */
    public function testCRUD()
    {
        
        // ユーザーを新規に作成・保存
        $user    = factory(User::class)->create();
        
        // 得意先登録する内容
        $customer     = factory(Customer::class)->make([
            'id' => 1,
            'customer_id' 		  => 'CUS00000001',
            'customer_name' 	  => '株式会社 渚',
            'customer_kana' 	  => 'カブシキガイシャ　ナナミ',
            'representative_name' => '廣川 七夏',
            'representative_kana' => 'ヒロカワ　ナナツ',
            'department_name' 	  => '営業部',
            'person_name' 		  => '中島 桃子',
            'zip' 				  => '1660001',
            'address' 			  => '東京都杉並区阿佐ヶ谷北1-1-1 サンライズアパート101号室',
            'tel' 				  => '09096537731',
            'fax' 				  => '09093363142',
            'cellphone'           => '09012345678',
        ]);
        
        // 編集する内容        
        $update       = factory(Customer::class)->make([
            'id' => 1,
            'customer_id' 		  => 'CUS00000001',
            'customer_name' 	  => '有限会社 西之園',
            'customer_kana' 	  => 'ユウゲンガイシャ　ニシノエン',
            'representative_name' => '中島 春香',
            'representative_kana' => 'ナカジマ　ハルカ',
            'department_name' 	  => '総務部',
            'person_name' 		  => '高橋 拓真',
            'zip' 				  => '1660002',
            'address' 			  => '東京都杉並区阿高円寺371番地',
            'tel' 				  => '08081448972',
            'fax' 				  => '0047954761',
            'cellphone'           => '09098765432',
        ]);

        $this->browse(function (Browser $browser) use ($user, $customer, $update) {
        $browser->loginAs($user)                                     // ログインする
                ->visit('/customers/edit')
                /* --------------------------------
                 * 新規登録 
                 *--------------------------------- */
                ->type('customer_name',                  $customer->customer_name)         // 入力:得意先名
                ->type('customer_kana',                  $customer->customer_kana)         // 入力:得意先カナ名
                ->type('representative_name',            $customer->representative_name)   // 入力:代表者名
                ->type('representative_kana',            $customer->representative_kana)   // 入力:代表者カナ名
                ->type('department_name',                $customer->department_name)       // 入力:部署名
                ->type('person_name',                    $customer->person_name)           // 入力:担当者名
                ->type('zip1',                    substr($customer->zip, 0, 3))            // 入力:郵便番号
                ->type('zip2',                    substr($customer->zip, 3))               // 入力:郵便番号
                ->type('address',                        $customer->address)               // 入力:住所
                ->type('tel', str_replace("-", "",       $customer->tel))                  // 入力:TEL
                ->type('fax', str_replace("-", "",       $customer->fax))                  // 入力:FAX
                ->type('cellphone', str_replace("-", "", $customer->cellphone))            // 入力:携帯番号
                ->press('confirm')                                                         // 押下:「確認」ボタン
                ->assertPathIs('/customers/save')                                          // 確認:確認ページであること
                ->press('insert')                                                          // 押下:「新規登録」ボタン
                ->assertPathIs('/customers')                                               // 確認:得意先マスタ一覧であること
                /* --------------------------------
                 * 編集
                 *--------------------------------- */
                ->press('#test_update_'.$customer->id)                               // 「変更」ボタンを押下
                ->assertPathIs('/customers/edit/'.$customer->id)
//                ->assertSeeIn('#test_customer_id', $update->customer_id)            
                ->type('customer_name',                  $update->customer_name)           // 入力:得意先名
                ->type('customer_kana',                  $update->customer_kana)           // 入力:得意先カナ名
                ->type('representative_name',            $update->representative_name)     // 入力:代表者名
                ->type('representative_kana',            $update->representative_kana)     // 入力:代表者カナ名
                ->type('department_name',                $update->department_name)         // 入力:部署名
                ->type('person_name',                    $update->person_name)             // 入力:担当者名
                ->type('zip1',                    substr($update->zip, 0, 3))              // 入力:郵便番号
                ->type('zip2',                    substr($update->zip, 3))                 // 入力:郵便番号
                ->type('address',                        $update->address)                 // 入力:住所
                ->type('tel',       str_replace("-", "", $update->tel))                    // 入力:TEL
                ->type('fax',       str_replace("-", "", $update->fax))                    // 入力:FAX
                ->type('cellphone', str_replace("-", "", $update->cellphone))              // 入力:携帯番号
                ->press('confirm')                                                         // 押下:「確認」ボタン
                ->assertPathIs('/customers/save')                                          // 確認:確認ページであること
                ->press('insert')                                                          // 押下:「新規登録」ボタン
                ->assertPathIs('/customers')                                               // 確認:得意先マスタ一覧であること
                /* --------------------------------
                 * 削除
                 *--------------------------------- */
                ->press('#test_del_'.$customer->id)                                        // 「削除」ボタンを押す
                ->press('#test_confirm')                                                   // ポップアップ画面「削除」ボタンを押す
                ->assertPathIs('/customers');                                              // 得意先マスタ一覧であることを確認
        });
    }    
    
    /**
     * バリデーションの動作を確認する (createアクション)
     *
     * @return void
     */
    public function testValidationCreate()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 作成のみを行う。保存はしない
        $customer_require = factory(Customer::class)->make([
            'customer_name'       => '',
        ]);
        $customer_kana = factory(Customer::class)->make([
            'customer_kana'       => '得意先カナ名',
            'representative_kana' => '代表者カナ名',
        ]);
        $customer_kana_han = factory(Customer::class)->make([
            'customer_kana'       => mb_convert_encoding('ﾄｸｲｻｷﾒｲｶﾅ','UTF-8','sjis-win'),
            'representative_kana' => mb_convert_encoding('ﾀﾞｲﾋｮｳｼｬｶﾅﾒｲ','UTF-8','sjis-win'),
        ]);
        $customer_num = factory(Customer::class)->make([
            'tel'                 => 'abcde',
            'fax'                 => 'abcde',
            'cellphone'           => 'abcde',
        ]);
        $customer_zip = factory(Customer::class)->make([
            'zip1'                => '166',
            'zip2'                => '0001',
            'zip1_require'        => '',
            'zip2_require'        => '',
            'zip1_num'            => 'aaac',
            'zip2_num'            => '2kk9',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $customer_require, $customer_kana, $customer_kana_han, $customer_num, $customer_zip) {
            $browser->loginAs($user)
                    ->visit('/customers/edit')
                    ->type('customer_name',                  $customer_require->customer_name)                     // 入力:得意先名
                    ->type('customer_kana',                  $customer_require->customer_kana)                     // 入力:得意先カナ名
                    ->type('representative_name',            $customer_require->representative_name)               // 入力:代表者名
                    ->type('representative_kana',            $customer_require->representative_kana)               // 入力:代表者カナ名
                    ->type('department_name',                $customer_require->department_name)                   // 入力:部署名
                    ->type('person_name',                    $customer_require->person_name)                       // 入力:担当者名
                    ->type('zip1',                    substr($customer_require->zip, 0, 3))                        // 入力:郵便番号
                    ->type('zip2',                    substr($customer_require->zip, 4))                           // 入力:郵便番号
                    ->type('address',                        $customer_require->address)                           // 入力:住所
                    ->type('tel',       str_replace("-", "", $customer_require->tel))                              // 入力:TEL
                    ->type('fax',       str_replace("-", "", $customer_require->fax))                              // 入力:FAX
                    ->type('cellphone', str_replace("-", "", $customer_require->cellphone))                        // 入力:携帯番号
                    ->press('confirm')                                                                             // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                                              // 確認:登録ページであること
                    ->assertSee('得意先名は必須です。')                                                            // 確認:メッセージ
                    ->assertInputValue('customer_name',                  $customer_require->customer_name)         // 入力後:得意先名
                    ->assertInputValue('customer_kana',                  $customer_require->customer_kana)         // 入力後:得意先カナ名
                    ->assertInputValue('representative_name',            $customer_require->representative_name)   // 入力後:代表者名
                    ->assertInputValue('representative_kana',            $customer_require->representative_kana)   // 入力後:代表者カナ名
                    ->assertInputValue('department_name',                $customer_require->department_name)       // 入力後:部署名
                    ->assertInputValue('person_name',                    $customer_require->person_name)           // 入力後:担当者名
                    ->assertInputValue('zip1',                    substr($customer_require->zip, 0, 3))            // 入力後:郵便番号
                    ->assertInputValue('zip2',                    substr($customer_require->zip, 4))               // 入力後:郵便番号
                    ->assertInputValue('address',                        $customer_require->address)               // 入力後:住所
                    ->assertInputValue('tel',       str_replace("-", "", $customer_require->tel))                  // 入力後:TEL
                    ->assertInputValue('fax',       str_replace("-", "", $customer_require->fax))                  // 入力後:FAX                    
                    ->assertInputValue('cellphone', str_replace("-", "", $customer_require->cellphone))            // 入力後:携帯番号                    
                
                    /* 全角カナチェック */
                    ->type('customer_name',            $customer_kana->customer_name)            // 入力:得意先名
                    ->type('customer_kana',            $customer_kana->customer_kana)            // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_kana->representative_kana)      // 入力:代表者カナ名
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('得意先カナ名は全角カタカナで入力してください。')                // 確認:メッセージ

                    /* 半角カナチェック */
                    ->type('customer_kana',            $customer_kana_han->customer_kana)        // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_kana_han->representative_kana)  // 入力:代表者カナ名
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('得意先カナ名は全角カタカナで入力してください。')                // 確認:メッセージ

                    /* 数値ナチェック */
                    ->type('customer_kana',            $customer_num->customer_kana)             // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_num->representative_kana)       // 入力:代表者カナ名
                    ->type('tel',                      $customer_num->tel)                       // 入力:tel
                    ->type('fax',                      $customer_num->fax)                       // 入力:fax
                    ->type('cellphone',                $customer_num->cellphone)                 // 入力:携帯番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('TELは半角数字で入力してください。')                             // 確認:メッセージ
                    ->assertSee('FAXは半角数字で入力してください。')                             // 確認:メッセージ
                    ->assertSee('携帯番号は半角数字で入力してください。')                        // 確認:メッセージ
                    ->type('tel',       str_replace("-", "", $customer_require->tel))            // 入力:TEL
                    ->type('fax',       str_replace("-", "", $customer_require->fax))            // 入力:FAX
                    ->type('cellphone', str_replace("-", "", $customer_require->cellphone))      // 入力:携帯番号

                    /* ZIPチェック */
                    ->type('customer_name',            "得意先名")                                // 入力:得意先名
                    ->type('zip1',                     $customer_zip->zip1_num)                  // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2_num)                  // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 上桁は数字にしてください。')                           // 確認:メッセージ
                    ->assertSee('郵便番号 下桁は数字にしてください。')                           // 確認:メッセージ
                
                    ->type('zip1',                     $customer_zip->zip1_require)              // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2)                      // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 下桁が存在する時、郵便番号 上桁は必須です。')          // 確認:メッセージ
                
                    ->type('zip1',                     $customer_zip->zip1)                      // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2_require)              // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 上桁が存在する時、郵便番号 下桁は必須です。');         // 確認:メッセージ
            
        });
    }        
    
    /**
     * バリデーションの動作を確認する (editアクション)
     *
     * @return void
     */
    public function testValidationEdit()
    {
        // ユーザーを新規に作成・保存
        $user     = factory(User::class)->create();
        $customer = factory(Customer::class)->create();
        
        // 作成のみを行う。保存はしない
        $customer_require = factory(Customer::class)->make([
            'customer_name'       => '',
        ]);
        $customer_kana = factory(Customer::class)->make([
            'customer_kana'       => '得意先カナ名',
            'representative_kana' => '代表者カナ名',
        ]);
        $customer_kana_han = factory(Customer::class)->make([
            'customer_kana'       => mb_convert_encoding('ﾄｸｲｻｷﾒｲｶﾅ','UTF-8','sjis-win'),
            'representative_kana' => mb_convert_encoding('ﾀﾞｲﾋｮｳｼｬｶﾅﾒｲ','UTF-8','sjis-win'),
        ]);
        $customer_num = factory(Customer::class)->make([
            'tel'                 => 'abcde',
            'fax'                 => 'abcde',
            'cellphone'           => 'abcde',
        ]);
        $customer_zip = factory(Customer::class)->make([
            'zip1'                => '166',
            'zip2'                => '0001',
            'zip1_require'        => '',
            'zip2_require'        => '',
            'zip1_num'            => 'aaac',
            'zip2_num'            => '2kk9',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $customer, $customer_require, $customer_kana, $customer_kana_han, $customer_num, $customer_zip) {
            $path = '/customers/edit/'.$customer->id;
            $browser->loginAs($user)
                    ->visit($path)
               
                    /* 必須チェック */
                    ->type('customer_name',                  $customer_require->customer_name)                     // 入力:得意先名
                    ->type('customer_kana',                  $customer_require->customer_kana)                     // 入力:得意先カナ名
                    ->type('representative_name',            $customer_require->representative_name)               // 入力:代表者名
                    ->type('representative_kana',            $customer_require->representative_kana)               // 入力:代表者カナ名
                    ->type('department_name',                $customer_require->department_name)                   // 入力:部署名
                    ->type('person_name',                    $customer_require->person_name)                       // 入力:担当者名
                    ->type('zip1',                    substr($customer_require->zip, 0, 3))                        // 入力:郵便番号
                    ->type('zip2',                    substr($customer_require->zip, 4))                           // 入力:郵便番号
                    ->type('address',                        $customer_require->address)                           // 入力:住所
                    ->type('tel',       str_replace("-", "", $customer_require->tel))                              // 入力:TEL
                    ->type('fax',       str_replace("-", "", $customer_require->fax))                              // 入力:FAX
                    ->type('cellphone', str_replace("-", "", $customer_require->cellphone))                        // 入力:携帯番号
                    ->press('confirm')                                                                             // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                                              // 確認:登録ページであること
                    ->assertSee('得意先名は必須です。')                                                            // 確認:メッセージ
                    ->assertInputValue('customer_name',                  $customer_require->customer_name)         // 入力後:得意先名
                    ->assertInputValue('customer_kana',                  $customer_require->customer_kana)         // 入力後:得意先カナ名
                    ->assertInputValue('representative_name',            $customer_require->representative_name)   // 入力後:代表者名
                    ->assertInputValue('representative_kana',            $customer_require->representative_kana)   // 入力後:代表者カナ名
                    ->assertInputValue('department_name',                $customer_require->department_name)       // 入力後:部署名
                    ->assertInputValue('person_name',                    $customer_require->person_name)           // 入力後:担当者名
                    ->assertInputValue('zip1',                    substr($customer_require->zip, 0, 3))            // 入力後:郵便番号
                    ->assertInputValue('zip2',                    substr($customer_require->zip, 4))               // 入力後:郵便番号
                    ->assertInputValue('address',                        $customer_require->address)               // 入力後:住所
                    ->assertInputValue('tel',       str_replace("-", "", $customer_require->tel))                  // 入力後:TEL
                    ->assertInputValue('fax',       str_replace("-", "", $customer_require->fax))                  // 入力後:FAX
                    ->assertInputValue('cellphone', str_replace("-", "", $customer_require->cellphone))            // 入力後:携帯番号
                
                    /* 全角カナチェック */
                    ->type('customer_name',            $customer_kana->customer_name)            // 入力:得意先名
                    ->type('customer_kana',            $customer_kana->customer_kana)            // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_kana->representative_kana)      // 入力:代表者カナ名
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('得意先カナ名は全角カタカナで入力してください。')                   // 確認:メッセージ

                    /* 半角カナチェック */
                    ->type('customer_kana',            $customer_kana_han->customer_kana)        // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_kana_han->representative_kana)  // 入力:代表者カナ名
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('得意先カナ名は全角カタカナで入力してください。')                   // 確認:メッセージ

                    /* 数値ナチェック */
                    ->type('customer_kana',            $customer_num->customer_kana)             // 入力:得意先カナ名
                    ->type('representative_kana',      $customer_num->representative_kana)       // 入力:代表者カナ名
                    ->type('tel',                      $customer_num->tel)                       // 入力:tel
                    ->type('fax',                      $customer_num->fax)                       // 入力:fax
                    ->type('cellphone',                $customer_num->cellphone)                 // 入力:携帯番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('TELは半角数字で入力してください。')                               // 確認:メッセージ
                    ->assertSee('FAXは半角数字で入力してください。')                               // 確認:メッセージ
                    ->assertSee('携帯番号は半角数字で入力してください。')                          // 確認:メッセージ
                    ->type('tel',       str_replace("-", "", $customer_require->tel))                  // 入力:TEL
                    ->type('fax',       str_replace("-", "", $customer_require->fax))                  // 入力:FAX
                    ->type('cellphone', str_replace("-", "", $customer_require->cellphone))            // 入力:携帯番号

                    /* ZIPチェック */
                    ->type('customer_name',            "得意先名")                                // 入力:得意先名
                    ->type('zip1',                     $customer_zip->zip1_num)                  // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2_num)                  // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 上桁は数字にしてください。')                             // 確認:メッセージ
                    ->assertSee('郵便番号 下桁は数字にしてください。')                             // 確認:メッセージ
                
                    ->type('zip1',                     $customer_zip->zip1_require)              // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2)                      // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 下桁が存在する時、郵便番号 上桁は必須です。')              // 確認:メッセージ
                
                    ->type('zip1',                     $customer_zip->zip1)                      // 入力:郵便番号
                    ->type('zip2',                     $customer_zip->zip2_require)              // 入力:郵便番号
                    ->press('confirm')                                                           // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                            // 確認:登録ページであること
                    ->assertSee('郵便番号 上桁が存在する時、郵便番号 下桁は必須です。');             // 確認:メッセージ);

        });
    }        
    
    /**
     * 画面遷移テスト
     *
     * @return void
     */
    public function testValidationTransition()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 作成のみを行う。保存はしない
        $customer = factory(Customer::class)->make();

        $customer_kana = factory(Customer::class)->make([
            'customer_kana'       => '得意先カナ名',
        ]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $customer, $customer_kana) {
            $browser->loginAs($user)
                    ->visit('/customers/edit')
                    ->type('customer_name',            $customer->customer_name)         // 入力:得意先名
                    ->type('customer_kana',            $customer->customer_kana)         // 入力:得意先カナ名
                    ->press('confirm')                                                   // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                    // 確認:確認ページであること
                    ->press('back')                                                      // 押下:「入力画面に戻る」ボタン
                    ->assertPathIs('/customers/save')                                    // 確認:得意先マスタ入力画面であること
                    ->type('customer_kana',            $customer_kana->customer_kana)    // 入力:得意先カナ名
                    ->press('confirm')                                                   // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                    // 確認:確認ページであること
                    ->assertSee('得意先カナ名は全角カタカナで入力してください。')           // 確認:メッセージ
                    ->type('customer_kana',            $customer->customer_kana)         // 入力:得意先カナ名
                    ->press('confirm')                                                   // 押下:「確認」ボタン
                    ->assertPathIs('/customers/save')                                    // 確認:確認ページであること
                    ->press('back')                                                      // 押下:「新規登録」ボタン
                    ->assertPathIs('/customers/save')                                    // 確認:得意先マスタ入力画面であること
                    ->press('#list')                                                     // 押下:登録画面下部「一覧へ」ボタン
                    ->assertPathIs('/customers')                                         // 確認:得意先マスタ一覧であること
                    ->press('#new')                                                      // 押下:得意先マスタ一覧「新規登録」ボタン
                    ->assertPathIs('/customers/edit')                                    // 確認:得意先マスタ入力画面であること
                    ->press('#up_list')                                                  // 押下:登録画面上部「一覧」ボタン
                    ->assertPathIs('/customers');                                        // 確認:得意先マスタ一覧であること
                    
         });
   }    
    
}
