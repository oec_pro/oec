@extends('layouts.app')

@section('content')

<!-- container -->
<div class="navi_sub">
    
    <!-- navi_sub_left -->
    <div class="navi_sub_left">
        <a class="btn btn-success btn-sm" href="{{ action('CustomersController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
            <i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
        </a>
        <a id="up_list" class="btn btn-success btn-sm" href="{{ action('CustomersController@index') }}" role="button" data-toggle="tooltip" title="得意先一覧を表示">
            <i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
        </a>
    </div><!-- navi_sub_left -->
    
    <div class="navi_sub_right">
    </div>
    <div class="clear"></div>
</div><!-- container -->

<!-- container_page -->
<div class="container_page">
    
    <!-- title_box -->
    <div class="title_box">
        <div class="title_left">
            @if ( is_numeric($data['id']) )
                <h3><b><?php echo "得意先変更"; ?></b></h3>
            @else
                <h3><b><?php echo "得意先登録"; ?></b></h3>
            @endif
            <p id="title_mode" style="color:#FF0000;font-size:20px"></p>
        </div>
        <div class="clear"></div>
        <hr>
    </div>
    <!-- / title_box -->
        
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                {!! Form::open(['name' => 'form', 'url' => 'customers/save', 'class' => 'form-horizontal form-label-left', 'method' => 'POST']) !!}    
                    {!! Form::hidden('id', $data['id']) !!}

                    @if ( is_numeric($data['id']) )
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">得意先ID<span class="required_color">※</span></label>
                            <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                                <span id="test_customer_id">{{ $data['customer_id'] }}</span>
                            </div>
                        </div>
                    @endif
                    {!! Form::hidden('customer_id', $data['customer_id']) !!}

                    <div class="form-group {{ !empty($errors->first('customer_name')) ? ' has-error' :'' }}">
                        
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">得意先名<span class="required_color">※</span></label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('customer_name', $data['customer_name'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）株式会社 海猫商事', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('customer_name')}}</span>
                            @else
                                {{ $data['customer_name'] }}
                                {!! Form::hidden('customer_name', $data['customer_name']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('customer_kana')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">得意先カナ名</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('customer_kana', $data['customer_kana'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）カブシキガイシャ　ウミネコショウジ', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('customer_kana')}}</span>
                            @else
                                {{ $data['customer_kana'] }}
                                {!! Form::hidden('customer_kana', $data['customer_kana']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('representative_name')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">代表者名</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('representative_name', $data['representative_name'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）海猫太郎', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('representative_name')}}</span>
                            @else
                                {{ $data['representative_name'] }}
                                {!! Form::hidden('representative_name', $data['representative_name']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('representative_kana')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">代表者カナ名</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('representative_kana', $data['representative_kana'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）ウミネコタロウ', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('representative_kana')}}</span>
                            @else
                                {{ $data['representative_kana'] }}
                                {!! Form::hidden('representative_kana', $data['representative_kana']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('department_name')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">部署名</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('department_name', $data['department_name'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）営業部', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('department_name')}}</span>
                            @else
                                {{ $data['department_name'] }}
                                {!! Form::hidden('department_name', $data['department_name']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('person_name')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">担当者名</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('person_name', $data['person_name'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）営業一郎', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('person_name')}}</span>
                            @else
                                {{ $data['person_name'] }}
                                {!! Form::hidden('person_name', $data['person_name']) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ !empty($errors->first('zip1')) || !empty($errors->first('zip2')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">郵便番号</label>
                        @if ( empty($data['confirm']) )
                            <div class="col-md-2 form-group has-feedback">
                                    {!! Form::text('zip1', $data['zip1'], ['class' => 'form-control en input-sm', 'placeholder' => '例）904', 'maxlength'=>'3']) !!}
                                    <span class="help-block">{{$errors->first('zip1')}}</span>
                            </div>
                            <div class="col-xs-1" style="width:40px; text-center">-</div>
                            <div class="col-md-2 form-group has-feedback">
                                    {!! Form::text('zip2', $data['zip2'], ['class' => 'form-control en input-sm', 'placeholder' => '例）0001', 'maxlength'=>'4', 'onKeyUp'=>'AjaxZip3.zip2addr("zip1","zip2","address","address");']) !!}
                                    <span class="help-block">{{$errors->first('zip2')}}</span>
                            </div>
                        @else
                            <div class="col-md-2 form-group has-feedback">
                                    {{ $data['zip1'] }}-{{ $data['zip2'] }}
                                    {!! Form::hidden('zip1', $data['zip1']) !!}
                                    {!! Form::hidden('zip2', $data['zip2']) !!}
                            </div>
                        @endif
                    </div>
                    
                    <div class="form-group {{ !empty($errors->first('address')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">住所</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('address', $data['address'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）沖縄県那覇市1-1-1', 'maxlength' => '120']) !!}
                                <span class="help-block">{{$errors->first('address')}}</span>
                            @else
                                {{ $data['address'] }}
                                {!! Form::hidden('address', $data['address']) !!}
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ !empty($errors->first('tel')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">TEL</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('tel', $data['tel'], ['class' => 'form-control en input-sm', 'placeholder' => '例）0981234567', 'maxlength'=>'20']) !!}
                                <span class="help-block">{{$errors->first('tel')}}</span>
                            @else
                                {{ $data['tel'] }}
                                {!! Form::hidden('tel', $data['tel']) !!}
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ !empty($errors->first('fax')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">FAX</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('fax', $data['fax'], ['class' => 'form-control en input-sm', 'placeholder' => '例）0981234567', 'maxlength'=>'20']) !!}
                                <span class="help-block">{{$errors->first('fax')}}</span>
                            @else
                                {{ $data['fax'] }}
                                {!! Form::hidden('fax', $data['fax']) !!}
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ !empty($errors->first('cellphone')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">携帯番号</label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('cellphone', $data['cellphone'], ['class' => 'form-control en input-sm', 'placeholder' => '例）0981234567', 'maxlength'=>'20']) !!}
                                <span class="help-block">{{$errors->first('cellphone')}}</span>
                            @else
                                {{ $data['cellphone'] }}
                                {!! Form::hidden('cellphone', $data['cellphone']) !!}
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            @if ( empty($data['confirm']) )
                                {!! Form::submit("確認", ['class' => 'btn btn-primary regist_del_btn', 'name' => 'confirm']) !!}
                                {!! link_to(action('CustomersController@index', ''), '一覧へ', ['class' => 'btn btn-default', 'id' => 'list']) !!}
                            @else
                                {!! Form::submit(is_numeric($data['id'])? '変更': '登録', ['class' =>'btn btn-primary regist_del_btn', 'name' => 'insert']) !!}
                                {!! Form::submit('入力画面に戻る', ['class' => 'btn btn-default', 'name' => 'back']) !!}
                            @endif
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
<!-- / container_page -->

@endsection

