<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory; // 追加

$factory->define(App\Customer::class, function (Faker $faker) {
    $faker = FakerFactory::create('ja_JP'); // 追加
    
    return [
        'customer_id'         => 'CUS000000001',
        'customer_name'       => $faker->company,
        'customer_kana'       => $faker->kanaName,
        'representative_name' => $faker->name,
        'representative_kana' => $faker->kanaName,
        'department_name'     => $faker->country,
        'person_name'         => $faker->name,
        'zip'       		  => substr($faker->postcode, 0, 3).'-'.substr($faker->postcode, 3),
        'address'       	  => $faker->prefecture.$faker->city.$faker->streetAddress.$faker->secondaryAddress,
        'tel'       		  => $faker->phoneNumber,
        'fax'       		  => $faker->phoneNumber,        
        'cellphone'       	  => $faker->phoneNumber,
    ];
});
