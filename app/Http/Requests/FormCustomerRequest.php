<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function attributes()
    {
         return [];
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
           'customer_id'         => '',
           'customer_name'       => 'required|max:120',
           'customer_kana'       => 'string|nullable|max:120|all_em_kana',
           'representative_name' => 'string|nullable|max:120',
           'representative_kana' => 'string|nullable|max:120|all_em_kana',
           'department_name'     => 'string|nullable|max:120',
           'person_name'         => 'string|nullable|max:120',
           'zip1'                => 'numeric|nullable|max:999|required_with:zip2',
           'zip2'                => 'numeric|nullable|max:9999|required_with:zip1',
           'address'             => 'string|nullable|max:120',
           'tel'                 => 'tel_hankaku_num|max:20',
           'fax'                 => 'tel_hankaku_num|max:20',
           'cellphone'           => 'tel_hankaku_num|max:20',
        ];
    }
   
    public static function messages()
    {
        return [
            'customer_kana.all_em_kana'         => ':attributeは全角カタカナで入力してください。',
            'representative_kana.all_em_kana'   => ':attributeは全角カタカナで入力してください。',
            'tel.tel_hankaku_num'               => ':attributeは半角数字で入力してください。',
            'fax.tel_hankaku_num'               => ':attributeは半角数字で入力してください。',
            'cellphone.tel_hankaku_num'         => ':attributeは半角数字で入力してください。',
        ];
    }    
    
    protected function validationData()
    {
        $data = parent::all();

        return $data;
    }        
}
