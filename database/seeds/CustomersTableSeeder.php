<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000001',
            'customer_name'       => '沖縄県立辺土名高等学校',
            'customer_kana'       => 'オキナワケンリツヘントナコウトウガッコウ',
            'representative_name' => '沖縄県立辺土名高等学校長',
            'representative_kana' => 'オキナワケンリツヘントナコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '新庄利治',
            'zip'      			  => '905-1304',
            'address'      		  => '沖縄県国頭郡大宜味村饒波2015',
            'tel'      			  => '0980443103',
            'fax'      			  => '',
            'cellphone'       	  => '00012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000002',
            'customer_name'       => '沖縄県立北山高等学校',
            'customer_kana'       => 'オキナワケンリツホクザンコウトウガッコウ',
            'representative_name' => '沖縄県立北山高等学校長',
            'representative_kana' => 'オキナワケンリツホクザンコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '安谷屋正良',
            'zip'      			  => '905-0424',
            'address'      		  => '沖縄県国頭郡今帰仁村仲尾次540-1',
            'tel'      			  => '0980562401',
            'fax'      			  => '',
            'cellphone'       	  => '01012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000003',
            'customer_name'       => '沖縄県立本部高等学校',
            'customer_kana'       => 'オキナワケンリツモトブコウトウガッコウ',
            'representative_name' => '沖縄県立本部高等学校長',
            'representative_kana' => 'オキナワケンリツモトブコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '友利公啓',
            'zip'      			  => '905-0214',
            'address'      		  => '沖縄県国頭郡本部町渡久地377',
            'tel'      			  => '0980472418',
            'fax'      			  => '',
            'cellphone'       	  => '02012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000004',
            'customer_name'       => '沖縄県立名護高等学校',
            'customer_kana'       => 'オキナワケンリツナゴコウトウガッコウ',
            'representative_name' => '沖縄県立名護高等学校長',
            'representative_kana' => 'オキナワケンリツナゴコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '東章憲',
            'zip'      			  => '905-0018',
            'address'      		  => '沖縄県名護市大西5-17-1',
            'tel'      			  => '0980522615',
            'fax'      			  => '',
            'cellphone'       	  => '03012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000005',
            'customer_name'       => '沖縄県立宜野座高等学校',
            'customer_kana'       => 'オキナワケンリツギノザコウトウガッコウ',
            'representative_name' => '沖縄県立宜野座高等学校長',
            'representative_kana' => 'オキナワケンリツギノザコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '神谷隆博',
            'zip'      			  => '904-1302',
            'address'      		  => '沖縄県国頭郡宜野座村宜野座１',
            'tel'      			  => '0989688311',
            'fax'      			  => '',
            'cellphone'       	  => '04012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000006',
            'customer_name'       => '沖縄県立石川高等学校',
            'customer_kana'       => 'オキナワケンリツイシカワコウトウガッコウ',
            'representative_name' => '沖縄県立石川高等学校長',
            'representative_kana' => 'オキナワケンリツイシカワコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '比嘉徳秀',
            'zip'      			  => '904-1115',
            'address'      		  => '沖縄県うるま市石川伊波861',
            'tel'      			  => '0989642006',
            'fax'      			  => '',
            'cellphone'       	  => '05012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000007',
            'customer_name'       => '沖縄県立前原高等学校',
            'customer_kana'       => 'オキナワケンリツマエハラコウトウガッコウ',
            'representative_name' => '沖縄県立前原高等学校長',
            'representative_kana' => 'オキナワケンリツマエハラコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '徳村永秀',
            'zip'      			  => '904-2213',
            'address'      		  => '沖縄県うるま市田場1827',
            'tel'      			  => '0989733249',
            'fax'      			  => '',
            'cellphone'       	  => '06012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000008',
            'customer_name'       => '沖縄県立与勝高等学校',
            'customer_kana'       => 'オキナワケンリツヨカツコウトウガッコウ',
            'representative_name' => '沖縄県立与勝高等学校長',
            'representative_kana' => 'オキナワケンリツヨカツコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '前田孝志',
            'zip'      			  => '904-2312',
            'address'      		  => '沖縄県うるま市勝連平安名3248',
            'tel'      			  => '0989785230',
            'fax'      			  => '',
            'cellphone'       	  => '07012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000009',
            'customer_name'       => '沖縄県立読谷高等学校',
            'customer_kana'       => 'オキナワケンリツヨミタンコウトウガッコウ',
            'representative_name' => '沖縄県立読谷高等学校長',
            'representative_kana' => 'オキナワケンリツヨミタンコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '翁長博',
            'zip'      			  => '904-0303',
            'address'      		  => '沖縄県中頭郡読谷村伊良皆198',
            'tel'      			  => '0989562157',
            'fax'      			  => '',
            'cellphone'       	  => '08012345678',
        ]);
        factory(Customer::class)->create([
            'customer_id'         => 'CUS00000010',
            'customer_name'       => '沖縄県立嘉手納高等学校',
            'customer_kana'       => 'オキナワケンリツカデナコウトウガッコウ',
            'representative_name' => '沖縄県立嘉手納高等学校長',
            'representative_kana' => 'オキナワケンリツカデナコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '長濱玲子',
            'zip'      			  => '904-0202',
            'address'      		  => '沖縄県中頭郡嘉手納町屋良806',
            'tel'      			  => '0989563336',
            'fax'      			  => '',
            'cellphone'       	  => '09012345678',
        ]);
        
//        factory(Customer::class, 20)->create();

    }
}
