<?php
 
namespace App\Validation;
use Auth;
use Hash;
use Request;

class CustomValidator extends \Illuminate\Validation\Validator 
{
    /**
     * 半角英字チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaCheck($attribute, $value, $parameters)
    {
        return preg_match('/^[A-Za-z]+$/', $value);
    }
      
    /**
     * 半角英数字チェック
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaNumCheck($attribute, $value, $parameters)
    {
        return !is_null($value)?preg_match('/^[A-Za-z\d]+$/', $value):true;
    }
      
    /**
     * 半角英数字チェック(_ and -)
     *
     * @param string $value
     * @return bool
     */
    public function validateAlphaDashCheck($attribute, $value, $parameters)
    {
        return preg_match('/^[A-Za-z\d_-]+$/', $value);
    }    
    
    /**
     * 日本の電話番号かチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateTelHankakuNum($attribute, $value, $parameters)
     {
        return !is_null($value)?preg_match('/^[0-9]+$/', $value):true;
     }
    
    /**
     * すべて全角カタカナかチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllEmKana($attribute,$value,$parameters)
     {
        //全角カタカナ
        if (preg_match('/^[ァ-ヾ－ー\s　]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }
     
    /**
     * すべて半角カタカナかチェックする
     *
     * @param string $value
     * @return bool
     */
     public function validateAllEnKana($attribute, $value)
     {
        //半角カタカナ
        if (preg_match('/^[ｦ-ﾟ\s ]+$/u', $value)) {
            return true;
        } else {
            return false;
        }
     }
     
    /**
     * 入力されたパスワードとDBのパスワードが合致しているかチェック
     *
     * @param string $value
     * @return bool
     */
     public function validatePassConfirmHash($attribute, $value)
     {
        $userInfo = Auth::user();

        //現在のユーザーパスワードをDBから取得
        $hashedPassword = $userInfo->password;

        //入力された現在パスワードとDBのパスワードが合致しているかチェック
        if (Hash::check($value, $hashedPassword)){
            return true;
        } else {
            return false;
        }
     }

    /**
     * 指定したパラメータの日付より過去日付かチェックする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateDateAfter($attribute, $value, $parameters)
     {
        
        $this->requireParameterCount(2, $parameters, 'date_after');
        
        $data = $this->getValue($parameters[0]);
        $ymd_if = $parameters[1];
        
        if($ymd_if == "Y") {
            $dataYMD = $data."/01/01";
            $valueYMD = $value."/01/01";
        } elseif($ymd_if == "YM") {
            $dataYMD = $data."/01";
            $valueYMD = $value."/01";
        } elseif($ymd_if == "YMD") {
            $dataYMD = $data;
            $valueYMD = $value;
        }

        if(strtotime($dataYMD) > strtotime($valueYMD)){
            return false;
        }
        return true;
        
     }
     
    /**
     * date_after エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceDateAfter($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getDisplayableValue($parameters[0], $this->getValue($parameters[0]));
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    } 

    /**
     * 指定したパラメータの日付より未来日付かチェックする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateDateBefore($attribute, $value, $parameters)
     {
        
        $this->requireParameterCount(2, $parameters, 'date_before');
        
        $data = $this->getValue($parameters[0]);
        $ymd_if = $parameters[1];

        if($ymd_if == "Y") {
            $dataYMD = $data."/01/01";
            $valueYMD = $value."/01/01";
        } elseif($ymd_if == "YM") {
            $dataYMD = $data."/01";
            $valueYMD = $value."/01";
        } elseif($ymd_if == "YMD") {
            $dataYMD = $data;
            $valueYMD = $value;
        }

        if(strtotime($dataYMD) < strtotime($valueYMD)){
            return false;
        }
        return true;
        
     }
     
    /**
     * date_before エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceDateBefore($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getDisplayableValue($parameters[0], $this->getValue($parameters[0]));
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    } 
    
    /**
     * 指定パラメータの入力値が指定したパラメータ比較条件の時、指定したパラメータの必須入力チェックをする
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
     public function validateRequiredIfValue($attribute, $value, $parameters)
     {

        $this->requireParameterCount(3, $parameters, 'required_if_value');
        
        $attribute_data = $parameters[0];
        $required_data = $this->getValue($parameters[0]);
        $chk_value = $parameters[1];
        $value_if = $parameters[2];
        
        if($value_if == "=") {
            if((int)$value == (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == ">") {
            if((int)$value > (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == ">=") {
            if((int)$value >= (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == "<") {
            if((int)$value < (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        } elseif($value_if == "<=") {
            if((int)$value <= (int)$chk_value) {
                if (!$this->validateRequired($attribute_data, $required_data)) {
                    return false;
                }
            }
        }

        return true;
        
     }
     
    /**
     * date_before エラーメッセージを作成する
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    public function replaceRequiredIfValue($message, $attribute, $rule, $parameters)
    {
        $parameters[1] = $this->getValue($attribute);
        $parameters[0] = $this->getAttribute($parameters[0]);
        return str_replace([':other', ':value'], $parameters, $message);
    }
}

