<div class="modal fade" id="modal_switch" role="dialog" aria-labelledby="modal_switch_label" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
            <div class="modal-header" style="background-color:#f5f5f5; -webkit-border-top-left-radius: 10px; -webkit-border-top-right-radius: 10px;">
                <div class="row">
                    <div class="col-md-11">
                        <h4 class="modal-title" id="modal_switch_label">得意先選択</h4>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <!-- modal-body -->
            <div class="modal-body">
                <!-- col-md-12 -->
                <div class="col-md-12">
                    <!-- panel panel-default -->
                    <div class="panel panel-default">

                        <!-- panel-heading headingOne --> 
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h5 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="" href="#collapseOne_customer" aria-expanded="true" aria-controls="collapseOne">
                                    <b>検索条件</b>
                                    <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                                </a>
                            </h5>
                        </div>
                        <!-- panel-heading headingOne --> 

                        <!-- collapseOne_customer headingOne --> 
                        <div id="collapseOne_customer" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <!-- panel-body --> 
                            <div class="panel-body">
                                <!-- estimates_egistration --> 
                                <div class="estimates_egistration">

                                    <!-- estimates_list_box1 --> 
                                    <div class="cus_list_box1">
                                        <div class="col-md-12">
                                            <div class="panel-body" style="">
                                                <div class="form-group">
                                                    <label class="col-md-3">得意先名</label>
                                                    <div class="col-md-9">
                                                        <input id="s_customer_name" class="form-control ja" value="">
                                                    </div>
                                                    <br><br>
                                                    <label class="col-md-3">得意先名カナ</label>
                                                    <div class="col-md-9">
                                                        <input id="s_customer_kana" class="form-control ja" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- estimates_list_box1 -->

                                    <!-- estimates_list_box2 --> 
                                    <div class="cus_list_box2">
                                        <div class="col-md-4">
                                            <div class="panel-body"><div class="form-group"></div></div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <button id="search_switch" type="button" class="btn btn-primary">検索</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- estimates_list_box -->

                                    <div class="clear"></div>
                                </div>
                                <!-- estimates_egistration -->
                            </div>
                            <!-- panel-body -->
                        </div>
                        <!-- collapseOne_customer headingOne -->

                    </div>
                    <!-- panel panel-default -->                
                </div>
                <!-- col-md-12 -->
                
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- panel -->
                        <div class="panel">
                            <!-- panel-body -->
                            <div class="panel-body">
                                
                                <!-- panel-group -->
                                <div class="panel-group">
                                    <div class="order_detail_info_wrap">
                                        <table class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame estimate_list">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="detail_title">得意先No</th>
                                                    <th class="detail_title">得意先名</th>
                                                    <th class="detail_title">担当者</th>
                                                    <th class="detail_title">住所</th>
                                                    <th class="detail_title">TEL</th>
                                                    <th class="detail_title">選択</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- panel-group -->
                                
                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <div id="info_switch" class="">検索結果： 0件 / 0件</div>
                                        <ul id="pagination_switch" class="pagination-sm"></ul>
                                    </div>
                                </div>
                                <!-- row -->
                                
                            </div>
                            <!-- panel-body -->
                        </div>
                        <!-- panel -->
                    </div>
                </div>
                <!-- /row -->
              
            </div>
            <!-- modal-body -->

        </div>
    </div>
</div>

<script type='text/javascript' charset='utf-8'>
	$( document ).ready( function() {
        $( '#modal_switch' ).on( 'show.bs.modal', function () {
            fillSwitchTable();
        });
	    $( '#search_switch' ).on( 'click', function() {
  				fillSwitchTable();
	    });
	});

  function fillSwitchTable(page) {
        if ( typeof page === "undefined" || page === null ) {
	        page = 1;
            
            //active表示のため一時追加
			$( '#pagination_switch' ).find( 'li' ).remove();
            var pagination = $('#pagination_switch');
			pagination.twbsPagination('destroy');
        }
        $( '#modal_switch' ).find( 'tbody' ).find( 'a' ).off( 'click' );
        $( '#modal_switch' ).find( 'tbody' ).find( 'tr' ).remove();
        $( '#info_switch' ).text( '' );
        $( '#modal_switch' ).find( 'tbody' ).append( $( '<tr>' ).append( $( '<td>' ).attr({ colspan: 8 }).text( '情報取得中' ) ) );
//			$( '#pagination_switch' ).find( 'li' ).remove();   //active表示のため一時コメント
        $.ajax({
                url: "{{ url('estimates/select') }}",
                type: 'post',
                dataType: 'json',
                data: { s_customer_name: $( '#s_customer_name' ).val(),  s_customer_kana: $( '#s_customer_kana' ).val(), page: page },
                fail: function () {
                    $( '#modal_switch' ).find( 'tbody' ).find( 'tr' ).remove();
                        $( '#info_switch' ).text( '検索結果： 0件 / 0件' );
                        $( '#modal_switch' ).find( 'tbody' ).append( $( '<tr>' ).append( $( '<td>' ).attr({ colspan: 8 }).text( '情報取得失敗' ) ) );
                },
                success: function ( response ) {
                        $( '#modal_switch' ).find( 'tbody' ).find( 'tr' ).remove();
                        $( '#info_switch' ).text( '検索結果： ' + response.info.showing + '件 / ' + response.info.total + '件' );
                        if( typeof response.data == 'undefined' || response.data == null || response.data.length < 1) {
                                $( '#modal_switch' ).find( 'tbody' ).append( $( '<tr>' ).append( $( '<td>' ).attr({ colspan: 8 }).text( '検索結果なし' ) ) );
                                return
                        }

                        var data = Object.keys( response.data ).map( function ( key ) {
                             return response.data[key];
                        });

                        data.sort( function( a, b ) {
                            if ( a.created_at < b.created_at ) return -1;
                            if ( a.created_at > b.created_at ) return 1;
                            return 0;
                        });

                        $.each( data, function( key, value ) {
                              console.log(value.created_at);
                                var row = $( '<tr>' );
                                row.append( $( '<td>' ).text( value.id ).attr('class', 'contents_box_right') );
                                row.append( $( '<td>' ).text( value.customer_name ) );
                                row.append( $( '<td>' ).text( value.person_name ) );
                                row.append( $( '<td>' ).text( value.address ) );
                                row.append( $( '<td>' ).text( value.tel ) );
                                row.append( $( '<td>' ).append( $( '<a>' ).text( '選択' )).attr('class', 'contents_box_center') );
                                row.find( 'a' ).on( 'click', function() {
//											$( '#lbl_customer_name' ).text( value.customer_name );
                                        $( '#customer_name' ).val( value.customer_name );
                                        $( '#select_switch' ).find( 'input' ).val( value.id );
                                        $( '#select_switch_b' ).find( 'input' ).val( value.customer_name );
                                        $( '#modal_switch' ).modal( 'hide' );
                                });
                                row.find( 'a' ).attr('class', 'btn btn-success btn-sm');
                                $( '#modal_switch' ).find( 'tbody' ).append( row );
                        });
                        setupPagination( response.pagination );
                }
        });
	}

	function setupPagination(config, pages) {
        var pagination = $('#pagination_switch');

//			pagination.twbsPagination('destroy');   //active表示のため一時コメント
        pagination.twbsPagination({
              initiateStartPageClick: true,
              startPage: config.startPage,
              totalPages: config.totalPages,
              hideOnlyOnePage: true,
                first: null,
                last: null,
                prev: '<',
                next: '>',
              onPageClick: function (event, page) {
//					    if (page != config.startPage) {   //active表示のため一時コメント
                      fillSwitchTable(page);
//					    }
              },
        });
	}

</script>