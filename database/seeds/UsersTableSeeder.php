<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
              'user_id'   => 'admin',
              'user_name' => 'システム管理者',
              'password'  => '$2y$10$0WzFBxUgJtZsZDKvCXuIRe8/9kqPc5LiFrbPsBSCRfnn1KuEDVSVy',    //hogehoge
            ],
        ]);
        
//        factory(User::class, 50)->create();
    }
}
