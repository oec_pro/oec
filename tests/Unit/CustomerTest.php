<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\CustomersController;

class CustomerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    
    /** 
     * 各テストメソッドが実行される前に実行する 
     */
    protected function setUp() { 
        // テストするオブジェクトを生成する
        $this->object = new CustomersController();
    }
    
    /**
     * edit
     */
    public function testedit() {
        // 引数に3,5を渡すと8が返ってくることを確認する
        $this->assertEquals(8, $this->object->add(3));
    }    
    
}
