<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estimate_detail extends Model
{
    use SoftDeletes;
 
    protected $table = 'estimate_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'esti_no',
        'usr_esti_no',
        'esti_line_no',
        'item_id',
        'item_name',
        'item_description',
        'quantity',
        'unit',
        'sal_price',
        'sal_amount',
        'cost_price',
        'cost_amount',
        'tax_flg',
        'item_remarks',
        'created_name',
        'updated_name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];    
    
}
