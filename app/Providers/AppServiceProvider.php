<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(\Laravel\Dusk\DuskServiceProvider::class);
        }
        
        // ログレベルの設定
        $monolog = Log::getMonoLog();
        foreach ($monolog->getHandlers() as $handler) {
            $handler->setLevel(env('APP_LOG_LEVEL', 'error'));
        }        
        
//        // fakerを日本語にする
//        $this->app->singleton(FakerGenerator::class, function () {
//            return FakerFactory::create('ja_JP');
//        });        
    }
}
