<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('const.packagetitle') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ Config::get('const.core') }}/assets/bootstrap/vendors/drawer/css/drawer.css">
    
    <!-- Bootstrap -->
    <link href="{{ Config::get('const.core') }}/assets/bootstrap/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ Config::get('const.core') }}/assets/bootstrap/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ Config::get('const.core') }}/assets/bootstrap/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datepicker -->
    <link href="{{ Config::get('const.core') }}/assets/bootstrap/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet">

    <!-- Custom Styles CSS -->
    
    <!-- jQuery -->

    <!-- ajax zip -->
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/ajaxzip3/ajaxzip3.js" ></script>

    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/jquery/dist/jquery-ui.min.js"></script>
    
</head>
<body role="document" class="container_body">
    <div id="app">
        <div class="container drawer drawer--left">
            
            <!-- Authentication Links -->
            @guest
                <!-- navbar navbar-inverse navbar-fixed-top -->
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <!-- patern03 -->
                    <div id="patern03" class="collapse navbar-collapse navi_style">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ route('login') }}">ログイン　　</a></li>
<!--                            <li><a href="{{ route('register') }}">新規登録</a></li> -->
                        </ul>
                    </div><!-- patern03 -->
                </nav><!-- navbar navbar-inverse navbar-fixed-top --> 
            @else
                <!-- navbar navbar-inverse navbar-fixed-top -->
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <button id="menu" type="button" class=" drawer-toggle drawer-hamburger">
                        <span class="sr-only">toggle navigation</span>
                        <span class="drawer-hamburger-icon"></span>
                    </button>

                    <!-- drawer -->
                    <nav class="drawer-nav" role="navigation">
                        <ul class="drawer-menu">
                            <li class="drawer-brand"><i class="fa fa-align-justify" aria-hidden="true"></i><span class="icon_style">MENU</span></li>
                            <li id="estimates" class="drawer-dropdown">
                                <a id="estimates" class="drawer-menu-item" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                                    見積管理
                                    <span class="drawer-caret"></span>
                                </a>
                                <ul class="drawer-dropdown-menu">
                                    <li><a id="customers_regist" class="drawer-dropdown-menu-item" href="{{ action('EstimatesController@edit', '') }}"><span class="icon_style_contents_sub">見積登録</span></a></li>
                                    <li><a id="customers_list" class="drawer-dropdown-menu-item" href="{{ action('EstimatesController@index') }}"><span class="icon_style_contents_sub">見積一覧</span></a></li>
                                </ul>
                            </li>
                            <li id="master" class="drawer-dropdown">
                                <a id="master" class="drawer-menu-item" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                                    マスタ管理
                                    <span class="drawer-caret"></span>
                                </a>
                                <ul class="drawer-dropdown-menu">
                                    <li><a id="customers" class="drawer-dropdown-menu-item" href="{{ action('CustomersController@index') }}"><span class="icon_style_contents_sub">得意先マスタ</span></a></li>
                                    <li><a id="taxrates" class="drawer-dropdown-menu-item taxrates" href="{{ action('TaxratesController@index') }}"><span class="icon_style_contents_sub">税率マスタ</span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav><!-- drawer -->

                    <!-- patern03 -->
                    <div id="patern03" class="collapse navbar-collapse navi_style">
                        <ul id="navlist" class="nav navbar-nav navbar-right">
                            <li><a href="{{ route('register') }}">新規登録</a></li>
                            <li class="dropdown style_margin_left">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    <span class="navbar_icon">{{ Auth::user()->user_name }}</span>
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a id="userEdit" href="{{ action('UsersController@edit') }}"><i class="fa fa-key" aria-hidden="true"></i><span class="navbar_icon">プロパティ</span></a></li>
                                    <li class="divider"></li>
                                    <li>
                                        <a id="testLogout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                            <span class="navbar_icon">ログアウト</span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>                                
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- patern03 -->

                </nav><!-- navbar navbar-inverse navbar-fixed-top --> 
            @endguest
            
        </div>
    
        @yield('content')
        
        <!-- footer -->
        <footer class="footer">
                <div class="pull-right">
                    <div class="footer_company_name">○○○</div>
                    <div class="footer_company_name">CopyRight &copy; ○○○ co. All Rights Reserved.</div>
                </div>
        </footer>
        <!-- //footer -->
        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <!-- Bootstrap -->
    <!--<script type="text/javascript" src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/bootstrap/dist/js/bootstrap.min.js"></script>-->

    <!-- bootstrap-daterangepicker -->
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/datepicker/locales/bootstrap-datepicker.ja.min.js"></script>

    <!-- tablednd -->
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/tablednd/jquery.tablednd.js" ></script>
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/tablednd/jquery.tablednd.min.js" ></script>    
    
    <!-- iscroll -->
    <script src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/iscroll/iscroll.js"></script>
    
    <!-- drawer -->
    <script src="{{ Config::get('const.core') }}/assets/bootstrap/vendors/drawer/js/drawer.min.js"></script>
    
    <!-- twbsPagination -->
    <script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/pagination/jquery.twbsPagination.min.js"></script>
    
    <!-- bootstrap-datepicker -->
    <script type="text/javascript" charset="utf-8">
        $(function () {
            'use strict';

            // 日付選択
            $('.input-group.date').datepicker({
                language: 'ja',
                format: 'yyyy/mm/dd'
            })
        });
    </script>
    <!-- /bootstrap-datepicker -->
    
    <!-- tooltip -->
    <script type="text/javascript">
      $(function () {
        $('[data-toggle="tooltip"]').tooltip();
      });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            $(".drawer").drawer();
            
        });
    </script>    

</body>
</html>
