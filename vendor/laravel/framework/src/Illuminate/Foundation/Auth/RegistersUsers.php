<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // バリデーション
        $this->validator($request->all())->validate();

        // ユーザー登録のLaravelシステムイベント発行
        event(new Registered($user = $this->create($request->all())));

        // 登録時はログインさせない 20180201 add onaga
        // $this->guard()->login($user);

//        return $this->registered($request, $user)
//                        ?: redirect($this->redirectPath());
        
        // 登録画面で完了メッセージを表示する 20180201 add onaga
        return redirect('register')->with('regist_success', 'ユーザー登録処理が完了しました。');        
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
