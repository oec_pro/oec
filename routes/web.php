<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index')->name('top');


Route::get('/top', 'HomeController@index')->name('home');

/* 
|--------------------------------------------------------------------------
| ユーザー管理
|--------------------------------------------------------------------------
 */
Auth::routes();

//ユーザー編集初期画面
Route::get('users/edit', 'UsersController@edit');
//ユーザー編集処理
Route::post('users/update', 'UsersController@update');

/* 
|--------------------------------------------------------------------------
| 税率マスタ
|--------------------------------------------------------------------------
 */
//初期表示
Route::get('taxrates', 'TaxratesController@index');
//検索
Route::get('taxrates/search', 'TaxratesController@search');
//登録更新画面
Route::get('taxrates/edit', 'TaxratesController@edit');
Route::get('taxrates/edit/{id}', 'TaxratesController@edit');
//登録更新処理
Route::post('taxrates/save', 'TaxratesController@save');
//論理削除処理
Route::get('taxrates/del/{id}', 'TaxratesController@del');

/* 
|--------------------------------------------------------------------------
| 得意先マスタ
|--------------------------------------------------------------------------
 */
//初期表示
Route::get('customers', 'CustomersController@index');
//検索
Route::get('customers/search', 'CustomersController@search');
//登録更新画面
Route::get('customers/edit', 'CustomersController@edit');
Route::get('customers/edit/{id}', 'CustomersController@edit');
//登録更新処理
Route::post('customers/save', 'CustomersController@save');
//論理削除処理
Route::get('customers/del/{id}', 'CustomersController@del');
//得意先の選択
Route::post('customers/selectcustomers', 'CustomersController@getSelectCustomers');

/* 
|--------------------------------------------------------------------------
| 見積管理
|--------------------------------------------------------------------------
 */
//初期表示
Route::get('estimates', 'EstimatesController@index');
//検索
Route::get('estimates/search', 'EstimatesController@search');
//登録更新画面
Route::get('estimates/edit', 'EstimatesController@edit');
Route::get('estimates/edit/{id}', 'EstimatesController@edit');
//登録更新処理
Route::post('estimates/save', 'EstimatesController@save');
//論理削除処理
Route::get('estimates/del/{id}', 'EstimatesController@del');
//論理削除処理
Route::get('estimates/close/{id}', 'EstimatesController@close');
//サブ画面として表示
Route::any('estimates/select', 'EstimatesController@select');
//見積書出力
Route::get('estimates/xlsprint/{out_type}/{id}','EstimatesController@xlsprint');
//CSVエクスポート-見積一覧
Route::get('estimates/download/list','EstimatesController@downlist');
//CSVエクスポート-見積明細
Route::get('estimates/download/detail/{id}','EstimatesController@downdetail');
