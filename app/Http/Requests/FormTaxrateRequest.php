<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormTaxrateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
         return [];
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
           'effective_date' => 'required|date',
           'tax_rate'       => 'required|integer|min:0|max:99',
        ];
    }
    
    public function messages()
    {
        return [];
    }    
}
