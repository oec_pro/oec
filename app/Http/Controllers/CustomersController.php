<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Customer;
use App\Http\Requests\FormCustomerRequest;
use Config;
use DB;
use Input;
use Request;
use Response;
use Validator;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * 初期表示
     *
     * @return Response
     */
    public function index()
    {
        $query = Customer::query();
        
        $data['results'] = $query->orderBy('id', 'desc')->paginate(\Config::get('const.paginate_20'));
        $data['search']  = null;

        return view('customers.index', compact('data'));

    }   
    
    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        $prms = Request::all();
        
        //query
        $query = \App\Customer::query();

        //得意先名
        if (isset($prms['customer_name']) && !empty($prms['customer_name'])){
            $query->where('customer_name','like','%'.$prms['customer_name'].'%');
        }
        //得意先名カナ
        if (isset($prms['customer_kana']) && !empty($prms['customer_kana'])){
            $query->where('customer_kana','like','%'.$prms['customer_kana'].'%');
        }
        //TEL
        if (isset($prms['tel']) && !empty($prms['tel'])){
            $query->where('tel','like','%'.$prms['tel'].'%');
        }
        //住所
        if (isset($prms['address']) && !empty($prms['address'])){
            $query->where('address','like','%'.$prms['address'].'%');
        }
        //携帯番号
        if (isset($prms['cellphone']) && !empty($prms['cellphone'])){
            $query->where('cellphone','like','%'.$prms['cellphone'].'%');
        }

        //paginate
        $data['results'] = $query->orderBy('id', 'desc')->paginate(Config::get('const.paginate_20'))->appends($prms);
        
        return view('customers.index', compact('data'));
    }    
    
    /**
     * 登録更新画面へ遷移
     *
     * @return Response
     */
    public function edit($id=null)
    {
        $data = new Customer();
		if (is_numeric($id)) {
            $data = Customer::find($id);
            if (is_null($data)) {
                //一覧画面へ遷移
                return redirect('customers');
            }
        }
        
        //郵便番号
        if ( !empty( $data['zip']) ) {
            $arrZip = explode("-", $data['zip']);
            $data['zip1'] = $arrZip[0];
            $data['zip2'] = $arrZip[1];
        }
        
        return view('customers.edit', compact('data'));
    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
//    public function save(Request $request)
    public function save()
    {
        //入力データ取得
        $data = Request::all();
//        $data = $request;
        
        /* ----------------------------------------------
         * バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make($data,
            FormCustomerRequest::rules(),
            FormCustomerRequest::messages()
        );

        // バリデーションエラーだった場合
        if ($validator->fails()) {
            $data['confirm'] = false;
            return view('customers.edit', compact('data'))->withErrors($validator);
        }        

        /* ----------------------------------------------
         * 画面遷移
         * ----------------------------------------------*/
        //入力画面へ遷移
        if (Input::get('back')) {
            return view('customers.edit', compact('data'));
        }
        
        //確認画面へ遷移
        if (Input::get('confirm')) {
            $data['confirm'] = true;
            return view('customers.edit', compact('data'));
        }
        
        /* ----------------------------------------------
         * 登録更新 
         * ----------------------------------------------*/
        //ID情報の取得
        $tbls = Customer::find($data["id"]);
        if (is_null($tbls)) {
            $tbls = new Customer;
        }
        
        //データセット
        $tbls->customer_id         = $this->getMakeCustomerId($data["customer_id"]);
        $tbls->customer_name       = $data["customer_name"];
        $tbls->customer_kana       = $data["customer_kana"];
        $tbls->representative_name = $data["representative_name"];
        $tbls->representative_kana = $data["representative_kana"];
        $tbls->department_name     = $data["department_name"];
        $tbls->person_name         = $data["person_name"];
        $tbls->zip                 = ($data["zip1"]!="" && $data["zip2"]!="")?$data["zip1"]."-".$data["zip2"]:"";
        $tbls->address             = $data["address"];
        $tbls->tel                 = $data["tel"];
        $tbls->fax                 = $data["fax"];
        $tbls->cellphone           = $data["cellphone"];
        
        //登録更新処理
        if ( !$tbls->save() ) {
            return view('customers.edit', compact('data'));
        }
        
        return redirect('customers');
    }    
    
    /**
     * 登録更新処理
     *
     * @return Response
     */
//    public function save(FormCustomerRequest $request)
//    {
//        $data = $request;
//        
//        //ID情報の取得
//        $tbls = Customer::find($data->id);
//        if (is_null($tbls)) {
//            $tbls = new Customer;
//        }
//
//        //入力画面へ遷移
//        if (Input::get('back')) {
//            return view('customers.edit', compact('data'));
//        }
//        
//        //確認画面へ遷移
//        if (Input::get('confirm')) {
//            $data['confirm'] = true;
//            return view('customers.edit', compact('data'));
//        }
//        
//        //データセット
//        $tbls->customer_id         = $this->getMakeCustomerId($data->customer_id);
//        $tbls->customer_name       = $data->customer_name;
//        $tbls->customer_kana       = $data->customer_kana;
//        $tbls->representative_name = $data->representative_name;
//        $tbls->representative_kana = $data->representative_kana;
//        $tbls->department_name     = $data->department_name;
//        $tbls->person_name         = $data->person_name;
//        $tbls->zip                 = ($data->zip1!="" && $data->zip2!="")?$data->zip1."-".$data->zip2:"";
//        $tbls->address             = $data->address;
//        $tbls->tel                 = $data->tel;
//        $tbls->fax                 = $data->fax;
//        
//        //登録更新処理
//        if ( !$tbls->save() ) {
//            return view('customers.edit', compact('data'));
//        }
//        
//        return redirect('customers');
//    }    
    
    /**
     * 論理削除処理
     *
     * @return Response
     */
    public function del($id)
    {
		if(!empty($id)){
            $sql = Customer::find($id);
			$sql->delete();
		}
        return redirect('customers');
    }    

    /**
     * 得意先ID、得意先名の取得
     *
     * @return Response
     */
    public function getSelectCustomers()
    {
        $id = Input::get('id');
        
        $data = new Customer();
		if (is_numeric($id)) {
            $data = Customer::find($id);
        }
        
        return response()->json(
                [
                    'cud_id' => $data['id'],
                    'cud_nm' => $data['customer_name']
                ],
                200,[],
                JSON_UNESCAPED_UNICODE
            );
    }        
    
	//**************************************************************
	//顧客ID生成
	//CUS＋8桁0埋め連番
	//例:IDが1の場合、CUS00000000001
	//**************************************************************
	private function getMakeCustomerId($id) {
		$val = $id;
		$HEAD  = "CUS";

		//新規登録
		if ($id == "") {
			//最大IDの取得
			$result = DB::table('customers')->orderBy('id', 'desc')->take(1)->get();
			if (!empty($result[0])) {
				//idの格納
				$getId = $result[0]->id;
				//0埋め
				$val = $HEAD .  sprintf('%08d', ($getId + 1));
			}else{
				$val = $HEAD . "00000001";
			}
		}

		return $val;

	}       
}
