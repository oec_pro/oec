<?php
namespace Tests\Browser;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class LoginTest extends DuskTestCase
{
    // Dusk実行前にマイグレーションする
    use DatabaseMigrations;
    
    /**
     * ユーザーに関する操作(作成、編集、削除)のテスト
     *
     * @return void
     */
    public function testCRUD()
    {
        $user = factory(User::class)->create([]);
        
        // ユーザーを新規に作成・保存
        $user_new = factory(User::class)->make([
            'user_id'               => 'hoge',
            'user_name'             => 'ほげほげ',
            'password'              => 'hogehoge',
            'password-confirm'      => 'hogehoge',
            
        ]);
        
        // 編集する内容
        $update = factory(User::class)->make([
            'user_id'               => 'hoge',
            'user_name'             => 'ほげ太郎',
            'password'              => 'mogemoge',
            'password_confirmation' => 'mogemoge',
        ]);

        $this->browse(function (Browser $browser) use ($user, $user_new, $update) {
        $browser->loginAs($user)                                     // ログインする
                ->visit('/register')
                /* --------------------------------
                 * 新規登録 
                 *--------------------------------- */
                ->type('user_id',                $user_new->user_id)             // 入力:ユーザーID
                ->type('user_name',              $user_new->user_name)           // 入力:ユーザー名
                ->type('password',               $user_new->password)            // 入力:パスワード
                ->type('password_confirmation',  $user_new->password)            // 入力:パスワード確認
                ->press('insert')                                                // 押下:「新規登録」ボタン
                ->assertPathIs('/register')                                      // 確認:見積一覧であること
                ->assertSee('ユーザー登録処理が完了しました。')                    // 確認:メッセージ

                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#testLogout')                                           // 押下:「プロパティ」ボタン
                ->assertPathIs('/login')                                         // 確認:見積一覧であること
                ->type('user_id',               $user_new->user_id)                // ユーザーIDを入力
                ->type('password',              $user_new->password)               // パスワードを入力
                ->press('Login')                                                 // 送信ボタンをクリック
                ->assertPathIs('/estimates')                                    // 確認:見積一覧であること
            
                /* --------------------------------
                 * 編集 - ユーザー名変更
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#userEdit')                                             // 押下:「プロパティ」ボタン
                ->assertPathIs('/users/edit')
                ->type('user_name',              $update->user_name)             // 入力:得意先名
                ->press('confirm')                                               // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                  // 確認:確認ページであること
                ->press('insert')                                                // 押下:「登録」ボタン
                ->assertPathIs('/estimates')                                     // 確認:見積一覧であること
                ->assertSee($update->user_name)
                /* --------------------------------
                 * 編集 - パスワード変更
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#userEdit')                                             // 押下:「プロパティ」ボタン
                ->assertPathIs('/users/edit')
                ->type('password',               $update->password)              // 入力:パスワード
                ->type('password_confirmation',  $update->password_confirmation) // 入力:パスワード確認
                ->press('confirm')                                               // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                  // 確認:確認ページであること
                ->press('insert')                                                // 押下:「登録」ボタン
                ->assertPathIs('/estimates')                                     // 確認:見積一覧であること
                /* --------------------------------
                 * ログアウト - 再度ログインできるか
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#testLogout')                                           // 押下:「プロパティ」ボタン
                ->assertPathIs('/login')                                         // 確認:見積一覧であること
                ->type('user_id',               $update->user_id)                // ユーザーIDを入力
                ->type('password',              $update->password)               // パスワードを入力
                ->press('Login')                                                 // 送信ボタンをクリック
                ->assertPathIs('/estimates');                                    // 確認:見積一覧であること
        });
    }        
    
    /**
     * バリデーションの動作を確認する (loginアクション)
     *
     * @return void
     */
    public function testValidationLogin()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create([
            'user_id'   => 'hoges',
            'user_name' => 'ほげほげ',
        ]);
        
        // 作成のみを行う。保存はしない
        $update = factory(User::class)->make([
            'user_id_require'   => '',
            'password_require'  => '',
            'user_id'           => 'hoges',
            'user_name'         => 'ほげほげほげ',
        ]);
        
        $this->browse(function (Browser $browser) use ($user, $update) {
            $browser->visit('/login')                    // ログインページへ移動
                    /* 必須チェック */
                    ->type('user_id',  $update->user_id_require)   // 入力:ユーザーID
                    ->type('password', $update->password_require)  // 入力:パスワード
                    ->press('Login')                               // 押下:ログインボタン
                    ->assertPathIs('/login')                       // 確認:ログイン画面であること
                    ->assertSee('ユーザーIDは必須です。')           // 確認:メッセージ
                    ->assertSee('パスワードは必須です。')           // 確認:メッセージ

                    /* ID・パスワードの不一致チェック */
                    ->type('user_id',  $user->user_id)            // ユーザーIDを入力
                    ->type('password', 'misspass')                // ミスパスワードを入力
                    ->press('Login')                              // 送信ボタンをクリック
                    ->assertPathIs('/login')
                    ->assertSee('認証に失敗しました。ログインできません。');
        });
    }
    
    /**
     * バリデーションの動作を確認する (createアクション)
     *
     * @return void
     */
    public function testValidationCreate()
    {
        // ユーザーを新規に作成・保存
        $admin = factory(User::class)->create([]);
        
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->make([
            'user_id'               => 'hoge',
            'user_name'             => 'ほげほげ',
            'password'              => 'hogehoge',
            'password-confirm'      => 'hogehoge',
            'user_id_require'                => '',
            'user_name_require'              => '',
            'password_require'               => '',
            'password_confirmation_require'  => '',
            'password_confirmation_mismatch' => 'mogemoge',
            'password_min_6'                 => '12345',
        ]);

        $this->browse(function (Browser $browser) use ($admin, $user) {
        $browser->loginAs($admin)                                                                    // ログインする
                ->pause(2000)
                ->visit('/register')
                ->pause(2000)
                /* 必須チェック */
                ->type('user_id',                $user->user_id_require)                             // 入力:ユーザーID
                ->type('user_name',              $user->user_name_require)                           // 入力:ユーザー名
                ->type('password',               $user->password_require)                            // 入力:パスワード
                ->type('password_confirmation',  $user->password_confirmation_require)               // 入力:パスワード確認
                ->press('insert')                                                                    // 押下:「新規登録」ボタン
                ->assertPathIs('/register')                                                          // 確認:見積一覧であること
                ->assertSee('ユーザーIDは必須です。')                                                 // 確認:メッセージ
                ->assertSee('ユーザー名は必須です。')                                                 // 確認:メッセージ
                ->assertSee('パスワードは必須です。')                                                 // 確認:メッセージ
                ->assertInputValue('user_id',    $user->user_id_require)                            // 入力後:ユーザーID
                ->assertInputValue('user_name',  $user->user_name_require)                          // 入力後:ユーザー名
                ->assertInputValue('password',   $user->password_require)                           // 入力後:パスワード
                ->assertInputValue('password_confirmation',   $user->password_confirmation_require) // 入力後:パスワード確認

                /* パスワード桁数チェック */
                ->type('user_id',                $user->user_id)                                    // 入力:ユーザーID
                ->type('user_name',              $user->user_name)                                  // 入力:ユーザー名
                ->type('password',               $user->password_min_6)                             // 入力:パスワード
                ->type('password_confirmation',  $user->password_min_6)                             // 入力:ミスマッチパスワード
                ->press('insert')                                                                   // 押下:「新規登録」ボタン
                ->assertPathIs('/register')                                                         // 確認:見積一覧であること
                ->assertSee('パスワードは6文字以上にしてください。')
            
                /* ID・パスワードの不一致チェック */
                ->type('user_id',                $user->user_id)                                    // 入力:ユーザーID
                ->type('user_name',              $user->user_name)                                  // 入力:ユーザー名
                ->type('password',               $user->password)                                   // 入力:パスワード
                ->type('password_confirmation',  $user->password_confirmation_mismatch)             // 入力:ミスマッチパスワード
                ->press('insert')                                                                   // 押下:「新規登録」ボタン
                ->assertPathIs('/register')                                                         // 確認:見積一覧であること
                ->assertSee('パスワードは確認用項目と一致していません。')
                ->assertInputValue('user_id',    $user->user_id)                                    // 入力後:ユーザーID
                ->assertInputValue('user_name',  $user->user_name)                                  // 入力後:ユーザー名
                ->assertInputValue('password',   $user->password_require)                           // 入力後:パスワード
                ->assertInputValue('password',   $user->password_require);                          // 入力後:パスワード確認

        });
    }    
    
    /**
     * バリデーションの動作を確認する (editアクション)
     *
     * @return void
     */
    public function testValidationEdit()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create([
            'user_id'               => 'hoge',
            'user_name'             => 'ほげほげ',
        ]);
        
        // 編集する内容
        $update = factory(User::class)->make([
            'user_id'               => 'hoge',
            'user_name'             => 'ほげ太郎',
            'password'              => 'mogemoge',
            'password_confirmation' => 'mogemoge',
            'user_id_require'                => '',
            'user_name_require'              => '',
            'password_require'               => '',
            'password_confirmation_require'  => '',
            'password_confirmation_mismatch' => 'mogemogemogemoge',
            'password_min_6'                 => '12345',
        ]);

        $this->browse(function (Browser $browser) use ($user, $update) {
        $browser->loginAs($user)                                     // ログインする
                ->visit('/users/edit')
                /* --------------------------------
                 * 必須チェック
                 *--------------------------------- */
                ->type('user_name',                         $update->user_id_require)                     // 入力:得意先名
                ->type('password',                          $update->password_require)                    // 入力:パスワード
                ->type('password_confirmation',             $update->password_confirmation_require)       // 入力:パスワード確認
                ->press('confirm')                                                                        // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                                           // 確認:確認ページであること
                ->assertSee('ユーザー名は必須です。')                                                       // 確認:メッセージ
                ->assertInputValue('user_name',             $update->user_name_require)                   // 入力後:ユーザー名
                ->assertInputValue('password',              $update->password_require)                    // 入力後:パスワード
                ->assertInputValue('password_confirmation', $update->password_confirmation_require)       // 入力後:パスワード確認
                /* --------------------------------
                 * パスワード桁数チェック
                 *--------------------------------- */
                ->type('user_name',              $update->user_name)                                      // 入力:ユーザー名
                ->type('password',               $update->password_min_6)                                 // 入力:パスワード
                ->type('password_confirmation',  $update->password_min_6)                                 // 入力:ミスマッチパスワード
                ->press('confirm')                                                                        // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                                           // 確認:確認ページであること
                ->assertSee('パスワードは6文字以上にしてください。')                                         // 確認:メッセージ
                ->assertSee('パスワード(確)は6文字以上にしてください。')                                     // 確認:メッセージ
                /* --------------------------------
                 * ID・パスワードの不一致チェック
                 *--------------------------------- */
                ->type('password',               $update->password)                                       // 入力:パスワード
                ->type('password_confirmation',  $update->password_confirmation_mismatch)                 // 入力:ミスマッチパスワード
                ->press('confirm')                                                                        // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                                           // 確認:確認ページであること
                ->assertSee('パスワードは確認用項目と一致していません。');                                   // 確認:メッセージ
        });
    } 
    
    
}