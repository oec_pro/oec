@extends('layouts.app')

@section('content')


<script type="text/javascript">
    $(function()
    {
        //初期表示行(ヘッダ行込み)
        var tatalRow = 8;
        
        //初期表示6行作成
        var rows = tbl.rows;
        if (rows.length < tatalRow) {
            var startRows = tatalRow - rows.length;
            for(var i = 0; i < startRows; i++){
                addRow(); //空行作成
            }
        }else{
            addRow(); //空行作成
        }
        
        $("table").tableDnD({
            dragHandle: ".handle img",
            onDrop: function(){
                autoNumber();
            }
        });
        clc();/* 小計、合計の算出 */
    });
  
    /* 状態 */
    $(document).on("click", "#salesCond", function(){
        var $sales_cond = $('input[name=s2]:checked').val();
        $('.status').find('input[type=hidden]').val($sales_cond);
    });  
    
    /* 複写(新規) */
    $(document).on("click", "#estimate_copy", function()
    {
        $mode = $('.mode').find('input[type=hidden]').val();
        
        if ( $mode == 0) {
            $('.mode').find('input[type=hidden]').val('1');
            $(this).attr('class', 'btn btn-info btn-sm');
            $('#title_mode').text('複写(新規登録)モード');
        }else{
            $('.mode').find('input[type=hidden]').val('0');
            $(this).attr('class', 'btn btn-success btn-sm');
            $('#title_mode').text('');
        }
    });
    
    /* 明細行追加 */
    $(document).on("click", "#row_add", function(){
        addRow();
    });  
        
    /* 明細行複写 */
    $(document).on("click", "#row_copy", function()
    {
        $("#row_delete").attr('disabled', false);
        $rows = $(this).parent().parent();
        $copy_line = $rows.clone(true);
        $copy_line.find('input[type=hidden]').val('');
        $copy_line.find('.help-block').empty();
        $copy_line.find('.has-error').attr('class', '');
        $copy_line.appendTo('#tbl');
        autoNumber();
        clc();/* 小計、合計の算出 */
    });

    /* 明細行削除 */
    $(document).on("click", "#row_delete", function()
    {
        $row = $(this).parent().parent();
        swal({
            title: "削除確認",
            text: "削除してもよろしいですか？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "削除",
            cancelButtonText: "キャンセル",
            closeOnConfirm: true
            },
            function(isConfirm){
                if (isConfirm) {
                    $row_num = $row.find('#id').text();
                    tbl.deleteRow(parseInt($row_num)+1);
                    autoNumber();
                    clc();/* 小計、合計の算出 */
                }
                var rows = tbl.rows;
                if (rows.length == 3) {
                    $("#row_delete").attr('disabled', true);
                }
                
                return false;
            }
        );
    });
    
    /* 明細行金額算出 */
    $(document).on('change', "input[name^=quantity], input[name^=cost_price], input[name^=sal_price], select[name^=tax_flg]", function()
    {
        $validate_flg = false;
        
        $rows = $(this).parent().parent();
        //数量
        $quantity = $rows.find('input[name^=quantity]').val().replace(",", "");
        if ($quantity =="") {
            $quantity = 0;
        }
        //売単価
        $sal_price = $rows.find('input[name^=sal_price]').val().replace(",", "");
        if ($sal_price =="") {
            $sal_price = 0;
        }
        //原単価
        $cost_price = $rows.find('input[name^=cost_price]').val().replace(",", "");
        if ($cost_price =="") {
            $cost_price = 0;
        }
        
        //売上金額算出
        if ( $quantity == "" || $sal_price == "") {
            $rows.find('input[name^=sal_amount]').val('');
        }else{
            if (!isNaN($quantity) && !isNaN($sal_price)) {
                //売上金額
                $sal_amount = $quantity * $sal_price;
                $rows.find('input[name^=sal_amount]').val($sal_amount.toLocaleString( undefined, { maximumFractionDigits: 20 }) );
            }else{
                $rows.find('input[name^=sal_amount]').val('');
                $validate_flg = true;
            }
        }
        //原価金額算出
        if ( $quantity == "" || $cost_price == "") {
            $rows.find('input[name^=cost_amount]').val('');
        }else{
            if (!isNaN($quantity) && !isNaN($cost_price)) {
                //原価金額
                $cost_amount = $quantity * $cost_price;
                $rows.find('input[name^=cost_amount]').val($cost_amount.toLocaleString( undefined, { maximumFractionDigits: 20 }) );
            }else{
                $rows.find('input[name^=cost_amount]').val('');
                $validate_flg = true;
            }
        }
        
        if ($validate_flg) {
            return;
        }
        
        //小計、合計の算出
        clc();
       
    });

    /* 税率の変更 */
    $(document).on('change', "select[name=tax_rate], select[name=round_type]", function()
    {
        //小計、合計の算出
        clc();
    });

    /* 小計、合計の算出 */
    function clc()
    {
        var rows = tbl.rows;
        var a_tax_out=0;
        var b_tax_out=0;
        var c_tax_out=0;
        var a_tax_in=0;
        var b_tax_in=0;
        var c_tax_in=0;
        var a_tax=0;
        var b_tax=0;
        var c_tax=0;
        var total_cost_amount=0;
        //税率
        var tax = $('[name=tax_rate] option:selected').text();        
        //丸め
//        var round_type = "{{ Config::get('const.round_type') }}";
        //丸め
        var round_type = $('select[name=round_type]').val();
        
        //ヘッダー行考慮のため"i"は2。
        for(var i = 2; i < rows.length; i++){
            //課税区分の取得
            var tax_flg = rows[i].cells[8].childNodes[0];
            //売上金額の取得
            var clc  = rows[i].cells[7].childNodes[0];
            //原価金額の取得
            var cost_amount  = rows[i].cells[7].childNodes[4];
            
            switch (tax_flg.value) {
//                case "0"://未選択
                case "1"://小計 税抜
                    a_tax_out += ( !isNaN(parseFloat(clc.value.replace(/,/g, ''))) )? parseFloat(clc.value.replace(/,/g, '')) : 0;
                    //原価合計
                    total_cost_amount += ( !isNaN(parseFloat(cost_amount.value.replace(/,/g, ''))) )? parseFloat(cost_amount.value.replace(/,/g, '')) : 0;
                    break;
                case "2"://小計 税込
                    b_tax_in  += ( !isNaN(parseFloat(clc.value.replace(/,/g, ''))) )? parseFloat(clc.value.replace(/,/g, '')) : 0;
                    //原価合計
                    total_cost_amount += ( !isNaN(parseFloat(cost_amount.value.replace(/,/g, ''))) )? parseFloat(cost_amount.value.replace(/,/g, '')) : 0;
                    break;
                case "3"://小計 非課税
                    c_tax_in += ( !isNaN(parseFloat(clc.value.replace(/,/g, ''))) )? parseFloat(clc.value.replace(/,/g, '')) : 0;
                    //原価合計
                    total_cost_amount += ( !isNaN(parseFloat(cost_amount.value.replace(/,/g, ''))) )? parseFloat(cost_amount.value.replace(/,/g, '')) : 0;
                    break;
            }
            if ( isNaN(total_cost_amount) ) {
                total_cost_amount = 0;
            }
        }
        
        switch (round_type) {
            case "1"://四捨五入
                a_tax = Math.round( a_tax_out * (tax / 100));
                b_tax = Math.round( b_tax_in * tax / ( 100 + parseInt(tax) ));
                break;
            case "2"://切り上げ
                a_tax = Math.ceil( a_tax_out * (tax / 100));
                b_tax = Math.ceil( b_tax_in * tax / ( 100 + parseInt(tax) ));
                break;
            case "3"://切り捨て
                a_tax = Math.floor( a_tax_out * (tax / 100));
                b_tax = Math.floor( b_tax_in * tax / ( 100 + parseInt(tax) ));
                break;
        }

        //課税区分 税抜
        a_tax_in = a_tax_out + a_tax;
        if ( isNaN(a_tax_out) ) {
            a_tax = 0;
            a_tax_out = 0;
            a_tax_in = 0;
        }
        $("input[name=a_tax]").val(a_tax);
        $("input[name=a_tax_out]").val(a_tax_out);
        $("input[name=a_tax_in]").val(a_tax_in);
        
        //課税区分 税込
        b_tax_out = b_tax_in - b_tax;
        if ( isNaN(b_tax_in) ) {
            b_tax = 0;
            b_tax_out = 0;
            b_tax_in = 0;
        }
        $("input[name=b_tax]").val(b_tax);
        $("input[name=b_tax_out]").val(b_tax_out);
        $("input[name=b_tax_in]").val(b_tax_in);
        
        //課税区分 非課税
        c_tax_out = c_tax_in;
        if ( isNaN(c_tax_in) ) {
            c_tax_out = 0;
            c_tax_in = 0;
        }
        $("input[name=c_tax]").val(0);
        $("input[name=c_tax_out]").val(c_tax_out);
        $("input[name=c_tax_in]").val(c_tax_in);
        
        //税抜合計
        $("input[name=total_amount_taxnon]").val( parseFloat(a_tax_out + b_tax_out + c_tax_out).toLocaleString( undefined, { maximumFractionDigits: 20 })  );
        //消費税
        $("input[name=tax_amount]").val( parseFloat(a_tax + b_tax + c_tax).toLocaleString( undefined, { maximumFractionDigits: 20 })  );
        //税込合計
        $("input[name=total_amount_taxin]").val( parseFloat(a_tax_in + b_tax_in + c_tax_in).toLocaleString( undefined, { maximumFractionDigits: 20 })  );
        //原価合計
        $("input[name=total_cost_amount]").val( parseFloat(total_cost_amount).toLocaleString( undefined, { maximumFractionDigits: 20 })  );

        //粗利額   ( 税抜合計-原価合計 )
        //粗利率 ( ( 税抜合計-原価合計 ) / 税抜合計 ) * 100
        if ( parseFloat(a_tax_out + b_tax_out + c_tax_out) > 0 ) {
            var gross_profit = ( ( parseFloat(a_tax_out + b_tax_out + c_tax_out) - total_cost_amount ) / parseFloat(a_tax_out + b_tax_out + c_tax_out) ) * 100
            var gross_pro_gk =   ( parseFloat(a_tax_out + b_tax_out + c_tax_out) - total_cost_amount ).toLocaleString( undefined, { maximumFractionDigits: 20 })
        }else{
            var gross_profit = 0;
            var gross_pro_gk = 0;
        }
        $("input[name=gross_profit]").val( Math.round(gross_profit * 10) / 10 );
        $("input[name=gross_pro_gk]").val( gross_pro_gk );

    }

    /* 明細行追加 */
    function addRow()
    {
        $("#row_delete").attr('disabled', false);
        $lastTr = $('#tbl tr:last').clone( true );
        $lastTr.find('input[type=text]').val('');
        $lastTr.find('input[type=hidden]').val('');
        $lastTr.find('select').val(1);
        $lastTr.find('textarea').val('');
        $lastTr.find('.help-block').empty();
        $lastTr.find('.has-error').attr('class', '');
        $('#tbl').append($lastTr);
        autoNumber();
    }

    /* 行番号の再採番 */
    function autoNumber()
    {
        var rows = tbl.rows;
        //ヘッダー行考慮のため"i"は2。
        for(var i = 2; i < rows.length; i++){
            //No.ラベル
            rows[i].cells[1].firstChild.nodeValue = (parseInt(i)-1);
            //No.HIDDIN
            var objInput = rows[i].cells[1].childNodes[1];
            objInput.value = (parseInt(i)-1); 

            //TR.id連番の振り直し(tableDnDでは行番号が必須のため)
            rows[i].setAttribute("id", "id"+(parseInt(i)-1)); 

            //入力項目のname連番の振り直し
            rows[i].cells[0].childNodes[0].setAttribute("name", "id["+(parseInt(i)-2)+"]");
            rows[i].cells[0].childNodes[2].setAttribute("id",   "test_img"+(parseInt(i)-2));
            rows[i].cells[1].childNodes[1].setAttribute("name", "no["+(parseInt(i)-2)+"]");
            rows[i].cells[2].childNodes[0].setAttribute("name", "item_id["+(parseInt(i)-2)+"]");
            rows[i].cells[3].childNodes[0].setAttribute("name", "item_name["+(parseInt(i)-2)+"]");
            rows[i].cells[3].childNodes[4].setAttribute("name", "item_description["+(parseInt(i)-2)+"]");
            rows[i].cells[4].childNodes[0].setAttribute("name", "quantity["+(parseInt(i)-2)+"]");
            rows[i].cells[5].childNodes[0].setAttribute("name", "unit["+(parseInt(i)-2)+"]");
            rows[i].cells[6].childNodes[0].setAttribute("name", "sal_price["+(parseInt(i)-2)+"]");
            rows[i].cells[6].childNodes[4].setAttribute("name", "cost_price["+(parseInt(i)-2)+"]");
            rows[i].cells[7].childNodes[0].setAttribute("name", "sal_amount["+(parseInt(i)-2)+"]");
            rows[i].cells[7].childNodes[4].setAttribute("name", "cost_amount["+(parseInt(i)-2)+"]");
            rows[i].cells[8].childNodes[0].setAttribute("name", "tax_flg["+(parseInt(i)-2)+"]");
            rows[i].cells[9].childNodes[0].setAttribute("name", "item_remarks["+(parseInt(i)-2)+"]");
        }
    }

    /* 得意先取得 */
    function getCus()
    {
        id = $sales_cond = $('#select_switch').find('input[type=text]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url : "{{ url('customers/selectcustomers') }}",
            type: 'post',
            dataType: 'json',
            data: { id: id },
            success : function(json) {
//                $( '#customer_name' ).val( json['cud_nm'] );
                $( '#select_switch' ).find( 'input' ).val( json['cud_id'] );
                $( '#select_switch_b' ).find( 'input' ).val( json['cud_nm'] );
//                $( '#lbl_customer_name' ).text( json['cud_nm'] );
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert("エラーが発生しました：" + textStatus + ":\n" + errorThrown);
            }
        });
    };
    
</script>
<style>
.tDnD_whileDrag {
    background-color: #E8F1FB;
    }

    .handle img{
    cursor: move;
}
</style>

<style type="text/css">
<!--
.input_detail {
color: #ffff00;
}
-->
</style>

<!-- container -->
<div class="navi_sub">
    
    <!-- navi_sub_left -->
    <div class="navi_sub_left">
        <a class="btn btn-success btn-sm" href="{{ action('EstimatesController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
            <i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
        </a>
        <a id="list" class="btn btn-success btn-sm" href="{{ action('EstimatesController@index') }}" role="button" data-toggle="tooltip" title="見積書一覧を表示">
            <i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
        </a>
        @if ( is_numeric($data['info']['seq_id']) )
            <div class="btn-group">
                <button id="xls_out" type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-file-text" aria-hidden="true"></i><span class="navi_sub_marginleft">見積書出力</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(1, '御見積書出力', {{ $data['info']['seq_id'] }});return false;">御見積書出力</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(2, '請書出力', {{ $data['info']['seq_id'] }});return false;">請書出力</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(3, '納品書出力', {{ $data['info']['seq_id'] }});return false;">納品書出力</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="confirmExl(4, '請求書出力', {{ $data['info']['seq_id'] }});return false;">請求書出力</a></li>
                </ul>
            </div>        
            <button id="csv_out" type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="見積明細書CSV出力" onclick="confirmCsv({{ $data['info']['seq_id'] }});return false;">
                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i><span class="navi_sub_marginleft">CSV出力</span>
            </button>
            <button  id="estimate_copy" type="button" class="btn btn-success btn-sm @if($data['mode']==0) btn-success btn-sm @else btn-info btn-sm @endif" data-toggle="tooltip" title="この見積書を複写" >
                <i class="fa fa-files-o" aria-hidden="true"></i><span class="navi_sub_marginleft">複写(新規)</span>
            </button>
        @endif
    </div><!-- navi_sub_left -->
    
    <div class="navi_sub_right">
        <button type="button" class="btn btn-primary btn-sm regist_del_btn" data-toggle="tooltip" title="この見積書を登録" onclick="confirmEdit({{ $data['info']['seq_id'] }});return false;" id="regist">
            <span class="">登録</span>
        </button>
        <button type="button" class="btn btn-danger btn-sm regist_del_btn" data-toggle="tooltip" title="この見積書を削除" onclick="confirmDel({{ $data['info']['seq_id'] }});return false; " id="delete" @if ( !is_numeric($data['info']['seq_id']) ) disabled @endif>
            <span class="">削除</span>
        </button>
    </div>
    <div class="clear"></div>
</div><!-- container -->

<!-- container -->
<div class="container_page">
    
    <!-- title_box -->
    <div class="title_box">
        <div class="title_left">
            @if ( is_numeric($data['info']['seq_id']) )
                <h3><b><?php echo "見積書変更"; ?></b></h3>
            @else
                <h3><b><?php echo "見積書登録"; ?></b></h3>
            @endif
            <p id="title_mode" style="color:#FF0000;font-size:20px"></p>
        </div>
        <div class="title_right">
            <div id="salesCond" class="sample"> 
                <input type="radio" name="s2" id="select1" value="1" <?php if ( $data['info']['sales_cond']==1 ) echo "checked"; ?>> 
                <label for="select1" class="switch-estimate">見積</label> 
                <input type="radio" name="s2" id="select2" value="2" <?php if ( $data['info']['sales_cond']==2 ) echo "checked"; ?>> 
                <label for="select2" class="switch-orders">受注</label> 
                <input type="radio" name="s2" id="select3" value="3" <?php if ( $data['info']['sales_cond']==3 ) echo "checked"; ?>> 
                <label for="select3" class="switch-delivery">納品</label> 
                <input type="radio" name="s2" id="select4" value="4" <?php if ( $data['info']['sales_cond']==4 ) echo "checked"; ?>> 
                <label for="select4" class="switch-claim">請求</label> 
            </div> 
        </div>
        <div class="clear"></div>
        <hr>
    </div>
    <!-- / title_box -->
        
    <!-- panel-body -->
    <div class="panel-body">

        <div class="@if(Session::has('edit_success')) alert alert-success @endif">{{ Session::get('edit_success') }}</div>
        
        @if (count($data['results']) >= 1)
            {!! Form::open(['name' => 'form', 'url' => 'estimates/save', 'class' => 'form-horizontal form-label-left']) !!}

                <!-- panel-group -->
                <div class="panel-group" id="" role="tablist" aria-multiselectable="true">
                    <!-- panel panel-default -->
                    <div class="panel panel-default">

                        <!-- panel-heading -->
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h5 class="panel-title">
                                <a id="test_tgl" role="button" data-toggle="collapse" data-parent="" href="#collapseOne_three" aria-expanded="true" aria-controls="collapseOne">
                                    <b>見積伝票情報</b>
                                    <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                                </a>
                            </h5>
                        </div>
                        <!-- / panel-heading -->

                        <!-- collapseOne_three -->
                        <div id="collapseOne_three" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <!-- panel-body-->
                            <div class="panel-body">
                                <div>
                                    {!! Form::hidden('seq_id', $data['info']['seq_id']) !!}
                                    {!! Form::hidden('created_at', $data['info']['created_at']) !!}
                                    {!! Form::hidden('updated_at', $data['info']['updated_at']) !!}
                                    {!! Form::hidden('created_name', $data['info']['created_name']) !!}
                                    {!! Form::hidden('updated_name', $data['info']['updated_name']) !!}
                                </div>
                                <div class="status">
                                    {!! Form::hidden('sales_cond', $data['info']['sales_cond']) !!}
                                </div>
                                <div class="mode">
                                    {!! Form::hidden('mode_copy', $data['mode']) !!}
                                </div>

                                <!-- estimates_egistration  -->
                                <div class="estimates_egistration">

                                    <!-- estimates_egistration_box1  -->
                                    <div class="estimates_egistration_box1">
                                        <table id="table jambo_table"  class="table table-bordered">
                                            <tbody>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width" rowspan="2"><div class="col_title_right_req">得意先名<span class="required_color">※</span></div></th>
                                                    <td class="form-group {{ !empty($errors->first('customer_name')) ? ' has-error' :'' }}">
                                                        <div id="select_switch" class='input-group col_contents_group'>
                                                            {!! Form::text('customer_id', $data['info']['customer_id'], ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'onblur'=>'getCus()']) !!}
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default input-sm" type="button" data-target="#modal_switch" data-toggle="modal" title="商品検索"><span class="fa fa-search"></span></button>
                                                            </span>
                                                        </div>
                                                        <span class="help-block">{{$errors->first('customer_name')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <td class="form-group">
                                                        <div class="">
                                                            <div id="select_switch_b" class=''>
                                                                {!! Form::text('customer_name', $data['info']['customer_name'], ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly',  'tabindex' => '-1']) !!}
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width"><div class="col_title_right_req">件名（名称）<span class="required_color">※</span></div></th>
                                                    <td class="form-group {{ !empty($errors->first('title')) ? ' has-error' :'' }}">
                                                        {!! Form::text('title', $data['info']['title'], ['style' => 'color:#000','class' => 'form-control ja input-sm', 'placeholder' => '', 'maxlength' => '60']) !!}
                                                        <span class="help-block">{{$errors->first('title')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width">納品場所</th>
                                                    <td class="form-group {{ !empty($errors->first('delivery_location')) ? ' has-error' :'' }}">
                                                        {!! Form::text('delivery_location', !empty( $data['info']['delivery_location'] ) ? $data['info']['delivery_location']: 'ご指定場所', ['style' => 'color:#000','class' => 'form-control ja input-sm', 'placeholder' => '', 'maxlength' => '60']) !!}
                                                        <span class="help-block">{{$errors->first('delivery_location')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width">有効期限</th>
                                                    <td class="form-group {{ !empty($errors->first('expiration_date')) ? ' has-error' :'' }}">
                                                        {!! Form::text('expiration_date', !empty( $data['info']['expiration_date'] ) ? $data['info']['expiration_date']: '御見積り提出後、', ['style' => 'color:#000','class' => 'form-control ja input-sm', 'placeholder' => '', 'maxlength' => '20']) !!}
                                                        <span class="help-block">{{$errors->first('expiration_date')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width">受渡期日</th>
                                                    <td class="form-group {{ !empty($errors->first('delivery_date')) ? ' has-error' :'' }}">
                                                        {!! Form::text('delivery_date', $data['info']['delivery_date'], ['style' => 'color:#000','class' => 'form-control ja input-sm', 'placeholder' => '', 'maxlength' => '20']) !!}
                                                        <span class="help-block">{{$errors->first('delivery_date')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width">支払条件</th>
                                                    <td class="form-group {{ !empty($errors->first('pay_conditions')) ? ' has-error' :'' }}">
                                                        {!! Form::text('pay_conditions', $data['info']['pay_conditions'], ['style' => 'color:#000','class' => 'form-control ja input-sm', 'placeholder' => '', 'maxlength' => '20']) !!}
                                                        <span class="help-block">{{$errors->first('pay_conditions')}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="registration_height">
                                                    <th class="contents_titile_width">備考</th>
                                                    <td class="form-group {{ !empty($errors->first('remarks')) ? ' has-error' :'' }}">
                                                        {{ Form::textarea('remarks', $data['info']['remarks'], ['style' => 'color:#000','class' => 'form-control ja input-sm note_style', 'rows' => '3','cols' => '60']) }}
                                                        <span class="help-block">{{$errors->first('remarks')}}</span>
                                                    </td>
                                                </tr>
                                            <tbody>
                                        </table>
                                    </div>　
                                    <!-- / estimates_egistration_box1  -->

                                    <!-- estimates_egistration_box2  -->
                                    <div class="estimates_egistration_box2">
                                        <table id="info3" class="table table-striped table-bordered jambo_table bulk_action">
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">見積日<span class="required_color">※</span></div></th>
                                                <td class="form-group {{ !empty($errors->first('estimate_date')) ? ' has-error' :'' }}">
                                                    <div class="input-group date">
                                                        {!! Form::text('estimate_date', ( isset($data['info']['estimate_date']) && $data['info']['estimate_date'] != 0)? date('Y/m/d', strtotime($data['info']['estimate_date'])): date('Y/m/d'), array('style' => 'color:#000','class' => 'form-control en picker input-sm','maxlength' => '8', 'readonly' => 'readonly', 'placeholder' => '例）2017/01/01', 'tabindex' => '-1')); !!}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default input-sm" type="button"><span class="fa fa-calendar"></span></button>
                                                        </span>
                                                    </div>
                                                    <span class="help-block">{{$errors->first('estimate_date')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width">見積No</th>
                                                <td class="form-group {{ !empty($errors->first('esti_no')) ? ' has-error' :'' }}">
                                                    {{ $data['info']['esti_no'] }}
                                                    {!! Form::hidden('esti_no', $data['info']['esti_no']) !!}
                                                    <span class="help-block">{{$errors->first('esti_no')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">担当者名<span class="required_color">※</span></div></th>
                                                <td class="form-group {{ !empty($errors->first('staff_id')) ? ' has-error' :'' }}">
                                                    {!! Form::select('staff_id', $data['user_list'], Auth::user()->id, ['style' => 'color:#000','class' => 'form-control input-sm']) !!}
                                                    <span class="help-block">{{$errors->first('staff_id')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">税抜合計<span class="required_color">※</span></div></th>
                                                <td class="form-group {{ !empty($errors->first('total_amount_taxnon')) ? ' has-error' :'' }}">
                                                    {!! Form::text('total_amount_taxnon', number_format($data['info']['total_amount_taxnon']), ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                                    <span class="help-block">{{$errors->first('total_amount_taxnon')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width" rowspan="2"><div class="col_title_right_req">税額<span class="required_color">※</span></div></th>
                                                <td class="form-group {{ !empty($errors->first('total_amount_taxnon')) ? ' has-error' :'' }}">
                                                    {!! Form::text('tax_amount', number_format($data['info']['tax_amount']), ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                                    <span class="help-block">{{$errors->first('tax_amount')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <td class="form-group {{ !empty($errors->first('taxrates')) ? ' has-error' :'' }} ">
                                                    <?php 
                                                        $round_type_list = Config::get('const.round_type_list');
                                                    ?>
                                                    <div style="padding-left:0;height:38px;" class="col-md-5">{!! Form::select('tax_rate', $data['taxrates'], $data['info']['tax_rate'], ['style' => 'color:#000','class' => 'form-control input-sm']) !!}</div>
                                                    <div style="padding-left:0;height:38px;" class="col-md-7">{!! Form::select('round_type', $round_type_list, $data['info']['round_type'], ['style' => 'color:#000','class' => 'form-control input-sm']) !!}</div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">税込合計<span class="required_color">※</span></div></th>
                                                <td class="form-group {{ !empty($errors->first('total_amount_taxin')) ? ' has-error' :'' }}">
                                                    {!! Form::text('total_amount_taxin', number_format($data['info']['total_amount_taxin']), ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                                    <span class="help-block">{{$errors->first('total_amount_taxin')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">原価合計</div></th>
                                                <td class="form-group {{ !empty($errors->first('total_cost_amount')) ? ' has-error' :'' }}">
                                                    {!! Form::text('total_cost_amount', number_format($data['info']['total_cost_amount']), ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                                    <span class="help-block">{{$errors->first('total_cost_amount')}}</span>
                                                </td>
                                            </tr>
                                            <tr class="registration_height">
                                                <th class="contents_titile_width"><div class="col_title_right_req">粗利</div></th>
                                                <td class="form-group">
                                                    <div style="padding-left:0;height:38px;" class="col-md-5">{!! Form::text('gross_profit', 0, ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}</div>
                                                    <div style="padding-left:0;height:38px;line-height: 30px;vertical-align: middle;" class="col-md-1">%</div>
                                                    <div style="padding-left:0;height:38px;" class="col-md-5">{!! Form::text('gross_pro_gk', 0, ['style' => 'color:#000','class' => 'form-control en input-sm', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}</div>
                                                    <div style="padding-left:0;height:38px;line-height: 30px;vertical-align: middle;" class="col-md-1">円</div>
                                                    <span class="help-block"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>　
                                    <!-- / estimates_egistration_box2  -->
                                </div>
                                <!-- / estimates_egistration  -->
                            </div>
                            <!-- / panel-body-->
                        </div>
                        <!-- / collapseOne_three -->

                    </div>
                    <!-- / panel panel-default -->
                    <br>
                    <!-- order_detail_info_wrap -->
                    <div class="order_detail_info_wrap">
                        <div class="{{ !empty($errors->first('no_rows')) ? 'alert alert-danger' : '' }}">{{ $errors->first('no_rows') }}</div>

                        <!-- display: none -->
                        <div style="display: none;">
                            <table id="clcc" class="table table-striped table-bordered jambo_table bulk_action">
                                <tr class="">
                                    <th class="column-title"  colspan="4">ccccc</th>
                                </tr>
                                <tr class="">
                                    <td>税額D</td>
                                    <td>{!! Form::text('a_tax',  0, ['name' => 'a_tax', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('b_tax',  0, ['name' => 'b_tax', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('c_tax',  0, ['name' => 'c_tax', 'class' => 'form-control en']) !!}</td>
                                <tr class="">
                                <tr class="">
                                    <td>税抜額E</td>
                                    <td>{!! Form::text('a_tax_out',  0, ['name' => 'a_tax_out', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('b_tax_out',  0, ['name' => 'b_tax_out', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('c_tax_out',  0, ['name' => 'c_tax_out', 'class' => 'form-control en']) !!}</td>
                                <tr class="">
                                <tr class="">
                                    <td>税込額G</td>
                                    <td>{!! Form::text('a_tax_in',  0, ['name' => 'a_tax_in', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('b_tax_in',  0, ['name' => 'b_tax_in', 'class' => 'form-control en']) !!}</td>
                                    <td>{!! Form::text('c_tax_in',  0, ['name' => 'c_tax_in', 'class' => 'form-control en']) !!}</td>
                                <tr class="">
                            </table>
                        </div>
                        <!-- / display: none -->

                        <!-- main -->
                        <table summary="受注商品明細リスト" id="tbl" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame">
                            <thead>
                                <tr class="headings nodrop nodrag">
                                    <th class="order_detail_title1" rowspan="2" colspan="2"><div class="order_detail_height_style">No</div></th>
                                    <th class="order_detail_title2" rowspan="2"><div class="order_detail_height_style">商品No</div></th>
                                    <th class="order_detail_title3">商品名<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title4" rowspan="2"><div class="order_detail_height_style">数量<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title5" rowspan="2"><div class="order_detail_height_style">単位<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title6">売単価<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title7">売上金額<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title8" rowspan="2"><div class="order_detail_height_style">課税区分<span class="required_color">※</span></div></th>
                                    <th class="order_detail_title9" rowspan="2"><div class="order_detail_height_style">備考</div></th>
                                    <th class="order_detail_title10" rowspan="2"><div class="order_detail_height_style">操作</div></th>
                                </tr>
                                <tr class="headings nodrop nodrag">
                                    <th class="order_detail_title11">商品規格</th>
                                    <th class="order_detail_title12">原単価</th>
                                    <th class="order_detail_title13">原価金額</div></th>
                                </tr>
                            </thead>
                            <?php $i=0; ?>
                            @foreach($data['results'] as $result)
                                <tr class="odd pointer" id="id{{$i}}">
                                    <td class="handle" width="20px">
                                        {!! Form::hidden('id['.$i.']', $result['id']) !!}
                                        <img id="{{ 'test_img'.$i }}" width="20" height="20" src="{{ asset('assets/imgs/narabikae.png') }}">
                                    </td>
                                    <td id="id">
                                        {{$i+1}}
                                        {!! Form::hidden('no['.$i.']', $i+1) !!}
                                    </td>
                                    <td class="form-group @if(!empty($errors->first('item_id.'.$i))) has-error @endif">
                                        {!! Form::text('item_id['.$i.']', $result['item_id'], ['class' => 'form-control en input-sm margin_gap', 'placeholder' => '', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                        <span class="help-block">{{$errors->first('item_id.'.$i)}}</span>
                                    </td>
                                    <td class="form-group @if( !empty($errors->first('item_name.'.$i) || $errors->first('item_description.'.$i) )) has-error @endif">
                                        {!! Form::text('item_name['.$i.']', $result['item_name'], ['style' => 'color:#000','class' => 'form-control ja input-sm margin_gap', 'placeholder' => '', 'maxlength' => '60']) !!}
                                        <span class="help-block">{{$errors->first('item_name.'.$i)}}</span>
                                        {!! Form::text('item_description['.$i.']', $result['item_description'], ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap', 'placeholder' => '', 'maxlength' => '80']) !!}
                                        <span class="help-block">{{$errors->first('item_description.'.$i)}}</span>
                                    </td>
                                    <td class="form-group @if(!empty($errors->first('quantity.'.$i))) has-error @endif">
                                        {!! Form::text('quantity['.$i.']', $result['quantity'], ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap input_money', 'placeholder' => '100', 'maxlength' => '8']) !!}
                                        <span class="help-block">{{$errors->first('quantity.'.$i)}}</span>
                                    </td>
                                    <td class="form-group @if(!empty($errors->first('unit.'.$i))) has-error @endif">
                                        {!! Form::text('unit['.$i.']', $result['unit'], ['style' => 'color:#000','class' => 'form-control ja input-sm margin_gap', 'placeholder' => '個', 'maxlength' => '20']) !!}
                                        <span class="help-block">{{$errors->first('unit.'.$i)}}</span>
                                    </td>
                                    <td class="form-group @if(!empty($errors->first('sal_price.'.$i) || $errors->first('cost_price.'.$i) )) has-error @endif">
                                        {!! Form::text('sal_price['.$i.']', ((int)$result['sal_price']==0 && $result['item_name']=="")? "":number_format((int)$result['sal_price']), ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap input_money', 'placeholder' => '1000000', 'maxlength' => '13']) !!}
                                        <span class="help-block">{{$errors->first('sal_price.'.$i)}}</span>
                                        {!! Form::text('cost_price['.$i.']', ((int)$result['cost_price']==0 && $result['item_name']=="")? "":number_format((int)$result['cost_price']), ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap input_money', 'placeholder' => '1000000', 'maxlength' => '13']) !!}
                                        <span class="help-block">{{$errors->first('cost_price.'.$i)}}</span>
                                    </td>
                                    <td class="form-group">
                                        {!! Form::text('sal_amount['.$i.']', ((int)$result['sal_amount']==0 && $result['item_name']=="")? "":number_format((int)$result['sal_amount']), ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap input_money', 'placeholder' => '1000000', 'maxlength' => '13', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                        <span class="help-block">{{$errors->first('sal_amount.'.$i)}}</span>
                                        {!! Form::text('cost_amount['.$i.']',  ((int)$result['cost_amount']==0 && $result['item_name']=="")? "":number_format((int)$result['cost_amount']), ['style' => 'color:#000','class' => 'form-control en input-sm margin_gap input_money', 'placeholder' => '1000000', 'maxlength' => '13', 'readonly' => 'readonly', 'tabindex' => '-1']) !!}
                                        <span class="help-block">{{$errors->first('cost_amount.'.$i)}}</span>
                                    </td>
                                    <td width="120px" class="form-group @if(!empty($errors->first('tax_flg.'.$i))) has-error @endif">
                                        {!! Form::select('tax_flg['.$i.']', Config::get('const.tax_flg'), $result['tax_flg'], ['style' => 'color:#000','class' => 'form-control input-sm margin_gap']) !!}
                                        <span class="help-block">{{$errors->first('tax_flg.'.$i)}}</span>
                                    </td>
                                    <td class="form-group @if(!empty($errors->first('item_remarks.'.$i))) has-error @endif">
                                        {!! Form::textarea('item_remarks['.$i.']', $result['item_remarks'], ['style' => 'color:#000','class' => 'form-control ja input-sm margin_gap', 'rows' => '3','cols' => '30', 'placeholder' => '']) !!}
                                        <span class="help-block">{{$errors->first('item_remarks.'.$i)}}</span>
                                    </td>
                                    <td class=" last" width="140px">
                                        <button type="button" id="row_copy"   type="button" class="btn btn-primary btn-sm margin_gap_btn" >複写</button>
                                        <button type="button" id="row_delete" type="button" class="btn btn-danger btn-sm margin_gap_btn" >削除</button>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach

                        </table>
                        <!-- main -->
                        
                    </div>　
                    <!-- / order_detail_info_wrap -->
                    
                    <!-- 行追加/コメント欄 -->
                    <div class="">
                        <table>
                            <tr>
                                <td class="last">
                                    <button id="row_add" type="button" class="btn btn-info" value="{{$i}}">行追加</button>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <div>コメント</div>
                        <div class="col-xd-12">
                            {{ Form::textarea('coments', $data['info']['coments'], ['class' => 'form-control ja input-sm comment', 'rows' => '3']) }}
                        </div>
                    </div>
                    <!-- / 行追加/コメント欄 -->

                </div>
                <!-- / panel-group -->

				<hr>
                
                <!-- 登録日時 -->
                @if ( is_numeric($data['info']['seq_id']) )
                    <div>
                        <table>
                            <tbody>
                                <tr>
                                    <td><div class="history_day">初期登録日時:</div></td>
                                    <td>
                                        <div class="history_day">{{ date('Y/m/d H:i:s', strtotime($data['info']['created_at'])) }}</div>
                                    </td>
                                    <td>
                                        <div class="history_day">{{ $data['info']['created_name'] }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><div class="history_day">最終更新日時:</div></td>
                                    <td>
                                        <div class="history_day">{{ date('Y/m/d H:i:s', strtotime($data['info']['updated_at'])) }}</div>
                                    </td>
                                    <td>
                                        <div class="history_day">{{ $data['info']['updated_name'] }}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
                <!-- / 登録日時 -->
            
            {!! Form::close() !!}
            
        @elseif(!is_null($data['results']))
            <div class="alert alert-info">該当する情報がありませんでした。<br>検索項目を変更し、再度検索して下さい。</div>
        @endif                    
        
    </div>
    <!-- / panel-body -->

</div>
<!-- / container_page -->

<!-- 顧客検索フォーム -->    
@include('_modal_switch')

<!-- sweetalert -->
<link rel="stylesheet" href="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.min.js"></script>
<script>

    /* 登録 */
    function confirmEdit(id) {
        var form = document.forms.form;
        swal({
            title: "登録確認",
            text: "登録します、よろしいですか？",
            type: "", // default:null "warning","error","success","info"
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"登録", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#31b0d5", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: false // default:true 確認ボタンを押したらアラートが削除される            
        },
        function(isConfirm){
            if (isConfirm) {
                form.submit();
                return true;
            } else {
                return false;
            }
        }); 
    }
    
    /* 削除 */
    function confirmDel(id) {
        swal({
            title: "削除確認",
            text: "削除してもよろしいですか？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "削除",
            cancelButtonText: "キャンセル",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/estimates/" + "close/" + id, '_self');
                return true;
            } else {
                return false;
            }
        }); 
    }

    /* 見積書出力エクセル */
    function confirmExl(out_type, file_nm, id) {
        swal({
            title: file_nm,
            text: "出力してもよろしいですか？",
            type: "", // default:null "warning","error","success","info"
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"出力", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#31b0d5", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: true // default:true 確認ボタンを押したらアラートが削除される            
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/estimates/xlsprint/" + out_type + "/" + id, '_self');
                return true;
            } else {
                return false;
            }
        }); 
    }

    /* 見積明細書出力CSV */
    function confirmCsv(id) {
        swal({
            title: "見積明細書CSV出力",
            text: "出力してもよろしいですか？",
            type: "", // default:null "warning","error","success","info"
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"出力", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#31b0d5", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: true // default:true 確認ボタンを押したらアラートが削除される            
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/estimates/download/detail/" + id, '_self');
                return true;
            } else {
                return false;
            }
        }); 
    }
    
</script>


@endsection

