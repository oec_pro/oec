<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormHogeRequest extends FormRequest
{
    /**
     * 戻り先
     *
     * @var string
     */
    protected $redirect = 'views/hoges/index';
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function attributes()
    {
         return [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'hoge_id.*'        => 'required',
           'hoge_name.*'      => 'required',
           'quantity.*'       => 'numeric',
           'price.*'          => 'numeric',
           'amount.*'         => 'numeric',
        ];
        
    }
    
    public function messages()
    {
        return [
            'hoge_id.required' => '保存するレコードを少なくとも１つ選択してください',
            'hoge_name.required' => '１つ選択してください',
        ];
        
    }    
    
    protected function validationData()
    {
        $data = parent::all();
        
        $cnt = count($this->input('hoge_id'));
        for ( $i=0; $i<$cnt; $i++ ) {
            //一行全て未入力の場合はバリデーションしない
            if ( empty($this->input('hoge_id')[$i]) && empty($this->input('hoge_name')[$i]) && empty($this->input('quantity')[$i]) &&
                empty($this->input('price')[$i]) && empty($this->input('amount')[$i])) {

                unset($data['hoge_id'][$i]);
                unset($data['hoge_name'][$i]);
                unset($data['quantity'][$i]);
                unset($data['price'][$i]);
                unset($data['amount'][$i]);
                
            }
        }

        return $data;
    }    
    
    

    
}
