<?php

use Faker\Generator as Faker;

$factory->define(App\Taxrate::class, function (Faker $faker) {
    return [
        'effective_date' => $faker->date($format='Y-m-d', $max='now'),
        'tax_rate'       => $faker->numberBetween($min = 3, $max = 10),
    ];
});
