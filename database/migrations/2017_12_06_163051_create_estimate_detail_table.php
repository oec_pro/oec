<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimateDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('esti_no');
                $table->string('usr_esti_no', 20)->nullable();
                $table->integer('esti_line_no');
                $table->string('item_id', 20)->nullable();
                $table->string('item_name', 160);
                $table->string('item_description', 160)->nullable();
                $table->integer('quantity');
                $table->string('unit', 20)->nullable();
                $table->decimal('sal_price', 12, 2)->default(0);
                $table->decimal('sal_amount', 12, 2)->default(0);
                $table->decimal('cost_price', 12, 2)->default(0);
                $table->decimal('cost_amount', 12, 2)->default(0);
                $table->string('tax_flg', 1);
                $table->text('item_remarks')->nullable();
                $table->string('created_name', 40);
                $table->string('updated_name', 40);
                $table->timestamps();
                $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_details');
    }
}
