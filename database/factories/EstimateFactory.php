<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(App\Estimate::class, function (Faker $faker) {
    $faker = FakerFactory::create('ja_JP');

    return [
        'customer_id'         => '1',
        'customer_name'       => $faker->company,
        'esti_no'       	  => '1',
        'usr_esti_no'         => '',
        'estimate_date'       => $faker->dateTime,
        'sales_cond'       	  => '1',
        'title'       		  => $faker->title,
        'delivery_location'   => $faker->address,
        'expiration_date'     => $faker->secondaryAddress,
        'delivery_date'       => $faker->secondaryAddress,
        'pay_conditions'      => $faker->secondaryAddress,
        'remarks'       	  => $faker->realText,
        'total_amount_taxnon' => $faker->randomNumber,
        'tax_amount'       	  => $faker->randomNumber,
        'total_amount_taxin'  => $faker->randomNumber,
        'total_cost_amount'   => $faker->randomNumber,
        'tax_type'       	  => '1',
        'tax_rate'       	  => $faker->numberBetween($min = 1, $max = 2),
        'round_type'       	  => $faker->numberBetween($min = 1, $max = 3),
        'coments'       	  => $faker->realText,
        'staff_id'       	  => '1',
        'staff_name'       	  => $faker->name,
        'created_name'        => $faker->name,
        'updated_name'        => $faker->name,
    ];
});
