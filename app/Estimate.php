<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estimate extends Model
{
    use SoftDeletes;
 
    protected $table = 'estimates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'customer_id',
        'customer_name',
        'esti_no',
        'usr_esti_no',
        'estimate_date',
        'sales_cond',
        'title',
        'delivery_location',
        'expiration_date',
        'delivery_date',
        'pay_conditions',
        'remarks',
        'total_amount_taxnon',
        'tax_amount',
        'total_amount_taxin',
        'total_cost_amount',
        'tax_type',
        'tax_rate',
        'round_type',
        'coments',
        'staff_id',
        'staff_name',
        'created_name',
        'updated_name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];    
    
}
