<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Customer;
use App\Estimate;
use App\Estimate_detail;
use App\Taxrate;
use App\User;

/**
 * 模擬：受け入れテスト
 */
class AcceptanceTest extends DuskTestCase
{
    // Dusk実行前にマイグレーションする
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     * 65 assertions
     * @return void
     */
    public function testAcceptance()
    {
        $admin = factory(User::class)->create([
            'user_id'               => 'system',
            'user_name'             => 'システム管理者',
        ]);
        
        // ユーザーを新規に作成・保存
        $user_new = factory(User::class)->make([
            'id'                    => 2,
            'user_id'               => 'hoge',
            'user_name'             => 'プロペラ太郎',
            'password'              => 'hogehoge',
            'password-confirm'      => 'hogehoge',
        ]);
        
        // 編集する内容
        $update = factory(User::class)->make([
            'user_id'               => 'hoge',
            'user_name'             => 'プロペラ次郎',
            'password'              => 'mogemoge',
            'password_confirmation' => 'mogemoge',
        ]);

        // 税率
        $taxrate = factory(Taxrate::class)->make([
            'id' => 1,
            'effective_date' => '2014/04/01',
            'tax_rate' => '8',
        ]);

        // 得意先登録する内容
        $customer = factory(Customer::class)->make([
            'id' => 1,
            'customer_id'         => 'CUS000000001',
            'customer_name'       => '沖縄県立辺土名高等学校',
            'customer_kana'       => 'オキナワケンリツヘントナコウトウガッコウ',
            'representative_name' => '沖縄県立辺土名高等学校長',
            'representative_kana' => 'オキナワケンリツヘントナコウトウガッコウチョウ',
            'department_name'     => '事務局',
            'person_name'      	  => '新庄利治',
            'zip'      			  => '9051304',
            'address'      		  => '沖縄県国頭郡大宜味村饒波2015',
            'tel'      			  => '0980443103',
            'fax'      			  => '',
        ]);
        
        // 見積書のデータ作成
        $estimate = factory(Estimate::class)->make([
            'esti_no' => 80,
        ]);
        
        // 明細のデータ作成
        $estimate_detail = factory(Estimate_detail::class, 2)->make([]);
        
        $this->browse(function (Browser $browser) use ($admin, $user_new, $update, $taxrate, $customer, $estimate, $estimate_detail) {
        $browser->maximize()
                ->loginAs($admin)                                                // ログインする
                ->visit('/register')
                /* --------------------------------
                 * ユーザー新規登録 
                 *--------------------------------- */
                ->type('user_id',                $user_new->user_id)             // 入力:ユーザーID
                ->type('user_name',              $user_new->user_name)           // 入力:ユーザー名
                ->type('password',               $user_new->password)            // 入力:パスワード
                ->type('password_confirmation',  $user_new->password)            // 入力:パスワード確認
                ->press('insert')                                                // 押下:「新規登録」ボタン
                ->assertPathIs('/register')                                      // 確認:見積一覧であること
                ->assertSee('ユーザー登録処理が完了しました。')                    // 確認:メッセージ
                /* --------------------------------
                 * ログアウト
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#testLogout')                                           // 押下:「プロパティ」ボタン
                ->assertPathIs('/login')                                         // 確認:見積一覧であること
                /* --------------------------------
                 * 登録したユーザーでログイン
                 *--------------------------------- */
                ->type('user_id',               $user_new->user_id)              // ユーザーIDを入力
                ->type('password',              $user_new->password)             // パスワードを入力
                ->press('Login')                                                 // 送信ボタンをクリック
                ->assertPathIs('/estimates')                                     // 確認:見積一覧であること
                /* --------------------------------
                 * 編集 - ユーザー名変更
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#userEdit')                                             // 押下:「プロパティ」ボタン
                ->assertPathIs('/users/edit')
                ->type('user_name',              $update->user_name)             // 入力:得意先名
                ->press('confirm')                                               // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                  // 確認:確認ページであること
                ->press('insert')                                                // 押下:「登録」ボタン
                ->assertPathIs('/estimates')                                     // 確認:見積一覧であること
                ->assertSee($update->user_name)
                /* --------------------------------
                 * 編集 - パスワード変更
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#userEdit')                                             // 押下:「プロパティ」ボタン
                ->assertPathIs('/users/edit')
                ->type('password',               $update->password)              // 入力:パスワード
                ->type('password_confirmation',  $update->password_confirmation) // 入力:パスワード確認
                ->press('confirm')                                               // 押下:「確認」ボタン
                ->assertPathIs('/users/update')                                  // 確認:確認ページであること
                ->press('insert')                                                // 押下:「登録」ボタン
                ->assertPathIs('/estimates')                                     // 確認:見積一覧であること
                /* --------------------------------
                 * ログアウト - 再度ログインできるか
                 *--------------------------------- */
                ->press('#navlist')                                              // 押下:「ナビゲーション」ボタン
                ->press('#testLogout')                                           // 押下:「プロパティ」ボタン
                ->assertPathIs('/login')                                         // 確認:見積一覧であること
                ->type('user_id',               $update->user_id)                // ユーザーIDを入力
                ->type('password',              $update->password)               // パスワードを入力
                ->press('Login')                                                 // 送信ボタンをクリック
                ->assertPathIs('/estimates')                                    // 確認:見積一覧であること
                /* --------------------------------
                 * 税率新規登録 
                 *--------------------------------- */
                ->press('#menu')                                                 // 「MENU」ボタンを押下
                ->pause(1000)
                ->press('#master')                                               // 「マスタ管理」ボタンを押下
                ->pause(1000)
                ->press('#taxrates')                                             // 「税率マスタ」ボタンを押下
                ->assertPathIs('/taxrates')                                      // 税率マスタ一覧であることを確認
                ->press('#new-taxrates')                                         // 「新規登録」ボタンを押下
                ->assertPathIs('/taxrates/edit')                                 // 税率登録ページであることを確認
                ->press('#btnDate')                                              // 「カレンダー」ボタンを押下
                ->type('tax_rate', $taxrate->tax_rate)                           // 税率を入力
                ->press('confirm')                                               // 「確認」ボタンを押下
                ->assertPathIs('/taxrates/save')                                 // 確認ページであることを確認
                ->press('insert')                                                // 「新規登録」ボタンを押下
                ->assertPathIs('/taxrates')                                      // 税率マスタ一覧であることを確認        
                /* --------------------------------
                 * 得意先新規登録 
                 *--------------------------------- */
                ->press('#menu')                                                     // 「MENU」ボタンを押下
                ->pause(1000)
                ->press('#master')                                                   // 「マスタ管理」ボタンを押下
                ->pause(1000)
                ->press('#customers')                                                // 「得意先マスタ」ボタンを押下
                ->assertPathIs('/customers')                                         // 得意先マスタ一覧であることを確認
                ->press('#new')                                                      // 「新規登録」ボタンを押下
                ->assertPathIs('/customers/edit')                                    // 得意先登録ページであることを確認
                ->type('customer_name',            $customer->customer_name)         // 入力:得意先名
                ->type('customer_kana',            $customer->customer_kana)         // 入力:得意先カナ名
                ->type('representative_name',      $customer->representative_name)   // 入力:代表者名
                ->type('representative_kana',      $customer->representative_kana)   // 入力:代表者カナ名
                ->type('department_name',          $customer->department_name)       // 入力:部署名
                ->type('person_name',              $customer->person_name)           // 入力:担当者名
                ->type('zip1',              substr($customer->zip, 0, 3))            // 入力:郵便番号
                ->type('zip2',              substr($customer->zip, 3))               // 入力:郵便番号
                ->type('address',                  $customer->address)               // 入力:住所
                ->type('tel', str_replace("-", "", $customer->tel))                  // 入力:TEL
                ->type('fax', str_replace("-", "", $customer->fax))                  // 入力:FAX
                ->press('confirm')                                                   // 押下:「確認」ボタン
                ->assertPathIs('/customers/save')                                    // 確認:確認ページであること
                ->press('insert')                                                    // 押下:「新規登録」ボタン
                ->assertPathIs('/customers')                                         // 確認:得意先マスタ一覧であること
                /* --------------------------------------------
                 * 見積新規登録 
                 *--------------------------------------------- */
                ->press('#menu')                                                     // 「MENU」ボタンを押下
                ->pause(1000)
                ->press('#estimates')                                                // 「マスタ管理」ボタンを押下
                ->press('#customers_regist')                                         // 「見積りマスタ」ボタンを押下
                ->assertPathIs('/estimates/edit')                                    // 見積登録画面であることを確認
                /* --------------------------------
                 * 見積伝票情報
                 *--------------------------------- */
                ->type('customer_id',       "1")                                    // 入力：得意先ID
                ->type('title',             '件名テスト')                            // 入力：件名（名称）
                ->type('delivery_location', $estimate->delivery_location)           // 入力：納品場所
                ->type('expiration_date',   $estimate->expiration_date)             // 入力：有効期限
                ->type('delivery_date',     $estimate->delivery_date)               // 入力：受渡期日
                ->type('pay_conditions',    $estimate->pay_conditions)              // 入力：支払条件	
                ->type('remarks',           $estimate->remarks)                     // 入力：備考
                ->select('staff_id',        $user_new->id)                          // 入力：担当者名
                ->select('tax_rate',        $taxrate->id)                           // 入力：税額
                ->select('round_type',      $estimate->round_type)                  // 入力：税額
                /* --------------------------------
                 * 明細情報
                 *--------------------------------- */
                 //明細行1
                ->type('item_name[0]',        '商品名')                               // 入力：商品名
                ->type('item_description[0]', $estimate_detail[0]->item_description) // 入力：商品規格
                ->type('quantity[0]',         $estimate_detail[0]->quantity)         // 入力：数量
                ->type('unit[0]',             $estimate_detail[0]->unit)             // 入力：単位
                ->type('sal_price[0]',        $estimate_detail[0]->sal_price)        // 入力：売単価
                ->type('cost_price[0]',       $estimate_detail[0]->cost_price)       // 入力：原単価	
                ->select('tax_flg[0]',        $estimate_detail[0]->tax_flg)          // 入力：課税区分
                ->type('item_remarks[0]',     $estimate_detail[0]->item_remarks)     // 入力：備考        
                 //明細行2
                ->type('item_name[1]',        '商品名2')                              // 入力：商品名
                ->type('item_description[1]', $estimate_detail[1]->item_description) // 入力：商品規格
                ->type('quantity[1]',         $estimate_detail[1]->quantity)         // 入力：数量
                ->type('unit[1]',             $estimate_detail[1]->unit)             // 入力：単位
                ->type('sal_price[1]',        $estimate_detail[1]->sal_price)        // 入力：売単価
                ->type('cost_price[1]',       $estimate_detail[1]->cost_price)       // 入力：原単価	
                ->select('tax_flg[1]',        $estimate_detail[1]->tax_flg)          // 入力：課税区分
                ->type('item_remarks[1]',     $estimate_detail[1]->item_remarks)     // 入力：備考        
                 //登録
                ->type('title',             '件名テスト')                             // 入力：得意先ID
                ->pressAndWaitFor('#regist', 50)                                     // 押下：「登録」ボタン
                ->pause(2000)
                ->press('#test_confirm')                                             // 押下：ポップアップ画面「登録」ボタン
                ->pause(2000)
                ->assertPathIs('/estimates/edit')                                    // 確認：見積書登録であること
                ->assertSee('見積書を登録しました。')                                 // 確認:メッセージ
                 //一覧
                ->press('#list')                                                     // 押下：ポップアップ画面「登録」ボタン
                ->assertPathIs('/estimates')                                         // 確認：見積書登録であること
        
                /* ----------------------------------------------------------------------------
                 * 見積書変更へ遷移　登録内容が正しいかチェック
                 * ---------------------------------------------------------------------------- */
                ->press('#test_update_'.$estimate->esti_no)                          // 「変更」ボタンを押下
                ->assertPathIs('/estimates/edit/'.$estimate->esti_no)               // 確認：見積書登録であること
                /* --------------------------------
                 * 登録内容と同じかチェック -　見積伝票情報 
                 *--------------------------------- */
                ->assertInputValue('customer_id',       $customer->id)                  // 入力：得意先ID
                ->assertInputValue('title',             '件名テスト')                    // 入力：件名（名称）
                ->assertInputValue('delivery_location', $estimate->delivery_location)   // 入力：納品場所
                ->assertInputValue('expiration_date',   $estimate->expiration_date)     // 入力：有効期限
                ->assertInputValue('delivery_date',     $estimate->delivery_date)       // 入力：受渡期日
                ->assertInputValue('pay_conditions',    $estimate->pay_conditions)      // 入力：支払条件	
                ->assertInputValue('remarks',           $estimate->remarks)             // 入力：備考
                ->assertSelected('staff_id',            $user_new->id)                  // 入力：担当者名
                ->assertSelected('tax_rate',            $taxrate->id)                   // 入力：税額
                ->assertSelected('round_type',          $estimate->round_type)          // 入力：税額
                /* --------------------------------
                 * 登録内容と同じかチェック -　明細情報
                 *--------------------------------- */
                 //明細行1
                ->assertInputValue('item_name[0]',        '商品名')                                // 入力：商品名
                ->assertInputValue('item_description[0]', $estimate_detail[0]->item_description)  // 入力：商品規格
                ->assertInputValue('quantity[0]',         $estimate_detail[0]->quantity)          // 入力：数量
                ->assertInputValue('unit[0]',             $estimate_detail[0]->unit)              // 入力：単位
                ->assertInputValue('sal_price[0]',        $estimate_detail[0]->sal_price)         // 入力：売単価
                ->assertInputValue('cost_price[0]',       $estimate_detail[0]->cost_price)        // 入力：原単価	
                ->assertSelected('tax_flg[0]',            $estimate_detail[0]->tax_flg)           // 入力：課税区分
                ->assertInputValue('item_remarks[0]',     $estimate_detail[0]->item_remarks)      // 入力：備考        
                 //明細行2
                ->assertInputValue('item_name[1]',        '商品名2')                               // 入力：商品名
                ->assertInputValue('item_description[1]', $estimate_detail[1]->item_description)  // 入力：商品規格
                ->assertInputValue('quantity[1]',         $estimate_detail[1]->quantity)          // 入力：数量
                ->assertInputValue('unit[1]',             $estimate_detail[1]->unit)              // 入力：単位
                ->assertInputValue('sal_price[1]',        $estimate_detail[1]->sal_price)         // 入力：売単価
                ->assertInputValue('cost_price[1]',       $estimate_detail[1]->cost_price)        // 入力：原単価	
                ->assertSelected('tax_flg[1]',            $estimate_detail[1]->tax_flg)           // 入力：課税区分
                ->assertInputValue('item_remarks[1]',     $estimate_detail[1]->item_remarks)      // 入力：備考
            
                /* ----------------------------------------------------------------------------
                 * 複写(新規)
                 * ---------------------------------------------------------------------------- */
                ->press('#estimate_copy')                                                           // 「複写(新規)」ボタンを押下
                ->type('title',             '複写(新規)テスト')                                      // 入力：件名（名称）
                ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                ->pause(2000)
                ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                ->pause(2000)
                ->assertPathIs('/estimates/edit')                                                   // 確認：見積書登録であること
                ->assertSee('見積書を登録しました。')                                                 // 確認:メッセージ
                ->press('#list')                                                                    // 押下：ポップアップ画面「登録」ボタン
                ->press('#test_update_81')                                                          // 「変更」ボタンを押下
                ->assertPathIs('/estimates/edit/81')                                                // 確認：見積書登録であること
                /* --------------------------------
                 * 複写(新規)内容と同じかチェック -　見積伝票情報 
                 *--------------------------------- */
                ->assertInputValue('customer_id',       $customer->id)                  // 入力：得意先ID
                ->assertInputValue('title',             '複写(新規)テスト')              // 入力：件名（名称）
                ->assertInputValue('delivery_location', $estimate->delivery_location)   // 入力：納品場所
                ->assertInputValue('expiration_date',   $estimate->expiration_date)     // 入力：有効期限
                ->assertInputValue('delivery_date',     $estimate->delivery_date)       // 入力：受渡期日
                ->assertInputValue('pay_conditions',    $estimate->pay_conditions)      // 入力：支払条件	
                ->assertInputValue('remarks',           $estimate->remarks)             // 入力：備考
                ->assertSelected('staff_id',            $user_new->id)                  // 入力：担当者名
                ->assertSelected('tax_rate',            $taxrate->id)                   // 入力：税額
                ->assertSelected('round_type',          $estimate->round_type)          // 入力：税額
       
            
            
            
            
            
            
            
            
            
                ->pause(5000);
        });
    }
}
