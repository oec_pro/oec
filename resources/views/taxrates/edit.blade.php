@extends('layouts.app')

@section('content')

<!-- container -->
<div class="navi_sub">
    
    <!-- navi_sub_left -->
    <div class="navi_sub_left">
        <a class="btn btn-success btn-sm" href="{{ action('TaxratesController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
            <i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
        </a>
        <a id="up_list" class="btn btn-success btn-sm" href="{{ action('TaxratesController@index') }}" role="button" data-toggle="tooltip" title="税率一覧を表示">
            <i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
        </a>
    </div><!-- navi_sub_left -->
    
    <div class="navi_sub_right">
    </div>
    <div class="clear"></div>
</div><!-- container -->

<!-- container_page -->
<div class="container_page">
    
    <!-- title_box -->
    <div class="title_box">
        <div class="title_left">
            @if ( is_numeric($data['id']) )
                <h3><b><?php echo "税率変更"; ?></b></h3>
            @else
                <h3><b><?php echo "税率登録"; ?></b></h3>
            @endif
            <p id="title_mode" style="color:#FF0000;font-size:20px"></p>
        </div>
        <div class="clear"></div>
    </div>
    <hr>
    <!-- / title_box -->
        
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                {!! Form::open(['name' => 'form', 'url' => 'taxrates/save', 'class' => 'form-horizontal form-label-left']) !!}    
                    {!! Form::hidden('id', $data['id']) !!}

                    <div class="form-group {{ !empty($errors->first('effective_date')) ? ' has-error' :'' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">施行日<span class="required_color">※</span></label>
                        @if ( empty($data['confirm']) )
                            <div class="input-group date col-md-4">
                                {!! Form::text('effective_date', ( isset($data['effective_date']) && $data['effective_date'] != 0)? date('Y/m/d', strtotime($data['effective_date'])): date('Y/m/d'), array('class' => 'form-control en picker input-sm','maxlength' => '8', 'readonly' => 'readonly', 'placeholder' => '例）2017/01/01')); !!}
                                <span id="btnDate" class="input-group-btn">
                                    <button class="btn btn-default input-sm" type="button"><span class="fa fa-calendar"></span></button>
                                </span>
                            </div>
                        @else
                            <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                                {{ date('Y年m月d日', strtotime($data['effective_date'])) }}
                                {!! Form::hidden('effective_date', $data['effective_date']) !!}
                            </div>
                        @endif
                    </div>

                    <div class="form-group {{ !empty($errors->first('tax_rate')) ? ' has-error' :'' }}">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">税率<span class="required_color">※</span></label>
                        <div class="col-md-7 col-sm-4 col-xs-12 form-group has-feedback">
                            @if ( empty($data['confirm']) )
                                {!! Form::text('tax_rate', $data['tax_rate'], ['class' => 'form-control ja input-sm', 'placeholder' => '例）3', 'maxlength' => '10']) !!}
                                <span class="help-block">{{$errors->first('tax_rate')}}</span>
                            @else
                                {{ $data['tax_rate'] }}
                                {!! Form::hidden('tax_rate', $data['tax_rate']) !!}
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            @if ( empty($data['confirm']) )
                                {!! Form::submit("確認", ['class' => 'btn btn-primary regist_del_btn', 'name' => 'confirm']) !!}
                                {!! link_to(action('TaxratesController@index', ''), '一覧へ', ['class' => 'btn btn-default', 'id' => 'list']) !!}
                            @else
                                {!! Form::submit(is_numeric($data['id'])? '変更': '登録', ['class' =>'btn btn-primary regist_del_btn', 'name' => 'insert']) !!}
                                {!! Form::submit('入力画面に戻る', ['class' => 'btn btn-default', 'name' => 'back']) !!}
                            @endif
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
<!-- / container_page -->

@endsection

