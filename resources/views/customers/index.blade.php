@extends('layouts.app')

@section('content')
	<div class="navi_sub">
		<div class="navi_sub_left">
			<a id="new" class="btn btn-success btn-sm" href="{{ action('CustomersController@edit',"") }}" role="button" data-toggle="tooltip" title="新規登録ページを表示">
				<i class="fa fa-plus-circle" aria-hidden="true"></i><span class="navi_sub_marginleft">新規登録</span>
			</a>
			<a id="list" class="btn btn-success btn-sm" href="{{ action('CustomersController@index') }}" role="button" data-toggle="tooltip" title="得意先一覧を表示">
				<i class="fa fa-align-justify" aria-hidden="true"></i><span class="navi_sub_marginleft">一覧</span>
			</a>
		</div>
		<div class="clear"></div>
	</div>

<!-- container_page -->
<div class="container_page">
    
    <div class="title_box">
        <div class="title_left">
            <h3><b>得意先一覧</b></h3>
        </div>
        <div class="title_right">
        </div>
        <div class="clear"></div>
        <hr>
    </div>

    <!-- panel-body --> 
    <div class="panel-body">
        
        <!-- panel-group --> 
		<div class="panel-group" id="" role="tablist" aria-multiselectable="true">

            <!-- panel panel-default --> 
            <div class="panel panel-default">
                
                <!-- panel-heading headingOne --> 
                <div class="panel-heading" role="tab" id="headingOne">
                    <h5 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="" href="#collapseOne_three" aria-expanded="true" aria-controls="collapseOne">
                            <b>検索条件</b>
                            <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                        </a>
                    </h5>
                </div><!-- panel-heading headingOne --> 
                
                <!-- collapseOne_three --> 
                <div id="collapseOne_three" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <!-- panel-body --> 
                    <div class="panel-body">
                        {!! Form::open(['method' => 'get', 'url' => 'customers/search', 'class'=>'form-horizontal form-label-left']) !!}
                            <!-- estimates_egistration --> 
                            <div class="estimates_egistration">
                                <!-- estimates_list_box1 --> 
                                <div class="estimates_list_box1">
                                    <table border="1" id="table jambo_table"  class="table table-bordered">
                                        <tbody>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">得意先名</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('customer_name', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">得意先名カナ</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('customer_kana', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">TEL</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('tel', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">携帯番号</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('cellphone', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="registration_height estimates_list_height_box">
                                                <th class="contents_titile_width"><div class="col_title_right_req">住所</div></th>
                                                <td class="registration_height">
                                                    <div class="col_contents">
                                                        {{ Form::text('address', "", array_merge(['class' => 'form-control input-sm'])) }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- estimates_list_box1 -->
                                
                                <!-- estimates_list_box2 --> 
                                <div class="estimates_list_box2">
                                    <table class="border-color-style">
                                        <tbody>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th><div class="col_title_right_req border-color-style"></div></th>
                                            </tr>
                                            <tr class="estimates_list_height_box border-color-style">
                                                <th>
                                                    <div class="">
                                                        {{ Form::submit('検索', ['class' => 'btn btn-primary btn-sm btn_list_mstyle1', 'data-toggle'=>'tooltip', 'title'=>'この条件で検索する']) }}
                                                        {{ Form::button('クリア', ['id' => 'clear', 'class' => 'btn btn-warning btn-sm btn_list_mstyle2', 'data-toggle'=>'tooltip', 'title'=>'条件をクリアする']) }}
                                                    </div>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- estimates_list_box -->
                                <div class="clear"></div>
                            </div>
                            <!-- estimates_egistration -->
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- collapseOne_three headingOne -->

            </div>
            <!-- panel panel-default -->                
                
            <!-- order_detail_info_wrap --> 
            <div id="order_detail_info_wrap">
                @if (count($data['results']) >= 1)
                    <table summary="年号一覧" class="table table-responsive table-striped table-condensed jambo_table bulk_action table_frame estimate_list">
                        <thead>
                            <tr class="headings">
                                <th class="detail_title">ID</th>
                                <th class="detail_title">得意先ID</th>
                                <th class="detail_title">得意先名</th>
                                <th class="detail_title">得意先カナ名</th>
                                <th class="detail_title">部署名</th>
                                <th class="detail_title">担当者名</th>
                                <th class="detail_title">TEL</th>
                                <th class="detail_title">携帯番号</th>
                                <th class="detail_title"></th>
                            </tr>
                        </thead>
                        <tbody id="tbodyLine">
                            @foreach($data['results'] as $result)
                                <tr id="trLine0">
                                    <td class="contents_box_center">{{ $result->id }}</td>
                                    <td class="contents_box_center">{{ $result->customer_id }}</td>
                                    <td class="contents_box_left">{{ $result->customer_name }}</td>
                                    <td class="contents_box_left">{{ $result->customer_kana }}</td>
                                    <td class="contents_box_left">{{ $result->department_name }}</td>
                                    <td class="contents_box_left">{{ $result->person_name }}</td>
                                    <td class="contents_box_left">{{ $result->tel }}</td>
                                    <td class="contents_box_left">{{ $result->cellphone }}</td>
                                    <td class="contents_box_center">
                                        {!! link_to(action('CustomersController@edit', [$result->id]), '変更', ['class' => 'btn btn-info', 'id' => 'test_update_'.$result->id]) !!}
                                        <button id = "test_del_{{ $result->id }}" type="button" class="btn btn-danger" onClick="confirmDel({{ $result->id }});return false;">削除</button>
                                    </td>
                                </tr><!-- trLine0 -->
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data['results']->links() }}
                @elseif(!is_null($data['results']))
                    <div class="alert alert-info">該当する情報がありませんでした。</div>
                @endif
            </div><!-- order_detail_info_wrap -->
        </div><!-- panel-group -->
    </div>　<!-- panel-body  -->
        
</div>
<!-- /container_page -->

<!-- sweetalert -->
<link rel="stylesheet" href="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="{{ Config::get('const.core') }}/assets/js/sweetalert/dist/sweetalert.min.js"></script>
<script>
    /* 検索項目クリアー */
    $(document).on("click", "#clear", function(){
        $('[name=customer_name]').val('');
        $('[name=customer_kana]').val('');
        $('[name=tel]').val('');
        $('[name=address]').val('');
    });  
    
    function confirmDel(id) {
        swal({
            title: "削除確認",
            text: "削除してもよろしいですか？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "削除",
            cancelButtonText: "キャンセル",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
                window.open(location.origin + "{!! Config::get('const.core') !!}/customers/" + "del/" + id, '_self');
                return true;
            } else {
                return false;
            }
        }); 
    }
</script>
@endsection