<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Customer;
use App\Estimate;
use App\Estimate_detail;
use App\Taxrate;
use App\User;

/**
 * 見積りに関するテスト
 */
class EstimateTest extends DuskTestCase
{
    // Dusk実行前にマイグレーションする
    use DatabaseMigrations;

    /**
     * 見積書に関する操作(作成、編集、削除)のテスト
     * 27 assertions
     * @return void
     */
    public function testCRUD()
    {
        // ユーザーを登録作成
        $user = factory(User::class)->create();
//        factory(App\Country::class, 10)->create();        
        
        // 税率を登録作成
        $taxrate = factory(Taxrate::class)->create();
        
        // 得意先を登録作成
        $customer = factory(Customer::class, 2)->create();
        
        // 見積書のデータ作成
        $estimate = factory(Estimate::class)->make([
            'esti_no' => 80,
        ]);
        
        // 明細のデータ作成
        $estimate_detail = factory(Estimate_detail::class, 2)->make([]);

        // 見積書のデータ作成(更新用)
        $esti_update = factory(Estimate::class)->make([
            'esti_no' => 80,
        ]);
        
        // 明細のデータ作成(更新用)
        $esti_det_update = factory(Estimate_detail::class, 2)->make([]);

        $this->browse(function (Browser $browser) use ($user, $taxrate, $customer, $estimate, $estimate_detail, $esti_update, $esti_det_update) {
        $browser->visit('/login')                   // ログインページへ移動
                ->maximize()
                ->type('user_id', $user->user_id)   // ユーザーIDを入力
                ->type('password', 'secret')        // パスワードを入力
                ->press('Login')                    // 送信ボタンをクリック
                ->assertPathIs('/estimates')        // 見積一覧であることを確認
                ->press('#new')                     // 「MENU」ボタンを押下
                ->assertPathIs('/estimates/edit')   // 見積登録画面であることを確認
            
                /* ----------------------------------------------------------------------------
                 * 新規登録
                 * ---------------------------------------------------------------------------- */
                /* --------------------------------
                 * 見積伝票情報
                 *--------------------------------- */
                ->type('customer_id',       $customer[0]->id)               // 入力：得意先ID
                ->type('title',             '件名テスト')                    // 入力：件名（名称）
                ->type('delivery_location', $estimate->delivery_location)   // 入力：納品場所
                ->type('expiration_date',   $estimate->expiration_date)     // 入力：有効期限
                ->type('delivery_date',     $estimate->delivery_date)       // 入力：受渡期日
                ->type('pay_conditions',    $estimate->pay_conditions)      // 入力：支払条件	
                ->type('remarks',           $estimate->remarks)             // 入力：備考
                ->select('staff_id',        $user->id)                      // 入力：担当者名
                ->select('tax_rate',        $taxrate->id)                   // 入力：税額
                ->select('round_type',      $estimate->round_type)          // 入力：税額
                /* --------------------------------
                 * 明細情報
                 *--------------------------------- */
                 //明細行1
                ->type('item_name[0]',        '商品名')                               // 入力：商品名
                ->type('item_description[0]', $estimate_detail[0]->item_description) // 入力：商品規格
                ->type('quantity[0]',         $estimate_detail[0]->quantity)         // 入力：数量
                ->type('unit[0]',             $estimate_detail[0]->unit)             // 入力：単位
                ->type('sal_price[0]',        $estimate_detail[0]->sal_price)        // 入力：売単価
                ->type('cost_price[0]',       $estimate_detail[0]->cost_price)       // 入力：原単価	
                ->select('tax_flg[0]',        $estimate_detail[0]->tax_flg)          // 入力：課税区分
                ->type('item_remarks[0]',     $estimate_detail[0]->item_remarks)     // 入力：備考
                 //明細行2
//                ->type('item_name[1]',        '商品名2')                              // 入力：商品名
//                ->type('item_description[1]', $estimate_detail[1]->item_description) // 入力：商品規格
//                ->type('quantity[1]',         $estimate_detail[1]->quantity)         // 入力：数量
//                ->type('unit[1]',             $estimate_detail[1]->unit)             // 入力：単位
//                ->type('sal_price[1]',        $estimate_detail[1]->sal_price)        // 入力：売単価
//                ->type('cost_price[1]',       $estimate_detail[1]->cost_price)       // 入力：原単価	
//                ->select('tax_flg[1]',        $estimate_detail[1]->tax_flg)          // 入力：課税区分
//                ->type('item_remarks[1]',     $estimate_detail[1]->item_remarks)     // 入力：備考
                 //登録
                ->type('title',             '件名テスト')                             // 入力：得意先ID
                ->pressAndWaitFor('#regist', 50)                                     // 押下：「登録」ボタン
                ->pause(2000)
                ->press('#test_confirm')                                             // 押下：ポップアップ画面「登録」ボタン
                ->pause(2000)
                ->assertPathIs('/estimates/edit')                                    // 確認：見積書登録であること
                ->assertSee('見積書を登録しました。')                                 // 確認:メッセージ
                 //一覧
                ->press('#list')                                                     // 押下：ポップアップ画面「登録」ボタン
                ->assertPathIs('/estimates')                                         // 確認：見積書登録であること
            
                /* ----------------------------------------------------------------------------
                 * 更新処理
                 * ---------------------------------------------------------------------------- */
                ->pause(2000)
                ->press('#test_update_'.$estimate->esti_no)                          // 「変更」ボタンを押下
                ->pause(2000)
                ->assertPathIs('/estimates/edit/'.$estimate->esti_no)               // 確認：見積書登録であること
                /* --------------------------------
                 * 登録内容と同じかチェック -　見積伝票情報 
                 *--------------------------------- */
                ->assertInputValue('customer_id',       $customer[0]->id)                // 入力：得意先ID
                ->assertInputValue('title',             '件名テスト')                    // 入力：件名（名称）
                ->assertInputValue('delivery_location', $estimate->delivery_location)   // 入力：納品場所
                ->assertInputValue('expiration_date',   $estimate->expiration_date)     // 入力：有効期限
                ->assertInputValue('delivery_date',     $estimate->delivery_date)       // 入力：受渡期日
                ->assertInputValue('pay_conditions',    $estimate->pay_conditions)      // 入力：支払条件	
                ->assertInputValue('remarks',           $estimate->remarks)             // 入力：備考
                ->assertSelected('staff_id',            $user->id)                      // 入力：担当者名
                ->assertSelected('tax_rate',            $taxrate->id)                   // 入力：税額
                ->assertSelected('round_type',          $estimate->round_type)          // 入力：税額
                /* --------------------------------
                 * 登録内容と同じかチェック -　明細情報
                 *--------------------------------- */
                 //明細行1
                ->assertInputValue('item_name[0]',        '商品名')                                // 入力：商品名
                ->assertInputValue('item_description[0]', $estimate_detail[0]->item_description)  // 入力：商品規格
                ->assertInputValue('quantity[0]',         $estimate_detail[0]->quantity)          // 入力：数量
                ->assertInputValue('unit[0]',             $estimate_detail[0]->unit)              // 入力：単位
                ->assertInputValue('sal_price[0]',        $estimate_detail[0]->sal_price)         // 入力：売単価
                ->assertInputValue('cost_price[0]',       $estimate_detail[0]->cost_price)        // 入力：原単価	
                ->assertSelected('tax_flg[0]',            $estimate_detail[0]->tax_flg)           // 入力：課税区分
                ->assertInputValue('item_remarks[0]',     $estimate_detail[0]->item_remarks)     // 入力：備考

                /* --------------------------------
                 * 見積伝票情報 - データの更新
                 *--------------------------------- */
                ->type('customer_id',       $customer[1]->id)                       // 入力：得意先ID
                ->type('title',             '件名テスト2')                           // 入力：件名（名称）
                ->type('delivery_location', $esti_update->delivery_location)        // 入力：納品場所
                ->type('expiration_date',   $esti_update->expiration_date)          // 入力：有効期限
                ->type('delivery_date',     $esti_update->delivery_date)            // 入力：受渡期日
                ->type('pay_conditions',    $esti_update->pay_conditions)           // 入力：支払条件	
                ->type('remarks',           $esti_update->remarks)                  // 入力：備考
                ->select('staff_id',        $user->id)                              // 入力：担当者名
                ->select('tax_rate',        $esti_update->tax_rate)                 // 入力：税額
                ->select('round_type',      $esti_update->round_type)               // 入力：税額
                /* --------------------------------
                 * 明細情報- データの更新
                 *--------------------------------- */
                 //明細行1
                ->type('item_name[0]',        '商品名1-1')                               // 入力：商品名
                ->type('item_description[0]', $esti_det_update[0]->item_description) // 入力：商品規格
                ->type('quantity[0]',         $esti_det_update[0]->quantity)         // 入力：数量
                ->type('unit[0]',             $esti_det_update[0]->unit)             // 入力：単位
                ->type('sal_price[0]',        $esti_det_update[0]->sal_price)        // 入力：売単価
                ->type('cost_price[0]',       $esti_det_update[0]->cost_price)       // 入力：原単価	
                ->select('tax_flg[0]',        $esti_det_update[0]->tax_flg)          // 入力：課税区分
                ->type('item_remarks[0]',     $esti_det_update[0]->item_remarks)     // 入力：備考
                 //登録
                ->type('title',             '件名テスト2')                            // 入力：得意先ID
                ->pressAndWaitFor('#regist', 5)                                     // 押下：「登録」ボタン
                ->pause(5000)
                ->press('#test_confirm')                                             // 押下：ポップアップ画面「登録」ボタン
                ->pause(5000)
                ->assertPathIs('/estimates/edit')                                    // 確認：見積書登録であること
                 //一覧
                ->press('#list')                                                     // 押下：ポップアップ画面「登録」ボタン
                ->assertPathIs('/estimates')                                         // 確認：見積書登録であること
                ->pause(5000)
                ->press('#test_update_'.$estimate->esti_no)                          // 「変更」ボタンを押下
                ->assertPathIs('/estimates/edit/'.$estimate->esti_no)                // 確認：見積書変更であること
                /* --------------------------------
                 * 削除
                 *--------------------------------- */
                ->pressAndWaitFor('#delete', 50)                                     // 「削除」ボタンを押す
                ->pause(2000)
                ->press('#test_confirm')                                             // 押下：ポップアップ画面「削除」ボタン
                ->pause(2000)
                ->assertPathIs('/estimates');                                        // 確認：見積一覧であること
        });
    }    
    
    /**
     * 見積書登録(初期表示)のテスト
     *　33 assertions
     * @return void
     */
    public function testLoade()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();

        //操作不可：見積日
        $estimateDate = 'readonly="readonly" placeholder="例）2017/01/01" tabindex="-1" name="estimate_date" type="text" value="'.date('Y/m/d').'"';

        
        $this->browse(function (Browser $browser) use ($user, $estimateDate) {
            $browser->loginAs($user)
//                    ->maximize()  最大化
                    ->visit('/estimates/edit')                                          //確認：見積書登録画面へ遷移されていること。
                    ->assertDontSee('見積書出力')                                        //確認：ボタン「見積書出力」が非表示であること。
                    ->assertDontSee('御見積書出力')                                      //           「御見積書出力」が非表示であること。
                    ->assertDontSee('請書出力')                                          //           「請書出力」が非表示であること。
                    ->assertDontSee('納品書出力')                                        //           「納品書出力」が非表示であること。
                    ->assertDontSee('請求書出力')                                        //           「請求書出力」が非表示であること。
                    ->assertSee('登録')                                                 //　　　　　　「登録」が表示されていること。
                    ->assertSourceHas('id="delete" disabled="disabled"')                //          　「削除」が操作不可であること。
                    ->assertSee('見積書登録')                                            //確認：タイトル「見積書登録」が表示されていること。

                    /* --------------------------------
                     * 見積伝票情報
                     *--------------------------------- */
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="customer_name"')       //確認：テキスト「得意先名」が操作不可であること。
                    ->assertSourceHas($estimateDate)                                    //             「見積日」  が操作不可で現在日付が表示されていること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="total_amount_taxnon"') //             「税抜合計」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="tax_amount"')          //             「税額」    が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="total_amount_taxin"')  //             「税込合計」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="total_cost_amount"')   //             「原価合計」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="gross_profit"')        //             「粗利」    が操作不可であること。

                    /* --------------------------------
                     * 明細情報
                     *--------------------------------- */
                     //商品No
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[0]"')          //確認：テキスト「商品No」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[1]"')          //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[2]"')          //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[3]"')          // 
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[4]"')          //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="item_id[5]"')          //
                     //売上金額
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[0]"')       //確認：テキスト「売上金額」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[1]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[2]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[3]"')       // 
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[4]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="sal_amount[5]"')       //
                     //原価金額
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[0]"')       //確認：テキスト「売上金額」が操作不可であること。
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[1]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[2]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[3]"')       // 
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[4]"')       //
                    ->assertSourceHas('readonly="readonly" tabindex="-1" name="cost_amount[5]"')       //
                    
                    ->assertRadioSelected('s2', '1');      //確認：ラジオボタン「見積」が選択されていること。
//                    ->screenshot("estimates/edit");            
        });
    }
    
    /**
     * バリデーションの動作を確認する (createアクション)
     *
     * @return void
     */
    public function testValidationCreate()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 税率を登録作成
        $taxrate = factory(Taxrate::class)->create();
        
        // 得意先を登録作成
        $customer = factory(Customer::class, 2)->create();
        
        // 作成のみを行う。保存はしない
        $estimate = factory(Estimate::class)->make([]);
        
        // 作成のみを行う。保存はしない
        $estimate_detail = factory(Estimate_detail::class)->make([]);
        
        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $taxrate, $customer, $estimate, $estimate_detail) {
            $browser->loginAs($user)
                    ->maximize()
                    ->visit('/estimates/edit')
                    /* 必須チェック */
                    ->type('delivery_location',                 $estimate->delivery_location)           // 入力：納品場所
                    ->type('expiration_date',                   $estimate->expiration_date)             // 入力：有効期限
                    ->type('delivery_date',                     $estimate->delivery_date)               // 入力：受渡期日
                    ->type('pay_conditions',                    $estimate->pay_conditions)              // 入力：支払条件	
                    ->type('remarks',                           $estimate->remarks)                     // 入力：備考
                    ->select('staff_id',                        '')                                     // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertPathIs('/estimates/save')                                                   // 確認：見積書登録であること
                    ->assertSee('得意先名は必須です。')                                                  // 確認:メッセージ
                    ->assertSee('件名は必須です。')                                                      // 確認:メッセージ
                    ->assertSee('担当者名は必須です。')                                                  // 確認:メッセージ
                    ->assertSee('明細情報が入力されておりません。')                                       // 確認:メッセージ

                    ->type('customer_id',                       $customer[0]->id)                       // 入力:得意先ID
                    ->type('title',                             $estimate->title)                       // 入力:件名（名称）
                    ->select('tax_rate',                        $taxrate->tax_rate)                     // 入力：税率
                    ->type('item_remarks[0]',                   $estimate_detail->item_remarks)         // 入力：商品規格
                    ->select('staff_id',                        $estimate->staff_id)                    // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertSee('商品名は必須です。')                                                    // 確認:メッセージ
                    ->assertSee('数量は必須です。')                                                      // 確認:メッセージ
                    ->assertSee('単位は必須です。')                                                      // 確認:メッセージ
//                    ->assertSee('売単価は必須です。')                                                    // 確認:メッセージ
//                    ->assertSee('原単価は必須です。')                                                    // 確認:メッセージ
//                    ->assertSee('売上金額は必須です。')                                                  // 確認:メッセージ
//                    ->assertSee('原価金額は必須です。')                                                  // 確認:メッセージ
//                    ->assertSee('課税区分は必須です。')                                                  // 確認:メッセージ
                
                    /* 数値チェック */
                    ->type('item_name[0]',                      '商品名1')                              // 入力：商品名
                    ->type('item_description[0]',               'abcde')                                // 入力：商品規格
                    ->type('quantity[0]',                       'abcde')                                // 入力：数量
                    ->type('unit[0]',                           $estimate_detail->unit)                 // 入力：単位
                    ->type('sal_price[0]',                      'abcde')                                // 入力：売単価
                    ->type('cost_price[0]',                     'abcde')                                // 入力：原単価
                    ->select('tax_flg[0]',                      $estimate_detail->tax_flg)              // 入力：課税区分
                    ->select('staff_id',                        $estimate->staff_id)                    // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertSee('数量は数字にしてください。')                                             // 確認:メッセージ
                    ->assertSee('売単価は数字にしてください。')                                           // 確認:メッセージ
                    ->assertSee('原単価は数字にしてください。')                                           // 確認:メッセージ
            
                    /* 数値マイナスチェック */
//                    ->type('quantity[0]',                       '-100')                                 // 入力：数量
//                    ->type('sal_price[0]',                      '-100')                                 // 入力：売単価
                    ->type('cost_price[0]',                     '-100')                                 // 入力：原単価
                    ->select('staff_id',                        $estimate->staff_id)                    // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
//                    ->assertSee('数量は0以上にしてください。')                                            // 確認:メッセージ
//                    ->assertSee('売単価は0以上にしてください。')                                          // 確認:メッセージ
                    ->assertSee('原単価は0以上にしてください。');                                         // 確認:メッセージ
            
        });
    }            
    
    /**
     * バリデーションの動作を確認する (editアクション)
     *
     * @return void
     */
    public function testValidationEdit()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 税率を登録作成
        $taxrate = factory(Taxrate::class)->create();
        
        // 得意先を登録作成
        $customer = factory(Customer::class, 2)->create();
        
        // 作成のみを行う。保存はしない
        $estimate = factory(Estimate::class)->make([
            'esti_no' => 80,
        ]);
        
        // 作成のみを行う。保存はしない
        $estimate_detail = factory(Estimate_detail::class, 2)->make([]);
//        
//        // バリデーション・エラーを起こす
        $this->browse(function (Browser $browser) use ($user, $taxrate, $customer, $estimate, $estimate_detail) {
            $browser->loginAs($user)
                    ->maximize()
                    ->visit('/estimates/edit')
                    /* --------------------------------
                     * 見積伝票情報
                     *--------------------------------- */
                    ->type('customer_id',       $customer[0]->id)                  // 入力：得意先ID
                    ->type('title',             '件名テスト')                    // 入力：件名（名称）
                    ->type('delivery_location', $estimate->delivery_location)   // 入力：納品場所
                    ->type('expiration_date',   $estimate->expiration_date)     // 入力：有効期限
                    ->type('delivery_date',     $estimate->delivery_date)       // 入力：受渡期日
                    ->type('pay_conditions',    $estimate->pay_conditions)      // 入力：支払条件	
                    ->type('remarks',           $estimate->remarks)             // 入力：備考
                    ->select('staff_id',        $user->id)                      // 入力：担当者名
                    ->select('tax_rate',        $taxrate->tax_rate)             // 入力：税額
                    /* --------------------------------
                     * 明細情報
                     *--------------------------------- */
                     //明細行1
                    ->type('item_name[0]',        '商品名')                               // 入力：商品名
                    ->type('item_description[0]', $estimate_detail[0]->item_description) // 入力：商品規格
                    ->type('quantity[0]',         $estimate_detail[0]->quantity)         // 入力：数量
                    ->type('unit[0]',             $estimate_detail[0]->unit)             // 入力：単位
                    ->type('sal_price[0]',        $estimate_detail[0]->sal_price)        // 入力：売単価
                    ->type('cost_price[0]',       $estimate_detail[0]->cost_price)       // 入力：原単価	
                    ->select('tax_flg[0]',        $estimate_detail[0]->tax_flg)          // 入力：課税区分
                    ->type('item_remarks[0]',     $estimate_detail[0]->item_remarks)     // 入力：備考
//                     //明細行2
//                    ->type('item_name[1]',        '商品名2')                              // 入力：商品名
//                    ->type('item_description[1]', $estimate_detail[1]->item_description) // 入力：商品規格
//                    ->type('quantity[1]',         $estimate_detail[1]->quantity)         // 入力：数量
//                    ->type('unit[1]',             $estimate_detail[1]->unit)             // 入力：単位
//                    ->type('sal_price[1]',        $estimate_detail[1]->sal_price)        // 入力：売単価
//                    ->type('cost_price[1]',       $estimate_detail[1]->cost_price)       // 入力：原単価	
//                    ->select('tax_flg[1]',        $estimate_detail[1]->tax_flg)          // 入力：課税区分
//                    ->type('item_remarks[1]',     $estimate_detail[1]->item_remarks)     // 入力：備考
                     //登録
                    ->type('title',             '件名テスト')                             // 入力：得意先ID
                    ->pressAndWaitFor('#regist', 50)                                     // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                             // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertPathIs('/estimates/edit')                                    // 確認：見積書登録であること
                     //一覧
                    ->press('#list')                                                     // 押下：ポップアップ画面「登録」ボタン
                    ->assertPathIs('/estimates')                                         // 確認：見積書登録であること
                    /* ----------------------------------------------------------------------------
                     * 更新処理
                     * ---------------------------------------------------------------------------- */
                    ->press('#test_update_'.$estimate->esti_no)                          // 「変更」ボタンを押下
                    ->assertPathIs('/estimates/edit/'.$estimate->esti_no)                // 確認：見積書更新であること     
            
//                    ->pressAndWaitFor('#row_delete', 50)                                 // 「削除」ボタンを押す
//                    ->press('#test_confirm')                                             // 押下：ポップアップ画面「削除」ボタン
                
                    /* 必須チェック */
                    ->type('customer_id',                       '')                                     // 入力:得意先ID
                    ->type('title',                             '')                                     // 入力:件名（名称）
                    //明細行1
                    ->type('item_name[0]',        '')                                                   // 入力：商品名
                    ->type('item_description[0]', '')                                                   // 入力：商品規格
                    ->type('quantity[0]',         '')                                                   // 入力：数量
                    ->type('unit[0]',             '')                                                   // 入力：単位
                    ->type('sal_price[0]',        '')                                                   // 入力：売単価
                    ->type('cost_price[0]',       '')                                                   // 入力：原単価	
                    ->select('tax_flg[0]',        '0')                                                  // 入力：課税区分
                    ->type('item_remarks[0]',     '')                                                   // 入力：備考
                    ->select('staff_id',                        '')                                     // 入力：担当者名
                
                    ->mouseover('#navlist')
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertPathIs('/estimates/save')                                                   // 確認：見積書登録であること
                    ->assertSee('得意先名は必須です。')                                                  // 確認:メッセージ
                    ->assertSee('件名は必須です。')                                                      // 確認:メッセージ
                    ->assertSee('担当者名は必須です。')                                                  // 確認:メッセージ
                    ->assertSee('明細情報が入力されておりません。')                                       // 確認:メッセージ

                    /* 必須チェック 明細*/
                    ->type('customer_id',                       $customer[0]->id)                       // 入力:得意先ID
                    ->type('title',                             $estimate->title)                       // 入力:件名（名称）
                    ->type('item_remarks[0]',                   $estimate_detail[0]->item_remarks)      // 入力：備考
                    ->select('staff_id',                        $user->id)                              // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertSee('商品名は必須です。')                                                    // 確認:メッセージ
                    ->assertSee('数量は必須です。')                                                      // 確認:メッセージ
                    ->assertSee('単位は必須です。')                                                      // 確認:メッセージ
//                    ->assertSee('売単価は必須です。')                                                    // 確認:メッセージ
//                    ->assertSee('原単価は必須です。')                                                    // 確認:メッセージ
//                    ->assertSee('売上金額は必須です。')                                                  // 確認:メッセージ
//                    ->assertSee('原価金額は必須です。')                                                  // 確認:メッセージ
//                    ->assertSee('課税区分は必須です。')                                                // 確認:メッセージ
                
                    /* 数値チェック */
                    ->type('item_name[0]',                      '商品名1')                              // 入力：商品名
                    ->type('item_description[0]',               'abcde')                                // 入力：商品規格
                    ->type('quantity[0]',                       'abcde')                                // 入力：数量
                    ->type('unit[0]',                           $estimate_detail[0]->unit)              // 入力：単位
                    ->type('sal_price[0]',                      'abcde')                                // 入力：売単価
                    ->type('cost_price[0]',                     'abcde')                                // 入力：原単価
                    ->select('tax_flg[0]',                      $estimate_detail[0]->tax_flg)           // 入力：課税区分
                    ->select('staff_id',                        $estimate->staff_id)                    // 入力：担当者名
                    ->mouseover('#navlist')
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertSee('数量は数字にしてください。')                                             // 確認:メッセージ
                    ->assertSee('売単価は数字にしてください。')                                           // 確認:メッセージ
                    ->assertSee('原単価は数字にしてください。')                                          // 確認:メッセージ
            
                    /* 数値マイナスチェック */
//                    ->type('quantity[0]',                       '-100')                                 // 入力：数量
//                    ->type('sal_price[0]',                      '-100')                                // 入力：売単価
                    ->type('cost_price[0]',                     '-100')                                // 入力：原単価
                    ->select('staff_id',                        $estimate->staff_id)                    // 入力：担当者名
                    ->pressAndWaitFor('#regist', 50)                                                    // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                                            // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
//                    ->assertSee('数量は0以上にしてください。')                                            // 確認:メッセージ
//                    ->assertSee('売単価は0以上にしてください。')                                          // 確認:メッセージ
                    ->assertSee('原単価は0以上にしてください。');                                         // 確認:メッセージ
            
            
        });
    }           
    
    /**
     * UIを確認する
     *　36 assertions
     * @return void
     */
    public function testUI()
    {
        // ユーザーを新規に作成・保存
        $user = factory(User::class)->create();
        
        // 税率を登録作成
        $taxrate = factory(Taxrate::class)->create();
        
        // 得意先を登録作成
        $customer = factory(Customer::class, 2)->create();
        
        // 見積を登録作成
        $estimate = factory(Estimate::class)->create([
            'esti_no' => 1,
        ]);
        
        // 見積明細を登録作成
        for ($i=1;$i<6;$i++) {
            $estimate_detail[$i-1] = factory(Estimate_detail::class)->create([
                'esti_no'      => 1,
                'esti_line_no' => $i,
                'item_name'    => '商品'.$i,
            ]);
        }      
        
        // 見積明細を登録作成(ADD用)
        for ($i=6;$i<8;$i++) {
            $add_dsti_det[$i] = factory(Estimate_detail::class)->make([
                'esti_no' => 1,
                'item_name'    => '商品'.$i,
            ]);
        }      

        $this->browse(function (Browser $browser) use ($user, $taxrate, $customer, $estimate, $estimate_detail, $add_dsti_det) {
            $browser->loginAs($user)
                    ->maximize()
                    ->visit('/estimates/edit/'.$estimate->esti_no)
                    ->pressAndWaitFor('#test_tgl', 3000)                                    // 押下：「見積伝票情報」トグルボタン
                                
                    /* -------------------------------------------
                     * ドラッグアンドドロップチェック 
                     * ------------------------------------------- */
                
                    /* -----操作１回目------------------------------------------------- 
                     * 行移動が正しくされてるか
                     * --------------------------------- ------------------------------*/
                    ->screenshot('/estimates/ui/step1_move_before')                         // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    //5行目を3行目に移動
                    ->drag('#test_img4', '#test_img1')
                    //1行目を3行目に移動
                    ->drag('#test_img0', '#test_img3')
                    ->pause(2000)
                    ->screenshot('/estimates/ui/step1_move_after')                          // 画像：後 ドラッグアンドドロップ後画像スナップショット
                    ->assertInputValue('item_name[0]',  $estimate_detail[1]->item_name)     // 確認：1行目 商品名2
                    ->assertInputValue('item_name[1]',  $estimate_detail[4]->item_name)     // 確認：2行目 商品名5
                    ->assertInputValue('item_name[2]',  $estimate_detail[0]->item_name)     // 確認：3行目 商品名1
                    ->assertInputValue('item_name[3]',  $estimate_detail[2]->item_name)     // 確認：4行目 商品名3
                    ->assertInputValue('item_name[4]',  $estimate_detail[3]->item_name)     // 確認：5行目 商品名4
            
                    /* -----操作２回目------------------------------------------------- 
                     * 空行へデータ入力後、行移動が正しくされてるか
                     * --------------------------------------------------------------- */
                    //明細行6行目追加 商品名6
                    ->screenshot('/estimates/ui/step2_move_before')                         // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    ->type('item_name[5]',              $add_dsti_det[6]->item_name." 操作２回目 空行データ入力")        // 入力：商品名
                    ->type('item_description[5]',       $add_dsti_det[6]->item_description) // 入力：商品規格
                    ->type('quantity[5]',               $add_dsti_det[6]->quantity)         // 入力：数量
                    ->type('unit[5]',                   $add_dsti_det[6]->unit)             // 入力：単位
                    ->type('sal_price[5]',              $add_dsti_det[6]->sal_price)        // 入力：売単価
                    ->type('cost_price[5]',             $add_dsti_det[6]->cost_price)       // 入力：原単価	
                    ->select('tax_flg[5]',              $add_dsti_det[6]->tax_flg)          // 入力：課税区分
                    ->type('item_remarks[5]',           $add_dsti_det[6]->item_remarks)     // 入力：備考
                    //6行目を2行目に移動
                    ->drag('#test_img5', '#test_img0')
                    ->pause(2000)
                    ->screenshot('/estimates/ui/step2_move_after')                          // 画像：後 ドラッグアンドドロップ後画像スナップショット
                    ->assertInputValue('item_name[0]',  $estimate_detail[1]->item_name)     // 確認：1行目 商品名2
                    ->assertInputValue('item_name[1]',  $add_dsti_det[6]->item_name." 操作２回目 空行データ入力")        // 確認：2行目 商品名6
                    ->assertInputValue('item_name[2]',  $estimate_detail[4]->item_name)     // 確認：3行目 商品名5
                    ->assertInputValue('item_name[3]',  $estimate_detail[0]->item_name)     // 確認：4行目 商品名1
                    ->assertInputValue('item_name[4]',  $estimate_detail[2]->item_name)     // 確認：5行目 商品名3
                    ->assertInputValue('item_name[5]',  $estimate_detail[3]->item_name)     // 確認：5行目 商品名4

                    /* -----操作３回目------------------------------------------------- 
                     * 行追加ボタン押下後、データ入力後、行移動が正しくされてるか
                     * --------------------------------------------------------------- */
                    //空行追加  商品名7
                    ->press('#row_add')                                                                     // 押下：ポップアップ画面「登録」ボタン
                    ->screenshot('/estimates/ui/step3_move_before')                                         // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    ->type('item_name[6]',              $add_dsti_det[7]->item_name." 操作３回目 空行追加")   // 入力：商品名
                    ->type('item_description[6]',       $add_dsti_det[7]->item_description)                 // 入力：商品規格
                    ->type('quantity[6]',               $add_dsti_det[7]->quantity)                         // 入力：数量
                    ->type('unit[6]',                   $add_dsti_det[7]->unit)                             // 入力：単位
                    ->type('sal_price[6]',              $add_dsti_det[7]->sal_price)                        // 入力：売単価
                    ->type('cost_price[6]',             $add_dsti_det[7]->cost_price)                       // 入力：原単価	
                    ->select('tax_flg[6]',              $add_dsti_det[7]->tax_flg)                          // 入力：課税区分
                    ->type('item_remarks[6]',           $add_dsti_det[7]->item_remarks)                     // 入力：備考
                    //7行目を3行目に移動
                    ->pause(2000)
                    ->drag('#test_img6', '#test_img0')
                    ->pause(2000)
                    ->screenshot('/estimates/ui/step3_move_after')                                              // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    ->assertInputValue('item_name[0]',  $estimate_detail[1]->item_name)                         // 確認：1行目 商品名2
                    ->assertInputValue('item_name[1]',  $add_dsti_det[7]->item_name." 操作３回目 空行追加")       // 確認：2行目 商品名7
                    ->assertInputValue('item_name[2]',  $add_dsti_det[6]->item_name." 操作２回目 空行データ入力") // 確認：3行目 商品名6
                    ->assertInputValue('item_name[3]',  $estimate_detail[4]->item_name)                         // 確認：4行目 商品名5
                    ->assertInputValue('item_name[4]',  $estimate_detail[0]->item_name)                         // 確認：5行目 商品名1
                    ->assertInputValue('item_name[5]',  $estimate_detail[2]->item_name)                         // 確認：6行目 商品名3
                    ->assertInputValue('item_name[6]',  $estimate_detail[3]->item_name)                         // 確認：7行目 商品名4

                    /* -----操作４回目------------------------------------------------- 
                     * 行が正しく複写され、データ入力後、行移動が正しくされてるか
                     * --------------------------------------------------------------- */
                
                    ->screenshot('/estimates/ui/step4_move_before')                                              // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    //1行目コピー
                    ->pressAndWaitFor('#row_copy', 5000)                                                                        
                    ->type('item_name[7]',              "商品8 操作操作４回目 複写")                               // 入力：商品名
                    //8行目を2行目に移動
                    ->drag('#test_img7', '#test_img0')                                                          
                    ->visit('/estimates/edit/'.$estimate->esti_no."#")                                           //移動：頁トップ
                    //1行目を削除
                    ->pause(5000)
                    ->pressAndWaitFor('#row_delete', 5000)
                    ->pause(5000)
                    ->pressAndWaitFor('#test_confirm', 5000)                                                       // 押下：ポップアップ画面「削除」ボタン
                    ->pause(5000)
                    ->screenshot('/estimates/ui/step4_move_after')                                               // 画像：前 ドラッグアンドドロップ前画像スナップショット
                    ->pause(2000)
                
                    ->assertInputValue('item_name[0]',  "商品8 操作操作４回目 複写")                              // 確認：1行目 商品名8
                    ->assertInputValue('item_name[1]',  $add_dsti_det[7]->item_name." 操作３回目 空行追加")       // 確認：2行目 商品名7
                    ->assertInputValue('item_name[2]',  $add_dsti_det[6]->item_name." 操作２回目 空行データ入力") // 確認：3行目 商品名6
                    ->assertInputValue('item_name[3]',  $estimate_detail[4]->item_name)                         // 確認：4行目 商品名5
                    ->assertInputValue('item_name[4]',  $estimate_detail[0]->item_name)                         // 確認：5行目 商品名1
                    ->assertInputValue('item_name[5]',  $estimate_detail[2]->item_name)                         // 確認：6行目 商品名3
                    ->assertInputValue('item_name[6]',  $estimate_detail[3]->item_name)                         // 確認：7行目 商品名4
                
                    /* ----------------------------------------------------------------------------
                     * 更新処理　行移動の順番で登録されているか
                     * ---------------------------------------------------------------------------- */
                    ->pressAndWaitFor('#regist', 50)                                     // 押下：「登録」ボタン
                    ->pause(2000)
                    ->press('#test_confirm')                                             // 押下：ポップアップ画面「登録」ボタン
                    ->pause(2000)
                    ->assertPathIs('/estimates/edit')                                    // 確認：見積書登録であること
                    ->assertSee('見積書を登録しました。')                                 // 確認:メッセージ
                     //一覧
                    ->press('#list')                                                     // 押下：ポップアップ画面「登録」ボタン
                    ->assertPathIs('/estimates')                                         // 確認：見積書登録であること
                     //更新画面
                    ->press('#test_update_'.$estimate->esti_no)                          // 「変更」ボタンを押下
                    ->assertPathIs('/estimates/edit/'.$estimate->esti_no)               // 確認：見積書登録であること
                    /* --------------------------------
                     * 変更内容と同じかチェック -　明細情報
                     *--------------------------------- */
                    ->assertInputValue('item_name[0]',  "商品8 操作操作４回目 複写")                              // 確認：1行目 商品名8
                    ->assertInputValue('item_name[1]',  $add_dsti_det[7]->item_name." 操作３回目 空行追加")       // 確認：2行目 商品名7
                    ->assertInputValue('item_name[2]',  $add_dsti_det[6]->item_name." 操作２回目 空行データ入力") // 確認：3行目 商品名6
                    ->assertInputValue('item_name[3]',  $estimate_detail[4]->item_name)                         // 確認：4行目 商品名5
                    ->assertInputValue('item_name[4]',  $estimate_detail[0]->item_name)                         // 確認：5行目 商品名1
                    ->assertInputValue('item_name[5]',  $estimate_detail[2]->item_name)                         // 確認：6行目 商品名3
                    ->assertInputValue('item_name[6]',  $estimate_detail[3]->item_name)                         // 確認：7行目 商品名4
                
                    ->pause(2000);
            
        });
    }        
    
    
}
