<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id', 20);
            $table->string('customer_name', 120);
            $table->string('customer_kana', 120)->nullable();
            $table->string('representative_name', 120)->nullable();
            $table->string('representative_kana', 120)->nullable();
            $table->string('department_name', 120)->nullable();
            $table->string('person_name', 120)->nullable();
            $table->string('zip', 10)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('tel', 20)->nullable();
            $table->string('fax', 20)->nullable();
            $table->string('cellphone', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
