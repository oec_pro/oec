<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(TaxratesTableSeeder::class);
        $this->call(EstimatesTableSeeder::class);
        $this->call(EstimatesDetailsTableSeeder::class);

        Model::reguard();        
    }
}
