<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormEstimateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
         return [];
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id',
            'customer_id',
            'customer_name',
            'esti_no',
            'usr_esti_no',
            'estimate_date',
            'sales_cond',
            'title',
            'delivery_location',
            'expiration_date',
            'delivery_date',
            'pay_conditions',
            'remarks',
            'total_amount_taxnon',
            'tax_amount',
            'total_amount_taxin',
            'cost_amount',
            'tax_type',
            'tax_rate',
            'coments',
            'created_name',
            'updated_name',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }
    
    public function messages()
    {
        return [
        ];
    }    
}
