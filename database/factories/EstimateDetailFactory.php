<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(App\Estimate_detail::class, function (Faker $faker) {
    $faker = FakerFactory::create('ja_JP');

    return [
//        'esti_no'       	=> '1',
        'usr_esti_no'       => '1',
        'esti_line_no'      => $faker->numberBetween($min = 0, $max = 100),
        'item_id'       	=> '',
        'item_name'       	=> $faker->company,
        'item_description'  => $faker->text(80),
        'quantity'       	=> $faker->numberBetween($min = 0, $max = 10),
        'unit'       		=> '個',
        'sal_price'       	=> $faker->numberBetween($min = 0, $max = 1000),
        'sal_amount'       	=> $faker->numberBetween($min = 0, $max = 1000),
        'cost_price'       	=> $faker->numberBetween($min = 0, $max = 1000),
        'cost_amount'       => $faker->numberBetween($min = 0, $max = 1000),
        'tax_flg'       	=> $faker->numberBetween($min = 1, $max = 3),
        'item_remarks'      => $faker->realText,
        'created_name'      => $faker->name,
        'updated_name'      => $faker->name,

    ];
});
