<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaxratesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxrates')->insert([
            [
              'effective_date' => date('1997-04-01 00:00:00'),
              'tax_rate' => '5',
            ],
            [
              'effective_date' => date('2014-04-01 00:00:00'),
              'tax_rate' => '8',
            ],
        ]);
    }
}
